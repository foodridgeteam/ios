//
//  SWHamburgerRevealViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/8/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "SWHamburgerRevealViewController.h"

@interface SWHamburgerRevealViewController ()

@end

@implementation SWHamburgerRevealViewController

- (void)setFrontViewController:(UIViewController *)frontViewController{
    
    if([frontViewController isKindOfClass:[UINavigationController class]]){
        UIViewController *vc = [[(UINavigationController *)frontViewController viewControllers] firstObject];
        vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(showPanel)];
    }
    [super setFrontViewController:frontViewController];
}

- (void)setFrontViewController:(UIViewController *)frontViewController animated:(BOOL)animated{
    
    if([frontViewController isKindOfClass:[UINavigationController class]]){
        UIViewController *vc = [[(UINavigationController *)frontViewController viewControllers] firstObject];
        vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(showPanel)];
    }
    [super setFrontViewController:frontViewController animated:animated];

}

- (void)showPanel{
    [self revealToggleAnimated:YES];
}



@end
