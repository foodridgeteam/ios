//
//  FRMealInfoViewController.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMealInfoViewController.h"
#import "FRMealInfoCell.h"
#import "FRAboutDishCell.h"
#import "FRIngridientsCell.h"
#import "FRFoodContentCell.h"
#import "FRAverageRatingCell.h"
#import "FRReviewCell.h"
#import "UIFont+FoodridgeFonts.h"
#import "FRStaticDataService.h"
#import "FRChefDetailViewController.h"
#import "FROrderComposeViewController.h"
#import "FRCommentsService.h"
#import "FRCommentsModel.h"
#import "FRFoodContentOthersCell.h"
#import "UIColor+FoodridgeColors.h"

typedef NS_ENUM(NSInteger, sectionType) {
    sectionTypeMealInfo = 0,
    sectionTypeAboutDish,
    sectionTypeIngridients,
    sectionTypeFoodContent,
    sectionTypeAverageRating,
    sectionTypeReviews
};

@interface FRMealInfoViewController ()

@property (nonatomic, strong) NSArray *ingredients;
@property (nonatomic, strong) NSArray *mealReviews;

@end

static NSUInteger const kFRFoodContentOtherIndex = 11;

@implementation FRMealInfoViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationController.navigationBar.translucent = NO;
    //self.title = NSLocalizedString(@"STR_MEAL_INFO", nil);
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    [[FRCommentsService service] getCommentsForMealWithID:[self.meal.uid integerValue] completion:^(id object, id rawObject, NSError *error) {
        if (!error) {
            self.mealReviews = object;
            [self.tableView reloadData];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
     self.navigationItem.title = self.meal.title;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case sectionTypeMealInfo:
            return 1;
            break;
        
        case sectionTypeAboutDish:
            
            return 1;
            break;
            
        case sectionTypeIngridients:
            if (self.meal.ingridients) {
                self.ingredients = self.meal.ingridients;
            }
            
            return self.meal.ingridients.count;
            
            break;
            
        case sectionTypeFoodContent:
            return self.meal.otherFlags.length > 0 ? 12 : 11;
            break;
            
        case sectionTypeAverageRating:
            return 1;
            break;
            
        case sectionTypeReviews:
            return [self.meal.reviewsCount integerValue];
            break;
            
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case sectionTypeMealInfo:{
            FRMealInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mealInfoCell" forIndexPath:indexPath];
            [cell setupCellWithMealModel:self.meal];
            return cell;
            break;
        }
        
        case sectionTypeAboutDish:{
            FRAboutDishCell *cell = [tableView dequeueReusableCellWithIdentifier:@"aboutDishCell" forIndexPath:indexPath];
            [cell setupCellWithMealModel:self.meal];
            return cell;
            break;
        }
            
        case sectionTypeIngridients:{
            FRIngridientsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ingridientsCell" forIndexPath:indexPath];
            [cell setupCellWithIngridientModel:[self.ingredients objectAtIndex:indexPath.row]];
            return cell;
            break;
        }
            
        case sectionTypeFoodContent:{
            
            
            if(indexPath.row == kFRFoodContentOtherIndex){
                FRFoodContentOthersCell *cell = [tableView dequeueReusableCellWithIdentifier:@"foodContentOtherCell" forIndexPath:indexPath];
                
               [cell setFoodContentCellWithFlag:[[FRMealExtendedModel flagNames] objectAtIndex:indexPath.row - 1] mealModel:self.meal];
                
                return cell;
                break;
            }
            
            FRFoodContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"foodContentCell" forIndexPath:indexPath];
            
            if (indexPath.row == 0) {
                cell.hideSpicyControl = NO;
                [cell setFoodContentCellWithFlag:nil mealModel:self.meal];
            }
            else{
                cell.hideSpicyControl = YES;
                [cell setFoodContentCellWithFlag:[[FRMealExtendedModel flagNames] objectAtIndex:indexPath.row - 1] mealModel:self.meal];
            }
         
            return cell;
            break;
        }
       
        case sectionTypeAverageRating:{
            FRAverageRatingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"averageRatingCell" forIndexPath:indexPath];
            [cell setupCellWithMealModel:self.meal];
            return cell;
            break;
        }
            
        case sectionTypeReviews:{
            FRReviewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reviewCell" forIndexPath:indexPath];
            
            if (indexPath.row < self.mealReviews.count) {
                [cell setupCellWithCommentModel:self.mealReviews[indexPath.row]];
            }
            return cell;
            break;
        }
            
        default:
            return nil;
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 70;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 260, 44)];
    titleLabel.font = [UIFont openSansBoldFontWithSize:17];
    titleLabel.textColor = [UIColor darkGrayColor];
    
    if (section == sectionTypeIngridients) {
        titleLabel.text = NSLocalizedString(@"STR_INGRIDIENTS", nil).uppercaseString;
    }
    else if (section == sectionTypeFoodContent) {
        titleLabel.text = NSLocalizedString(@"STR_FOOD_CONTENT_DISCLAIMER", nil).uppercaseString;
    }
    else {
        titleLabel.text =  NSLocalizedString(@"STR_MEAL_REVIEWS", nil).uppercaseString;
        UIImageView *commentsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 130, 18, 20, 20)];
        commentsImageView.image = [UIImage imageNamed:@"reviews_image"];
        
        UILabel *commentsCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(commentsImageView.frame.origin.x + commentsImageView.frame.size.width + 5, 4, 130, 44)];
        commentsCountLabel.textColor = [UIColor commentsBlueColor];
        commentsCountLabel.font = [UIFont openSansBoldFontWithSize:14];
        commentsCountLabel.text = [NSString stringWithFormat:@"%@ %@", self.meal.reviewsCount.stringValue, NSLocalizedString(@"STR_COMMENTS", nil)];
        
        [view addSubview:commentsCountLabel];
        [view addSubview:commentsImageView];
    }
    
    [view addSubview:titleLabel];

    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == sectionTypeIngridients || section == sectionTypeFoodContent) {
        return 44;
    }
    else if (section == sectionTypeReviews && self.mealReviews.count) {
        return 44;
    }
    else {
        return 0;
    }
}

#pragma mark - cell delegate

- (IBAction)orderNowButtonClicked {
    
    id<FRMealModel> meal = self.meal;
    
    [FROrderComposeViewController presentWithMeal:meal fromViewController:self];
}

- (IBAction)showChefInfo:(id)sender {
   
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"ChefDetail" bundle:nil];
    FRChefDetailViewController *vc = [sb instantiateInitialViewController];
    
    vc.chefUid = self.meal.chefID;
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
