//
//  FROrderMessageCell.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/29/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrderMessageCell.h"
#import "NSString+DateFormats.h"
#import "UIColor+FoodridgeColors.h"

@interface FROrderMessageCell ()

@property (strong, nonatomic) IBOutlet UILabel *messageTimeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *messageImageView;
@property (strong, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *messageTextLabel;


@end

@implementation FROrderMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setupWithOrder:(FROrderModel *)order message:(FROrderMessageModel *)message {
    
    if([message.senderUID isEqualToNumber: order.customer.uid]) {
        self.senderNameLabel.text =  order.customer.name;
        self.senderNameLabel.textColor = [UIColor aquaColor];
    } else {
        self.senderNameLabel.text = order.meal.chefTitle;
        self.senderNameLabel.textColor = [UIColor blackColor];
    }
    
    self.messageTextLabel.text = message.messageText;
    self.messageTimeLabel.text = [NSString localHoursMinutesStringFromDate:message.date];
    
    if([message.senderUID isEqualToNumber: order.customer.uid]){
        self.messageImageView.image = [UIImage imageNamed:[FROrderMessageModel getMessageImageNameForOrderStatusClient:message.type.integerValue asReceiver:order.customer.uid == message.senderUID ? YES : NO]]; }
    else {
        self.messageImageView.image = [UIImage imageNamed:[FROrderMessageModel getMessageImageNameForOrderStatus:message.type.integerValue asReceiver:order.customer.uid == message.senderUID ? YES : NO]];
    }
}

@end
