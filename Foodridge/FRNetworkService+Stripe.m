//
//  FRNetworkService+Stripe.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 7/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService+Stripe.h"

@implementation FRNetworkService (Stripe)

+ (NSURLSessionTask *)deleteStripeAccountWithCompletion:(FRResponceBlock)completion {
    return [FRNetworkService requestWithMethod:@"DELETE" requestAddress:@"stripe_disconnect" parameters:nil completion:completion];
}

@end
