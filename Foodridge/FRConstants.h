//
//  FRConstants.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

//server error codes
static const NSInteger FRErrorCodeEmailTaken = 9;
static const NSInteger FRErrorCodeInvalidLogin = 4;

//public API keys
//Stripe
static const NSString *FRStripePlatoformID  = @"ca_8OB29BXO5bvtIf5fFGYiQJmAGT83krzf";
static const NSString *FRStripePublicKey    = @"pk_live_5fhxilNDB1u27FnX2H3TAxlJ";
static const NSString *FRStripeRedirectURL  = @"https://www.foodridge.com/api/stripe";

