//
//  FRPanelViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRPanelViewController.h"
#import "FRAuthorisationService.h"
#import "FRMainScreenService.h"
#import "FRProfileModel.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <SWRevealViewController/SWRevealViewController.h>
#import <MessageUI/MessageUI.h>
//#import <MessageUI/MFMailComposeViewController.h>
#import "FRChefService.h"
#import "FRChefProfileModel.h"
#import "FRBuisinessInfoModel.h"
#import "FRTermsService.h"
#import "FROrdersContainerViewController.h"
#import "FROrdersService.h"

typedef NS_ENUM(NSInteger, FRPanelSection) {
    
    FRPanelSectionMenu = 0,
    FRPanelSectionOrders = 1,
    FRPanelSectionProfile = 2,
    FRPanelSectionMyMenu = 3,
    FRPanelSectionChefs = 4,
    FRPanelSectionAbout = 5,
    FRPanelSectionFeedback = 6,
    FRPanelSectionTerms = 7,
    FRPanelSectionLogout = 8,
};

@interface FRPanelViewController () <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailLabel;

@property (nonatomic, assign)  FRPanelSection selectedSection;

@property(nonatomic, strong) NSIndexPath *lastselected;

@end

@implementation FRPanelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectedSection = FRPanelSectionMenu;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:FRProfileUpdatedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:FRCurrentProfileUpdatedNotificationName object:nil];


}

- (void)reloadData{
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedSection inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    FRProfileModel *model = [[FRAuthorisationService service] currentUser];
    self.usernameLabel.text = model.name;
    self.emailLabel.text = model.email;
    
    NSURL *url = [NSURL URLWithString:model.avatarURL];
    
    if(url){
        [self.avatarImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    }
}

- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    self.avatarImageView.layer.masksToBounds = YES;
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.width * 0.5;
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self reloadData];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.revealViewController.frontViewController.view.userInteractionEnabled = NO;
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.revealViewController.frontViewController.view.userInteractionEnabled = YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == FRPanelSectionMyMenu && ![[FRChefService service] currentChef]){
        return 0;
    }
    
    return self.tableView.rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == self.selectedSection){
        return;
    }
    
    switch (indexPath.row) {
        case FRPanelSectionMenu:
            
            [self showMenu];
            break;

        case FRPanelSectionOrders:
            
            [self showOrdersOnPlacedPage:NO];
            break;
            
        case FRPanelSectionProfile:
            
            [self showProfile];
            break;
            
        case FRPanelSectionMyMenu:
            
            [self showMyMenu];
            break;
            
        case FRPanelSectionChefs:
            
            [self showChefs];
            break;
            
        case FRPanelSectionAbout:
            
            [self showAbout];
            break;
            
        case FRPanelSectionFeedback:
            
            [self createFeedback];
            break;
            
        case FRPanelSectionTerms:
            
            [self showTerms];
            break;
            
        case FRPanelSectionLogout:
            
            [tableView selectRowAtIndexPath:self.lastselected animated:YES scrollPosition:UITableViewScrollPositionNone];
            
            [self logout];
            break;
            
        default:
            self.selectedSection = self.selectedSection;
            break;
    }
}

- (void)setSelectedSection:(FRPanelSection)selectedSection{
    _selectedSection = selectedSection;
    
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedSection inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)showMenu{
    if(self.selectedSection != FRPanelSectionMenu){
        
        self.selectedSection = FRPanelSectionMenu;
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Menu" bundle:nil];
        UIViewController *vc = [sb instantiateInitialViewController];
        
        [self.revealViewController setFrontViewController:vc animated:NO];
    }
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];

}
- (IBAction)showProfile{
    if(self.selectedSection != FRPanelSectionProfile){
        
        self.selectedSection = FRPanelSectionProfile;
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
        UIViewController *vc = [sb instantiateInitialViewController];
        
        [self.revealViewController setFrontViewController:vc animated:NO];
    }
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];

}

- (void)showOrdersOnPlacedPage:(BOOL)placed {
    if(self.selectedSection != FRPanelSectionOrders){
        
        self.selectedSection = FRPanelSectionOrders;
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Orders" bundle:nil];
        UIViewController *vc = [sb instantiateInitialViewController];
        
        if (placed) {
            FROrdersContainerViewController *ordersContainerView = (FROrdersContainerViewController *)[vc.childViewControllers firstObject];
            [ordersContainerView openOnPlacedPage];
        }
        
        [self.revealViewController setFrontViewController:vc animated:NO];
    }
    
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
}

- (void)showChefs{
    if(self.selectedSection != FRPanelSectionChefs){
        
        self.selectedSection = FRPanelSectionChefs;
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Chefs" bundle:nil];
        UIViewController *vc = [sb instantiateInitialViewController];
        
        [self.revealViewController setFrontViewController:vc animated:NO];
    }
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
}

- (void)showMyMenu{
    
    if(self.selectedSection != FRPanelSectionMyMenu){    
        self.selectedSection = FRPanelSectionMyMenu;
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dishes" bundle:nil];
        UIViewController *vc = [sb instantiateInitialViewController];
        
        [self.revealViewController setFrontViewController:vc animated:NO];
    }
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
}

- (void)showAbout{
    if (self.selectedSection != FRPanelSectionAbout) {
        self.selectedSection = FRPanelSectionAbout;
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"About" bundle:nil];
        UIViewController *vc = [sb instantiateInitialViewController];
        [self.revealViewController setFrontViewController:vc animated:NO];
    }
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
}

-(void)showTerms{
    [[FRTermsService new] showTermsFromViewController:self shouldAgree:NO onDismiss:nil];
}

- (void)createFeedback{
    
    if(self.selectedSection != FRPanelSectionFeedback){
        self.selectedSection = FRPanelSectionFeedback;
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Feedback" bundle:nil];
        UIViewController *vc = [sb instantiateInitialViewController];
        
        [self.revealViewController setFrontViewController:vc animated:NO];
    }
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
}

- (void)logout{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_ARE_YOU_SURE_LOGOUT", @"") message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"STR_CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *logoutAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"STR_LOGOUT", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [[FRAuthorisationService service] logout];
        [[FRMainScreenService new] showLoginScreen];

    }];
    [alert addAction:cancelAction];
    [alert addAction:logoutAction];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - MailComposeDelegate

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(nullable NSError *)error {
 
    [self dismissViewControllerAnimated:YES completion:nil];
}
    



@end
