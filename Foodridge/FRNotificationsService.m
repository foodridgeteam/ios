//
//  FRNotificationsService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//
@import UIKit;
#import "FRNotificationsService.h"
#import "FRNetworkService+Profile.h"
#import "FRAuthorisationService.h"
#import "FRMealInfoViewController.h"
#import "FRMealsService.h"
#import "FROrdersListViewController.h"
#import "FROrdersService.h"
#import "FRMainScreenService.h"

@implementation FRNotificationsService

- (void)registerForNotifications{
    
    NSString *pushToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"FRPushToken"];
    if([[FRAuthorisationService service] authorized] && pushToken){
        [FRNetworkService updatePushToken:pushToken completion:^(id object, id rawObject, NSError *error) {
            
        }];
    }

    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |
                                                                                         UIUserNotificationTypeAlert |
                                                                                         UIUserNotificationTypeBadge) categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
}

- (void)processPushNotificationToken:(NSData *)tokenData{
    
    const unsigned char *deviceTokenBytes = [tokenData bytes];
    NSString *token = @"";
    
    for (int i = 0; i < [tokenData length]; i++) {
        token = [token stringByAppendingFormat:@"%02x", deviceTokenBytes[i]];
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"FRPushToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if([[FRAuthorisationService service] authorized]){
        [FRNetworkService updatePushToken:token completion:^(id object, id rawObject, NSError *error) {
            
        }];
    }
}

- (void)processNotification:(NSDictionary *)userInfo{
    
    NSInteger pushType = [[userInfo objectForKey:@"type"] integerValue];
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    
    if (state == UIApplicationStateActive) {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.userInfo = userInfo;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertBody = [userInfo valueForKeyPath:@"aps.alert"];
        localNotification.fireDate = [NSDate date];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
        if (pushType == notificationTypeNewOrder || pushType == FROrderStatusPayed) {
            [[FROrdersService service] processOrderNotification:userInfo];
        }
        else if (pushType == FROrderStatusAccepted || pushType == FROrderStatusPrepared || pushType == FROrderStatusSent) {
            [[FROrdersService service] processOrderNotification:userInfo];
        }
        else if (pushType == notificationTypeNewMessage || pushType == FROrderStatusReceived || pushType == FROrderStatusCompleted) {
            [[FROrdersService service] processOrderNotification:userInfo];
        }
    }
    else {
        [self processNotificationFromDictionary:userInfo];
    }
}

- (void)processNotificationFromAppLaunch:(NSDictionary *)userInfo {
    [self processNotificationFromDictionary:userInfo];
}

- (void)processNotificationFromDictionary:(NSDictionary *)userInfo {
    NSInteger pushType = [[userInfo objectForKey:@"type"] integerValue];
    NSInteger objectID = [[userInfo objectForKey:@"entity"] integerValue];
    
    if (pushType == notificationTypeNewMealAdded || pushType == notificationTypeNewComment) {
        [[FRMainScreenService new] showMealInfoScreenForMealWithID:objectID];
    }
    else if (pushType == notificationTypeNewOrder || pushType == FROrderStatusPayed) {
        [[FRMainScreenService new] showOrdersListAndFocusOnPlased:NO];
        [[FROrdersService service] processOrderNotification:userInfo];
    }
    else if (pushType == FROrderStatusAccepted || pushType == FROrderStatusPrepared || pushType == FROrderStatusSent) {
        [[FRMainScreenService new] showOrdersListAndFocusOnPlased:YES];
        [[FROrdersService service] processOrderNotification:userInfo];
    }
    else if (pushType == notificationTypeNewMessage || pushType == FROrderStatusReceived) {
        [[FRMainScreenService new] showOrdersListAndFocusOnPlased:NO];
        [[FROrdersService service] processOrderNotification:userInfo];
    }
}

@end
