//
//  THPlaceHolderTextView.h

//
//  Created by Maxym Deygin on 5/20/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//


/**
 *  Textview with ability to add placeholder text, and autoalling height constraint base on input text
 */
@import UIKit;

@interface THPlaceHolderTextView : UITextView

@property (nonatomic, assign) IBInspectable NSInteger minHeigth;

@property (nonatomic, retain) IBInspectable NSString *placeholder;
@property (nonatomic, retain) IBInspectable UIColor *placeholderColor;
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

-(void)textChanged:(NSNotification*)notification;

@end
