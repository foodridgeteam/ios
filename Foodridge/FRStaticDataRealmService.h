//
//  FRStaticDataService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRRealmService.h"

@interface FRStaticDataRealmService : FRRealmService

+ (void)saveCuisinesWithData:(NSArray *)array;
+ (void)saveIngridientWithData:(NSArray *)array;

+ (RLMResults *)getCuisinesWithIDs:(NSArray *)ids;
+ (RLMResults *)getCuisinesWithSearchText:(NSString *)text;

+ (RLMResults *)getIngridientsWithIDs:(NSArray *)ids;
+ (RLMResults *)getIngridientsWithSearchText:(NSString *)text;

@end
