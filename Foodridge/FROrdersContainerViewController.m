//
//  FROrdersContainerViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrdersContainerViewController.h"
#import "FROrdersListViewController.h"

@interface FROrdersContainerViewController ()

@property(nonatomic, strong) FROrdersListViewController *embedList;
@property(nonatomic, assign) BOOL showOnPlaced;

@end

@implementation FROrdersContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.showOnPlaced) {
        self.segmentedControl.selectedSegmentIndex = FROrdersFilterPlaced;
        [self.embedList setSelectedFilter:FROrdersFilterPlaced];
    }
}

- (IBAction)segmentChanged:(UISegmentedControl *)sender {
    [self.embedList setSelectedFilter:sender.selectedSegmentIndex];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"OrderListEmbedSegue"]){
        self.embedList = segue.destinationViewController;
    }
}

- (void)openOnPlacedPage {
    self.showOnPlaced = YES;
}

@end
