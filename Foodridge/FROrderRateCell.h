//
//  FRRateChefCell.h
//  Foodridge
//
//  Created by Victor Miroshnichenko on 5/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FROrderRateCell;

@protocol FROrderRateCellDelegate <NSObject>

@optional

- (void)rateOrderWithText:(NSString *)reviewText chefRating:(NSInteger)chefRating inCell:(FROrderRateCell *)cell;

@end

@interface FROrderRateCell : UITableViewCell

@property (weak, nonatomic) id<FROrderRateCellDelegate> delegate;

@end
