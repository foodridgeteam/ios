//
//  FRChefService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FRNetworkService.h"
#import "FRMealExtendedModel.h"

static  NSString *FRProfileUpdatedNotificationName = @"FRProfileUpdatedNotification";

@class FRChefProfileModel;
@interface FRChefService : NSObject

@property(nonatomic, strong, readonly) FRChefProfileModel *currentChef;

+ (FRChefService *)service;

+ (void)clearData;

- (NSURLSessionTask *)getChefProfileWithCompletion:(FRResponceBlock)completion;

- (NSURLSessionTask *)updateChefProfile:(FRChefProfileModel *)profile WithCompletion:(FRResponceBlock)completion;

- (NSURLSessionTask *)saveMyMeal:(id<FRMealModel>)mealModel withCompletion:(FRResponceBlock)completion;

- (void)connectStripe;

@end
