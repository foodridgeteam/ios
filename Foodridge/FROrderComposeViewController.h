//
//  FROrderComposeViewController.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/25/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRMealModel.h"

@interface FROrderComposeViewController : UIViewController

+ (void)presentWithMeal:(id<FRMealModel>)mealModel
     fromViewController:(UIViewController *)sourceViewController;

@end
