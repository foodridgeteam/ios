//
//  FRBuisinessInfoModel.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"
#import <Foundation/Foundation.h>

@interface FRBuisinessInfoModel : FRViewModel

@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *registrationID;
@property(nonatomic, copy) NSString *url;
@property(nonatomic, copy) NSString *phone;

- (BOOL)isValid;

@end
