//
//  ChefDetailViewController.h
//  Foodridge
//
//  Created by NekitBOOK on 25.04.16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRChefProfileModel.h"

@interface FRChefDetailViewController : UITableViewController
@property (strong, nonatomic) FRChefProfileModel *chefProfile;
@property (strong, nonatomic) NSNumber *chefUid;
@property (strong,nonatomic) NSArray* data;

@end
