//
//  FRAverageRatingCell.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRAverageRatingCell.h"
#import "FRReviewControl.h"
#import "UIColor+FoodridgeColors.h"
#import "UIImage+Color.h"
#import "FRAuthorisationService.h"

@interface FRAverageRatingCell ()

@property (strong, nonatomic) IBOutlet UILabel *totalPortionsSoldLabel;
@property (strong, nonatomic) IBOutlet UILabel *portionsCountLabel;
@property (strong, nonatomic) IBOutlet FRReviewControl *averageRatingControl;
@property (nonatomic, weak) IBOutlet UIButton* orderNowButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderNowViewHightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderNowTopSeparatorViewHightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderNowBottomSeparatorViewHightConstraint;

@end

@implementation FRAverageRatingCell



- (void)setupCellWithMealModel:(FRMealExtendedModel *)meal {
    
    self.totalPortionsSoldLabel.text = NSLocalizedString(@"STR_TOTAL_PORTIONS_SOLD", nil);
    self.portionsCountLabel.text = meal.portionsSold.stringValue;
    self.averageRatingControl.value = meal.averageRating.integerValue;
    
    if(meal.chefID.integerValue == [FRAuthorisationService currentUID]){
        self.orderNowViewHightConstraint.constant = 0;
        self.orderNowTopSeparatorViewHightConstraint.constant = 0;
        self.orderNowBottomSeparatorViewHightConstraint.constant = 0;
        
        return;
    }
    
    if (meal.servingsLeft.integerValue > 0){
        [self.orderNowButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
        [self.orderNowButton setTitle:NSLocalizedString(@"STR_ORDER_NOW", nil) forState:UIControlStateNormal];
        
    }else{
        
        [self.orderNowButton setImage:[UIImage imageNamed:@"order_icon"] forState:UIControlStateNormal];
        [self.orderNowButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
        [self.orderNowButton setTitle:NSLocalizedString(@"STR_SOLD_OUT", nil) forState:UIControlStateNormal];
    }
}

- (IBAction)orderNowAction:(id)sender {
    
}

@end
