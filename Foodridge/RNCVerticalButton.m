//
//  RNCVerticalButton.m
//  Foodridge
//
//  Created by Vitaly Evtushenko on 7/29/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import <Masonry/Masonry.h>

#import "RNCVerticalButton.h"

@implementation RNCVerticalButton



- (void)layoutSubviews {
    [super layoutSubviews];

    self.contentEdgeInsets = UIEdgeInsetsMake(0, 0, CGRectGetHeight(self.titleLabel.bounds) + 4, 0);
    self.titleEdgeInsets   = UIEdgeInsetsMake(CGRectGetHeight(self.imageView.bounds) + 30, - CGRectGetWidth(self.imageView.bounds), 0, 0);
    self.imageEdgeInsets   = UIEdgeInsetsMake( 0, 0, 0, - CGRectGetWidth(self.titleLabel.bounds) );
}

@end
