//
//  FROrderCommentCell.h
//  Foodridge
//
//  Created by Victor Miroshnichenko on 5/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRCommentsModel.h"

@interface FROrderCommentCell : UITableViewCell

- (void)setupCellWithCommentModel:(FRCommentsModel *)model;

@end
