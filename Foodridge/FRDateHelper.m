//
//  FRDateHelper.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/26/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRDateHelper.h"

@implementation FRDateHelper

+ (NSDate *)dateFromString:(NSString *)string {
    
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter =  [NSDateFormatter new];
        [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:locale];
    });
    
    NSDate *date = [dateFormatter dateFromString:string];
    return date;
}

+ (NSString *)dateStringFromDate:(NSDate *)date {
    
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter =  [NSDateFormatter new];
        [dateFormatter setDateFormat:@"EE, d MMMM, HH:mm"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    });
    
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    if (!formattedDateString.length) {
        return NSLocalizedString(@"STR_OFFLINE", nil);
    }
    
    return formattedDateString;
}

@end
