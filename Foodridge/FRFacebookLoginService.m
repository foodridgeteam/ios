//
//  FRFacebookLoginService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRFacebookLoginService.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@implementation FRFacebookLoginService

- (void)loginFromViewController:(UIViewController *)cntr completion:(FRFacebookLoginBlock)completion{

    if ([FBSDKAccessToken currentAccessToken]) {
        [FBSDKAccessToken setCurrentAccessToken:nil];
        [FBSDKProfile setCurrentProfile:nil];
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    [login logInWithReadPermissions:@[@"email", @"public_profile"]
                 fromViewController:cntr
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (!error && !result.isCancelled && [result.grantedPermissions
                                                                      containsObject:@"email"]) {
                                    
                                    if(!error){
                                        completion([FBSDKAccessToken currentAccessToken].userID, [FBSDKAccessToken currentAccessToken].tokenString, nil);
                                    }else{
                                        completion(nil, nil,error);
                                        
                                    }
                                    
                                } else {
                                    completion(nil, nil,error);
                                }
                            }];
    
    
}

@end
