//
//  FRMealCell.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRMealExtendedModel.h"

@class FRMealCell;
@protocol FRMealCellDelegate <NSObject>

- (void)orderNowButtonClicked:(FRMealCell *)cell;
- (void) showChef:(NSNumber*) chefID;

@end

@interface FRMealCell : UITableViewCell

@property(nonatomic,strong) FRMealExtendedModel* cellMealModel;
@property(nonatomic, weak) id<FRMealCellDelegate> delegate;

- (void)setupCellFromMealModel:(FRMealExtendedModel *)meal;

@end
