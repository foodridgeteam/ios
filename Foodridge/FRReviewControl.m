//
//  FRReviewControl.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRReviewControl.h"

@implementation FRReviewControl

- (UIImage *)fillImage{
    return [UIImage imageNamed:@"star_full"];
}

- (UIImage *)emptyImage{
    return [UIImage imageNamed:@"star_empty"];
}

@end
