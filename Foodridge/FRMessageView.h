//
//  FRMessageView.h
//  Foodridge
//
//  Created by Victor Miroshnichenko on 5/3/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRMessageView : UIView

@property (nonatomic, assign) BOOL chef;
@property (nonatomic, copy) void (^onDone)(BOOL canceed, NSString *messageText);

@end
