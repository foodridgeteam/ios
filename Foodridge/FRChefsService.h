//
//  RFChefsService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/29/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRDynamicDataLoadService.h"

static  NSString *FRChefsUpdatedNotificationName = @"FRCHefsUpdatedNotification";


@interface FRChefsService : FRDynamicDataLoadService

- (NSURLSessionTask *)chefListWithSearchText:(NSString *)text
                                  completion:(FRResponceBlock)completion;

- (NSURLSessionTask*) getChefWithId:(NSNumber *)chefId completion:(FRResponceBlock)completion;

- (NSURLSessionTask*) followChefwithID:(NSNumber *)chefId
                                follow:(bool)follow
                            completion:(FRResponceBlock)completion;
@end
