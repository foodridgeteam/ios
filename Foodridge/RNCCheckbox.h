//
//  RNCCheckbox.h
//  EndyMed
//
//  Created by Vitaly Evtushenko on 7/29/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Checkbox UI component.
 */
@interface RNCCheckbox : UIButton

/**
 *  Set or get checkbox state. Sends UIControlEventValueChanged on change.
 */
@property (nonatomic, getter=isOn) IBInspectable BOOL on;

@end
