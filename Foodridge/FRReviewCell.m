//
//  FRReviewCell.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "UIImageView+RoundImage.h"
#import "FRReviewCell.h"
#import "FRDateHelper.h"
#import "FRReviewControl.h"

@interface FRReviewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UILabel *reviewText;
@property (strong, nonatomic) IBOutlet FRReviewControl *dishRate;
@end

@implementation FRReviewCell

-(void)awakeFromNib {
    self.userImage.layer.cornerRadius = self.userImage.frame.size.width/2;
    self.userImage.layer.masksToBounds = YES;
}


- (void)setupCellWithCommentModel:(FRCommentsModel *)comment {
    
    [self.userImage fr_setRoundImageWithUrl:[NSURL URLWithString:comment.userProfile.avatarURL] placeholderImage:[UIImage imageNamed:@"ic_no_user"] success:^(UIImage *image) {} failure:^(NSError *error) {}];
    
        self.userName.text = comment.userProfile.name;
    self.reviewText.text = comment.message;
    self.date.text = [self dateStringFromDate:comment.creationDate];
    self.dishRate.value = [comment.rating integerValue];
}

- (NSString *)dateStringFromDate:(NSDate *)date {
    
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter =  [NSDateFormatter new];
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    });
    
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    if (!formattedDateString.length) {
        return NSLocalizedString(@"STR_OFFLINE", nil);
    }
    
    return formattedDateString;
}



@end
