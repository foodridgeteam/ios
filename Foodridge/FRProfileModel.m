//
//  FRProfile.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRProfileModel.h"

@implementation FRProfileModel

- (NSDictionary *)dictionaryRepresentation{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if(self.name){
        params[@"name"] = self.name;
    }
    
    if(self.phone){
        params[@"phone"] = self.phone;
    }
    
    NSDictionary *deliveryDict = [self.deliveryInfo dictionaryRepresentation];

    if(deliveryDict){
        params[@"deliveryInfo"] = deliveryDict;
    }
    
    if(params.allKeys.count == 0){
        return nil;
    }
    return params.copy;
}

+ (instancetype)modelWitDictionary:(NSDictionary *)profileDict{
    
    FRProfileModel *profile = [FRProfileModel new];
    
    profile.uid = [profileDict objectOrNilForKey:@"id" classObject:[NSNumber class]];
    profile.email = [profileDict objectOrNilForKey:@"email" classObject:[NSString class]];
    profile.name = [profileDict objectOrNilForKey:@"name" classObject:[NSString class]];
    profile.avatarURL = [profileDict objectOrNilForKey:@"avatar" classObject:[NSString class]];
    profile.phone = [profileDict objectOrNilForKey:@"phone" classObject:[NSString class]];
    
    NSDictionary *deliveryInfoDict = [profileDict objectOrNilForKey:@"deliveryInfo" classObject:[NSDictionary class]];
    profile.deliveryInfo =  [FRDeliveryInfoModel modelWitDictionary:deliveryInfoDict];
    
    return profile;

}

@end
