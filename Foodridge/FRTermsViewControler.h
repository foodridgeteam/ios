//
//  TermsViewControler.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import UIKit;

@interface FRTermsViewControler : UIViewController

@property(nonatomic, assign) BOOL showAgreeButton;

@property (nonatomic, copy) void (^onDismiss)(BOOL agreed);


@end
