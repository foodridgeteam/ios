//
//  FRModel.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//
#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>
#import "NSDictionary+NullToNil.h"

@interface FRViewModel : MTLModel<MTLJSONSerializing>

- (NSDictionary *)dictionaryRepresentation;

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary;

+ (NSArray *)modelsFromArray:(NSArray *)array;



@end
