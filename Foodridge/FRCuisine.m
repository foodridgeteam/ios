//
//  FRCuisine.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRCuisine.h"

@implementation FRCuisine

+ (NSString *)primaryKey{
    return @"uid";
}

@end
