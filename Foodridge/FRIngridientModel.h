//
//  FRIngridientModel.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/15/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"

@interface FRIngridientModel : FRViewModel

@property (nonatomic, copy) NSNumber *uid;
@property (nonatomic, copy) NSString *name;

+ (NSArray *)getIngredientsModelsFromIngredientsIDsArray:(NSArray *)ingredientsIDsArray;

@end
