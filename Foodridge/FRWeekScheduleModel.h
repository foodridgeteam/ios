//
//  FRWeekSchedule.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"
#import <Foundation/Foundation.h>

@interface FRWeekScheduleModel : FRViewModel

@property(nonatomic, strong) NSDate *monOpenTime;
@property(nonatomic, strong) NSDate *monCloseTime;

@property(nonatomic, strong) NSDate *tueOpenTime;
@property(nonatomic, strong) NSDate *tueCloseTime;

@property(nonatomic, strong) NSDate *wedOpenTime;
@property(nonatomic, strong) NSDate *wedCloseTime;

@property(nonatomic, strong) NSDate *thuOpenTime;
@property(nonatomic, strong) NSDate *thuCloseTime;

@property(nonatomic, strong) NSDate *friOpenTime;
@property(nonatomic, strong) NSDate *friCloseTime;

@property(nonatomic, strong) NSDate *satOpenTime;
@property(nonatomic, strong) NSDate *satCloseTime;

@property(nonatomic, strong) NSDate *sunOpenTime;
@property(nonatomic, strong) NSDate *sunCloseTime;

- (NSDate *)openDateForDayAtIndex:(NSInteger)day;
- (NSDate *)closeDateForDayAtIndex:(NSInteger)day;

- (void)setOpenDate:(NSDate *)date forDayAtIndex:(NSInteger)day;
- (void)setCloseDate:(NSDate *)date forDayAtIndex:(NSInteger)day;

- (NSAttributedString *)intervalStringForDayAtIndex:(NSInteger)index;

@end
