//
//  FRFeedbackViewController.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <libextobjc/extobjc.h>
#import "FRFeedbackViewController.h"
#import "FRNetworkService+Defaults.h"
#import "FRProgressHUD.h"

@interface FRFeedbackViewController () <UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UITextView *feedbackTextView;
@property (strong, nonatomic) IBOutlet UIButton *sendFeedbackButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *progressActivityIndicator;

@end

@implementation FRFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sendFeedbackButton.layer.cornerRadius = 3;
    self.feedbackTextView.layer.cornerRadius = 3;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (IBAction)sendFeedbackAction:(id)sender {
    @weakify(self);
    [self.progressActivityIndicator startAnimating];
    [FRNetworkService sendFeedbackWithText:self.feedbackTextView.text completion:^(id object, id rawObject, NSError *error) {
        @strongify(self);
        self.feedbackTextView.text = nil;
        [self.feedbackTextView resignFirstResponder];
        [FRProgressHUD showMessageHudWithText:@"Feedback sent" inView:self.view delay:2.5f];
        [self.progressActivityIndicator stopAnimating];
    }];
}

@end
