//
//  FRNotificationsService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, notificationType){
    notificationTypeNewMealAdded = 101,
    notificationTypeNewOrder,
    notificationTypeNewMessage,
    notificationTypeNewComment
};

@interface FRNotificationsService : NSObject

- (void)registerForNotifications;
- (void)processNotification:(NSDictionary *)userInfo;
- (void)processNotificationFromAppLaunch:(NSDictionary *)userInfo;

- (void)processPushNotificationToken:(NSData *)token;

@end
