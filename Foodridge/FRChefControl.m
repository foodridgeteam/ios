//
//  FRChefControl.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRChefControl.h"

@implementation FRChefControl

- (UIImage *)fillImage{
    return self.useWhiteChefHat ? [UIImage imageNamed:@"chef_hat_white"] : [UIImage imageNamed:@"chef_rate"];
}

- (UIImage *)emptyImage{
    return [UIImage imageNamed:@"chef_rate_empty"];
}

@end
