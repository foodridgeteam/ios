//
//  FRNetworkService+Profile.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/8/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService.h"
#import "FRMealModel.h"

@class FRProfileModel, FRChefProfileModel;

@interface FRNetworkService (Profile)

+ (NSURLSessionTask *)myProfileWithCompletion:(FRResponceBlock)completion;
+ (NSURLSessionTask *)myChefWithCompletion:(FRResponceBlock)completion;


+ (NSURLSessionTask *)profileWithUid:(NSInteger)uid
                          completion:(FRResponceBlock)completion;

//not changed fields should be nil in model
+ (NSURLSessionTask *)updateProfile:(FRProfileModel *)model
                          completion:(FRResponceBlock)completion;

//not changed fields should be nil in model
+ (NSURLSessionTask *)updateChefProfile:(FRChefProfileModel *)model
                         completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)saveMyMeal:(FRViewModel <FRMealModel> *)model
                             completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)updatePushToken:(NSString *)token
                           completion:(FRResponceBlock)completion;


@end
