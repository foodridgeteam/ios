//
//  FRMealModel.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/7/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMealModel.h"



@implementation FRMealModel

@synthesize uid;
@synthesize title;
@synthesize mealDescription;
@synthesize imageURLs;
@synthesize price;
@synthesize deliveryType;
@synthesize discountsActive;
@synthesize servingsLeft;
@synthesize reviewsCount;
@synthesize chefTitle;
@synthesize chefImageURL;
@synthesize chefLastVisit;
@synthesize chefRating;
@synthesize cuisines;
@synthesize flags;
@synthesize ingridients;
@synthesize spiceRate;
@synthesize otherFlags;
@synthesize deliveryPrice;
@synthesize listed;
@synthesize chefID;
@synthesize deliveryInfo;

+(NSArray *)flagNames{
    
    static NSArray *flags;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        flags =   @[@"breakfast",
                    @"lunch",
                    @"dinner",
                    @"bakery",
                    @"vegetarian",
                    @"egg",
                    @"glutenFree",
                    @"tracesNuts",
                    @"garlic",
                    @"ginger"];
    });
    return flags;
}

- (void)setFlag:(FRMealFlag)flag value:(BOOL)value{
// this is not model to edit. FRMealExtendedModel
}

- (BOOL)flagValue:(FRMealFlag)flag{
    return [self.flags[[self.class flagNames][flag-1]] boolValue];
}


@end
