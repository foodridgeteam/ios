//
//  FRBusinessDetailsViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRBusinessDetailsViewController.h"
#import "FRBuisinessInfoModel.h"

@interface FRBusinessDetailsViewController ()<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextField *registrationnumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *webSiteTextField;


@end

@implementation FRBusinessDetailsViewController

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if(textField == self.titleTextField){
        [self.registrationnumberTextField becomeFirstResponder];
    }else if(textField == self.registrationnumberTextField){
        [self.phoneTextField becomeFirstResponder];
    }else if(textField == self.phoneTextField){
        [self.webSiteTextField becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}
- (void)setupModel{
    [super setupModel];
    self.titleTextField.text = self.model.title;
    self.registrationnumberTextField.text = self.model.registrationID;
    self.webSiteTextField.text = self.model.url;
    self.phoneTextField.text = self.model.phone;
}

- (IBAction)saveAction:(id)sender {
    FRBuisinessInfoModel *model = [FRBuisinessInfoModel new];
    model.title = self.titleTextField.text;
    model.registrationID = self.registrationnumberTextField.text;
    model.url = self.webSiteTextField.text;
    model.phone = self.phoneTextField.text;
    
    self.onSave(model);
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
