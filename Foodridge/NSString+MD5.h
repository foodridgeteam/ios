//
//  NSString+MD5.h
//  MusketeerNew
//
//  Created by Max Deygin on 12/10/14.
//  Copyright (c) 2014 Musketeer safety net LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
