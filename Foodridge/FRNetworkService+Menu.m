//
//  FRNetworkService+Meal.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService+Menu.h"

@implementation FRNetworkService (Menu)

+ (NSURLSessionTask *)mealListWithLimit:(NSInteger)limit offset:(NSInteger)offset chefID:(NSInteger)chefID searchText:(NSString *)searchText completion:(FRResponceBlock)completion{
    NSMutableDictionary *params = [@{@"limit" : @(limit), @"offset" : @(offset)} mutableCopy];
    if(searchText.length > 0){
        params[@"search"] = searchText;
    }
    
    if(chefID > 0){
        params[@"chefId"] = @(chefID);
    }
    return [self requestWithMethod:@"GET" requestAddress:@"meal" parameters:params completion:completion];


}

+ (NSURLSessionTask *)updateMyMealWithModel:(FRViewModel<FRMealModel> *)model completion:(FRResponceBlock)completion {
    
    NSDictionary *params = [model dictionaryRepresentation];
    
    return [self requestWithMethod:@"PUT"
                    requestAddress:@"meal"
                        parameters:params
                        completion:completion];
}

+ (NSURLSessionTask *)getMealWithID:(NSInteger)mealID completion:(FRResponceBlock)completion {
    return [self requestWithMethod:@"GET" requestAddress:[NSString stringWithFormat:@"single_meal/%ld", mealID] parameters:nil completion:completion];
}


@end
