//
//  THPlaceHolderTextView.m

//
//  Created by Maxym Deygin on 5/20/15.
//  Copyright (c) Roll'n'Code (rollncode.com). All rights reserved.
//

@import UIKit;

static NSInteger kPlaceholderTag = 999;

#import "THPlaceHolderTextView.h"

@interface THPlaceHolderTextView () {
    NSString *placeholder;
    UIColor *placeholderColor;
    
@private
    UILabel *placeHolderLabel;
}

@property (nonatomic, retain) UILabel *placeHolderLabel;


@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightConstraint;

@end

@implementation THPlaceHolderTextView

@synthesize placeHolderLabel;
@synthesize placeholder;
@synthesize placeholderColor;

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    if(!self.placeholder){
        [self setPlaceholder:@""];
    }
    if(!self.placeholderColor){
        [self setPlaceholderColor:[UIColor lightGrayColor]];
        if(self.minHeigth == 0){
            self.minHeigth = self.frame.size.height;
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.cornerRadius;
}

- (id)initWithFrame:(CGRect)frame
{
    if( (self = [super initWithFrame:frame]) )
    {
        [self setPlaceholder:@""];
        [self setPlaceholderColor:[UIColor lightGrayColor]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    }
    return self;
}

- (void)textChanged:(NSNotification *)notification
{
    
    CGFloat height = [self sizeThatFits:CGSizeMake(self.frame.size.width,MAXFLOAT)].height;
    height = MAX(height, self.minHeigth);
    self.heightConstraint.constant = height;
    
    if([[self placeholder] length] == 0)
    {
        return;
    }

    if([[self text] length] == 0)
    {
        [[self viewWithTag:kPlaceholderTag] setAlpha:1];
    }
    else
    {
        [[self viewWithTag:kPlaceholderTag] setAlpha:0];
    }

}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self textChanged:nil];
}

- (void)drawRect:(CGRect)rect
{
    if( [[self placeholder] length] > 0 )
    {
        if ( placeHolderLabel == nil )
        {
            placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(8,8,self.bounds.size.width - 16,0)];
            placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
            placeHolderLabel.numberOfLines = 0;
            placeHolderLabel.font = self.font;
            placeHolderLabel.backgroundColor = [UIColor clearColor];
            placeHolderLabel.textColor = self.placeholderColor;
            placeHolderLabel.alpha = 0;
            placeHolderLabel.tag = kPlaceholderTag;
            [self addSubview:placeHolderLabel];
        }

        placeHolderLabel.text = self.placeholder;
        [placeHolderLabel sizeToFit];
        [self sendSubviewToBack:placeHolderLabel];
    }

    if( [[self text] length] == 0 && [[self placeholder] length] > 0 )
    {
        [[self viewWithTag:kPlaceholderTag] setAlpha:1];
        self.scrollEnabled = NO;
    }

    [super drawRect:rect];
}

//prevent scrolling in textView. It's autoresized.
- (void)setContentOffset:(CGPoint)contentOffset{
    [super setContentOffset:CGPointZero];
}

- (CGPoint)contentOffset{
    return CGPointZero;
}

@end
