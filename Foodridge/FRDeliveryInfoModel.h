//
//  FRDeliveryInfoModel.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/8/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"
#import <Foundation/Foundation.h>

@interface FRDeliveryInfoModel : FRViewModel

@property(nonatomic, copy) NSString *country;
@property(nonatomic, copy) NSString *city;
@property(nonatomic, copy) NSString *street;
@property(nonatomic, copy) NSString *more;

- (NSDictionary *)dictionaryRepresentation;
- (NSAttributedString *)stringRepresemtation;

@end
