//
//  ChefMenuTableViewHeader.h
//  Foodridge
//
//  Created by Dev on 5/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChefMenuTableViewHeader : UITableViewHeaderFooterView

+(instancetype)view;

@property (nonatomic) BOOL shooldShowHeaderTitle;

@end
