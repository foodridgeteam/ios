//
//  FRSpiceControl.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRRateControl.h"

@interface FRSpiceControl : FRRateControl

@property (nonatomic, assign) BOOL spicyControlForMealInfo;

@end
