//
//  FRBuisinessInfoModel.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRBuisinessInfoModel.h"

@implementation FRBuisinessInfoModel

- (NSDictionary *)dictionaryRepresentation{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if(self.title){
        params[@"name"] = self.title;
    }
    
    params[@"registrationId"] = (self.registrationID.length >0)?self.registrationID:[NSNull null];
    params[@"site"] = (self.url.length >0)?self.url:[NSNull null];
    
    
    if(self.phone){
        params[@"phone"] = self.phone;
    }
    
    if(params.allKeys.count == 0){
        return nil;
    }
    return params.copy;
}

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary{
    
    FRBuisinessInfoModel *model = [FRBuisinessInfoModel new];
    
    model.title = [dictionary objectOrNilForKey:@"name" classObject:[NSString class]];
    model.registrationID = [dictionary objectOrNilForKey:@"registrationId" classObject:[NSString class]];
    model.url = [dictionary objectOrNilForKey:@"site" classObject:[NSString class]];
    model.phone = [dictionary objectOrNilForKey:@"phone" classObject:[NSString class]];
    
    return model;
}


- (BOOL)isValid{
    return (self.title.length > 0)
    //&& (self.registrationID.length > 0)*/
    && (self.phone.length > 0);
}
@end
