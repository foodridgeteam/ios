//
//  UIImageView+RoundImage.m
//  Foodridge
//
//  Created by Dev on 5/17/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "UIImageView+BetterThanAFNetworking.h"

#import "UIImageView+RoundImage.h"

@implementation UIImageView (RoundImage)

- (void)fr_setRoundImageWithUrl:(NSURL *)url placeholderImage:(UIImage *)placeholder success:(void (^)(UIImage *))success failure:(void (^)(NSError *))failure{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    __weak typeof(self) weakSelf = self;
    
    [self fr_setImageWithUrl:url placeholderImage:placeholder success:^(UIImage *image) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        // fix this http://pasteboard.co/XWUED0l.png
        if(image.size.width != image.size.height){
            strongSelf.contentMode = UIViewContentModeScaleAspectFill;
        }else{
            strongSelf.contentMode = UIViewContentModeScaleAspectFit;
        }
        
        strongSelf.image = image;
        
        if(success){
            success(image);
        }

    } failure:failure];
    
}

@end
