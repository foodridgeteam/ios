
//
//  AppDelegate.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/SignIn.h>
#import <Stripe/Stripe.h>

#import "FRConstants.h"
#import "AppDelegate.h"
#import "FRAuthorisationService.h"
#import "FRAppearanceManager.h"
#import "FRNotificationsService.h"
#import "FRURLSchemeHandler.h"
#import <Crashlytics/Crashlytics.h>
#import "FRMainScreenService.h"
#import <Branch/Branch.h>


@import Fabric;
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Fabric with:@[[Crashlytics class]]];
    [FRAppearanceManager setupAppearance];
    [Stripe setDefaultPublishableKey:FRStripePublicKey];
    [application registerForRemoteNotifications];
    
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // params are the deep linked params associated with the link that the user clicked before showing up.
        NSLog(@"deep link data: %@", [params description]);
    }];
    
    
    NSDictionary *userInfo = [launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
    if (userInfo) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             [[FRNotificationsService new] processNotificationFromAppLaunch:userInfo];
        });
    }

    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}



- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    if([[Branch getInstance] handleDeepLink:url]){
        return YES;
    }
    
    if([[FRURLSchemeHandler new] handleURL:url]){
        
        return YES;
    }
    
    if ([[FBSDKApplicationDelegate sharedInstance] application:application
                                                       openURL:url
                                             sourceApplication:sourceApplication
                                                    annotation:annotation]) {
        return YES;
    
    }else if ([[GIDSignIn sharedInstance] handleURL:url
                                  sourceApplication:sourceApplication
                                         annotation:annotation]) {
        return YES;
        
    }
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
    
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"%@",error);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    [[FRNotificationsService new] processPushNotificationToken:deviceToken];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    [[FRNotificationsService new] processNotification:userInfo];
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    [FBSDKAppEvents activateApp];
}

-(void)application:(UIApplication *)application didUpdateUserActivity:(NSUserActivity *)userActivity{
    [[Branch getInstance] continueUserActivity:userActivity];
}

@end
