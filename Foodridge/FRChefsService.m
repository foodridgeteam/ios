//
//  RFChefsService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/29/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRChefsService.h"
#import "FRChefProfileModel.h"
#import "FRNetworkService+Chefs.h"

@implementation FRChefsService

+ (instancetype)service{
    
    FRChefsService *service = [FRChefsService new];
    service.items = [NSMutableArray new];
    service.loadMore = YES;
    return service;
}

+ (Class)modelClass{
    return [FRChefProfileModel class];
}

+ (NSString *)notificationName{
    return FRChefsUpdatedNotificationName;
}

- (NSURLSessionTask *)chefListWithSearchText:(NSString *)text
                             completion:(FRResponceBlock)completion{
    
    return [FRNetworkService chefsListWithLimit:defaultLimit offset:self.offset followed:NO searchText:text completion:^(id object, id rawObject, NSError *error) {
        [self prodcessResponce:rawObject orError:error withCompletion:completion];
    }];
}

- (NSURLSessionTask*)getChefWithId:(NSNumber *)chefId completion:(FRResponceBlock)completion {
    
    return [FRNetworkService getChefWithId:chefId completion:^(id object, id rawObject, NSError *error) {
        
        if (!error) {
            FRChefProfileModel* chefProfile = [FRChefProfileModel modelWitDictionary:object];
            
            completion(chefProfile, rawObject, nil);}
        else {
            completion(nil, nil, error);
        }
        
    }];
}

- (NSURLSessionTask*) followChefwithID:(NSNumber *)chefId
                                follow:(bool)follow
                            completion:(FRResponceBlock)completion {
    
    return [FRNetworkService followChef:chefId and:!follow completion:^(id object, id rawObject, NSError *error) {
        if (!error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FRChefFollowingUpdate" object:chefId userInfo:nil];
        }
    }];

}

@end
