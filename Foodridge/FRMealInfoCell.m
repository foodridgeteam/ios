//
//  FRMealInfoCell.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+BetterThanAFNetworking.h"
#import "UIImageView+RoundImage.h"
#import "FRMealInfoCell.h"
#import "UIButton+Localize.h"
#import "UIImage+Color.h"
#import "UIFont+FoodridgeFonts.h"
#import "UIColor+FoodridgeColors.h"
#import "FRReviewControl.h"
#import "FRPriceControl.h"
#import "FRChefControl.h"
#import "FRDateHelper.h"
#import "FRAuthorisationService.h"

@interface FRMealInfoCell ()

@property (strong, nonatomic) IBOutlet UIImageView *mealImage;
@property (strong, nonatomic) IBOutlet UIImageView *chefImage;
@property (strong, nonatomic) IBOutlet UILabel *chefName;
@property (strong, nonatomic) IBOutlet UILabel *lastVisited;
@property (strong, nonatomic) IBOutlet UIButton *orderNowButton;

@property (strong, nonatomic) IBOutlet FRReviewControl *reviewControl;
@property (strong, nonatomic) IBOutlet UILabel *mealName;

@property (strong, nonatomic) IBOutlet UIImageView *pickUpImageView;
@property (strong, nonatomic) IBOutlet UIImageView *deliveryImageView;
@property (strong, nonatomic) IBOutlet UIImageView *discountImageView;
@property (strong, nonatomic) IBOutlet UIImageView *servingsImageView;
@property (strong, nonatomic) IBOutlet UIImageView *ratingsImageView;
@property (strong, nonatomic) IBOutlet FRPriceControl *mealPriceControl;
@property (strong, nonatomic) IBOutlet FRChefControl *chefRateControl;
@property (strong, nonatomic) IBOutlet UILabel *reviewsLabel;
@property (strong, nonatomic) IBOutlet UILabel *servingsLabel;


@property (weak, nonatomic) IBOutlet UIView *gradientView;

@end

@implementation FRMealInfoCell

- (void)awakeFromNib {
    
    self.chefImage.layer.masksToBounds = YES;
    self.chefImage.layer.cornerRadius = self.chefImage.frame.size.width/2;
    
    [self.orderNowButton localizeWithOptions:THLocalizationOptionsUppercase];
    self.orderNowButton.layer.cornerRadius = 3;
    self.orderNowButton.layer.masksToBounds = YES;
    
    self.mealPriceControl.textColor = [UIColor blackColor];
}

- (void)setupCellWithMealModel:(FRMealExtendedModel *)meal {
    
    __weak typeof(self) weakSelf = self;
    [self.mealImage fr_setImageWithUrl:[NSURL URLWithString:meal.imageURLs.firstObject] placeholderImage:[UIImage imageNamed:@"dish_empty"] success:^(UIImage *image) {
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        strongSelf.mealImage.image = image;
        
        [UIView animateWithDuration:0.1 animations:^{
            strongSelf.mealPriceControl.textColor = [UIColor whiteColor];
            
            [strongSelf addGradientToView:strongSelf.gradientView];
        }];
        
    } failure:^(NSError *error) {}];
    
    [self.chefImage fr_setRoundImageWithUrl:[NSURL URLWithString:meal.chefImageURL] placeholderImage:[UIImage imageNamed:@"ic_no_user"] success:^(UIImage *image) {} failure:^(NSError *error) {}];
    
    self.pickUpImageView.highlighted = (meal.deliveryType.integerValue == FRDeliveryTypePickup || meal.deliveryType.integerValue == FRDeliveryTypePickupAndDelivery) ? YES : NO;
    
    self.deliveryImageView.highlighted = (meal.deliveryType.integerValue == FRDeliveryTypePickupAndDelivery || meal.deliveryType.integerValue == FRDeliveryTypeDelivery) ? YES : NO;
    
    self.servingsImageView.highlighted = meal.servingsLeft.integerValue > 0 ? YES : NO;
    
    self.ratingsImageView.highlighted = YES;// meal.reviewsCount.integerValue > 0 ? YES : NO;
    
    self.lastVisited.text = [NSString stringWithFormat:NSLocalizedString(@"STR_LAST_VISITED", nil),[FRDateHelper dateStringFromDate:meal.chefLastVisit]];
    
    self.reviewsLabel.text = [NSString stringWithFormat:NSLocalizedString(@"STR_REVIEWS", nil), meal.reviewsCount];
    
    NSMutableAttributedString *servingsLeftAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", meal.servingsLeft, NSLocalizedString(@"STR_SERVING_LEFT", nil)]];
    [servingsLeftAttributedString addAttribute:NSFontAttributeName value:[UIFont openSansBoldFontWithSize:17] range:NSMakeRange(0, meal.servingsLeft.stringValue.length)];
    self.servingsLabel.attributedText = servingsLeftAttributedString;
    self.mealName.text = meal.title;
   
    self.chefName.text = meal.chefTitle;
    self.mealPriceControl.price = meal.price;
    self.chefRateControl.value = meal.chefRating.integerValue;
    self.reviewControl.value = meal.averageRating.integerValue;
    
    if (meal.servingsLeft.integerValue > 0) {
        
        [self.orderNowButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
        [self.orderNowButton setTitle:NSLocalizedString(@"STR_ORDER_NOW", nil) forState:UIControlStateNormal];
        
    } else {
        
        [self.orderNowButton setImage:[UIImage imageNamed:@"order_icon"] forState:UIControlStateNormal];
        [self.orderNowButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
        [self.orderNowButton setTitle:NSLocalizedString(@"STR_SOLD_OUT", nil) forState:UIControlStateNormal];
    }
    
    self.orderNowButton.hidden = meal.chefID.integerValue == [FRAuthorisationService currentUID];

}

- (IBAction)orderNowAction:(id)sender {
    
}

- (void)addGradientToView:(UIView *)view
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
    gradient.colors = @[
                        (id)[[UIColor clearColor] CGColor],
                        (id)[[[UIColor blackColor] colorWithAlphaComponent:0.65f] CGColor],
                        (id)[[UIColor blackColor] CGColor]
                        ];

    gradient.opacity = 0.8f;
    
    [view.layer insertSublayer:gradient atIndex:0];
}

@end
