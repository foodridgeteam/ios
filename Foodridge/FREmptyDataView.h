//
//  FREmptyDataView.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FREmptyDataViewType) {
    FREmptyDataViewTypeReceived,
    FREmptyDataViewTypePlaced,
    FREmptyDataViewTypeCompleted,
    FREmptyDataTypeChefs,
    FREmptyDataTypeMeals
};

@interface FREmptyDataView : UIView

@property (nonatomic, assign) NSInteger filterType;

@end
