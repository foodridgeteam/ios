//
//  FRBusinessDetailsViewController.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRSelectionFormViewController.h"

@class FRBuisinessInfoModel;
@interface FRBusinessDetailsViewController : FRSelectionFormViewController

@property(strong, nonatomic) FRBuisinessInfoModel *model;

@property (nonatomic, copy) void (^onSave)(FRBuisinessInfoModel *changes);


@end
