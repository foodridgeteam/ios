//
//  FRWorkHoursEditingView.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface FRWorkHoursEditingView : UIView

- (void)setupWithOpenDate:(NSDate *)openDate closeDate:(NSDate *)closeDate;

@property (nonatomic, copy) void (^onDone)(BOOL canceed, NSDate *openDate, NSDate *closeDate);
@property(nonatomic, assign) NSInteger dayIndex;

@end
