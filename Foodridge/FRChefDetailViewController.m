//
//  ChefDetailViewController.m
//  Foodridge
//
//  Created by NekitBOOK on 25.04.16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <libextobjc/extobjc.h>

#import "FRChefDetailViewController.h"
#import "ChefMenuTableViewHeader.h"
#import "FRChefsService.h"
#import "ChefMenuTableViewCell.h"
#import "FRDeliveryInfoModel.h"
#import "FRBuisinessInfoModel.h"
#import "FRWeekScheduleModel.h"
#import "FRNetworkService+Chefs.h"
#import "UIImage+Color.h"
#import "FRMealExtendedModel.h"
#import "UIColor+FoodridgeColors.h"
#import "FRCuisineModel.h"
#import "FRMealsService.h"
#import "FRChefsService.h"
#import "FRMealInfoViewController.h"
#import "FROrderComposeViewController.h"
#import "FRAuthorisationService.h"
#import "FRChefControl.h"
#import "NSString+DateFormats.h"
#import "UIFont+FoodridgeFonts.h"
#import "FRDateHelper.h"

@class FRChefControl;

@interface FRChefDetailViewController () <UITableViewDataSource, UITableViewDelegate, FRChefMenuCellDelegate>

@property (weak, nonatomic) IBOutlet FRChefControl *chefRate;

@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIImageView *chefPhoto;

@property (weak, nonatomic) IBOutlet UILabel *chefBioLabel;
@property (weak, nonatomic) IBOutlet UILabel *chefFollowersCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *chefReviewsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *cuisinesLabel;

@property (weak, nonatomic) IBOutlet UIView *tableViewHeader;

@property (weak, nonatomic) IBOutlet UILabel *deliveryAdressCityLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAdressCountryLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAdressStreetLabel;

@property (weak, nonatomic) IBOutlet UILabel *businessDetailsNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *businessDetailsPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *businessDetailsRegIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *businessDetailsSiteLabel;
@property (strong, nonatomic) IBOutlet UIImageView *followersStarImageView;

@property (strong, nonatomic) IBOutletCollection (UILabel) NSArray* openingHoursLabels;

@end

@implementation FRChefDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UINib* cellNib = [UINib nibWithNibName:@"ChefMenuTableViewCell" bundle:nil];
    
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ChefMenuCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadChef) name:@"FRChefFollowingUpdate" object:nil];
  
    
    if (self.chefProfile) {
        [self loadChefData];
        [self.tableView reloadData];
        
    }
    
    [[FRMealsService service] mealListWithChefID:[self.chefUid integerValue] searchText:nil completion:^(id object,id rawObject, NSError *error) {
        self.data = object;
        [self reloadChef];
        [self.tableView reloadData];
    }];
    
    self.navigationController.navigationBar.topItem.title = @"";
}



- (void)setupNavigationBarTitle {
    UILabel *titleViewLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                        0,
                                                                        self.navigationController.navigationBar.frame.size.width,
                                                                        self.navigationController.navigationBar.frame.size.height)];
    titleViewLabel.backgroundColor = [UIColor clearColor];
    titleViewLabel.numberOfLines = 2;
    titleViewLabel.font = [UIFont openSansBoldFontWithSize:12];
    titleViewLabel.textAlignment = NSTextAlignmentLeft;
    titleViewLabel.textColor = [UIColor whiteColor];
    
    NSString *lastVisitString = [NSString stringWithFormat:NSLocalizedString(@"STR_LAST_VISITED", nil),[FRDateHelper dateStringFromDate:self.chefProfile.lastVisit]];
    
    NSMutableAttributedString *titleTextAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@", self.chefProfile.chefName, lastVisitString]];
    [titleTextAttributedString addAttribute:NSFontAttributeName value:[UIFont openSansFontWithSize:17] range:NSMakeRange(0, self.chefProfile.chefName.length)];
    [titleTextAttributedString addAttribute:NSFontAttributeName value:[UIFont openSansFontWithSize:10] range:NSMakeRange(self.chefProfile.chefName.length + 1, lastVisitString.length)];
    [titleTextAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, self.chefProfile.chefName.length)];
    [titleTextAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(self.chefProfile.chefName.length + 1, lastVisitString.length)];
    
    titleViewLabel.attributedText = titleTextAttributedString;
    self.navigationItem.titleView = titleViewLabel;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setupNavigationBarTitle];
}

- (void)reloadChef {
    
    if (self.chefUid) {
        
        @weakify(self);
        [[FRChefsService service] getChefWithId:self.chefUid completion:^(id object, id rawObject, NSError *error) {
            if (!error) {
                @strongify(self);
                self.chefProfile = object;
                [self loadChefData];
                [self.activityIndicator  stopAnimating];
                self.followButton.enabled = YES;
            } else {
                NSLog(@"%@",error);
            }
        }];
    }
  
}

- (void) loadChefData {
    
    self.chefRate.value = self.chefProfile.chefRating.integerValue;
    
    NSDateFormatter *dateFormatter =  [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    if ([self.chefProfile.follow boolValue]) {
        [self.followButton setTitle:NSLocalizedString(@"STR_UNFOLLOW", nil) forState:UIControlStateNormal];
        [self.followButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    } else {
        [self.followButton setTitle:NSLocalizedString(@"STR_FOLLOW", nil) forState:UIControlStateNormal];
        [self.followButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    }
    
    [self.followButton setImage:[UIImage imageNamed:@"roundedStarWhite"] forState:UIControlStateNormal];
    
    self.followButton.hidden = self.chefProfile.uid.integerValue == [FRAuthorisationService currentUID] ? YES : NO;
    
    self.chefBioLabel.text = self.chefProfile.bio;
    
    if (self.chefProfile.avatarURL != nil) {
        self.chefPhoto.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.chefProfile.avatarURL]]]; }
    
    //self.chefQualificationLabel.text = self.chefProfile.stripeId;
    
    NSString *reviews = self.chefProfile.reviewsCount.integerValue > 1 ? @"Reviews" : @"Review";
    self.chefReviewsCountLabel.text = self.chefProfile.reviewsCount.integerValue ? [NSString stringWithFormat:@"%ld %@", (long)[self.chefProfile.reviewsCount integerValue], reviews] : @"Reviews";
    
    NSString *followers = self.chefProfile.followersCount.integerValue > 1 ? @"Followers" : @"Follower";
    self.chefFollowersCountLabel.text = self.chefProfile.followersCount.integerValue ? [NSString stringWithFormat:@"%ld %@", (long)[self.chefProfile.followersCount integerValue], followers] : @"";
    self.followersStarImageView.hidden = self.chefProfile.followersCount.integerValue ? NO : YES;
  
    NSMutableArray* cuisinesList = [NSMutableArray new];
    for (FRCuisineModel* cuisine in self.chefProfile.cuisines) {
        [cuisinesList addObject: cuisine.name];
    }
    
    if (cuisinesList.count !=0) {
        self.cuisinesLabel.text = [cuisinesList componentsJoinedByString:@", "];
    } else {
        self.cuisinesLabel.text = @"Nothing here yet";
    }
    
    self.deliveryAdressCityLabel.text = [NSString stringWithFormat:@"%@\n",self.chefProfile.pickupInfo.city];
    self.deliveryAdressCountryLabel.text = [NSString stringWithFormat:@"%@\n",self.chefProfile.pickupInfo.country];
    self.deliveryAdressStreetLabel.text = [NSString stringWithFormat:@"%@\n",self.chefProfile.pickupInfo.street];
    
    self.businessDetailsNameLabel.text = self.chefProfile.businessInfo.title;
    self.businessDetailsRegIdLabel.text = self.chefProfile.businessInfo.registrationID;
    self.businessDetailsSiteLabel.text = self.chefProfile.businessInfo.url;
    self.businessDetailsPhoneLabel.text = self.chefProfile.businessInfo.phone;
    
    for(UILabel *l in self.openingHoursLabels){
        if ([self.chefProfile.weekSchedule openDateForDayAtIndex:l.tag] !=nil ){
            [l setAttributedText:[self.chefProfile.weekSchedule intervalStringForDayAtIndex:l.tag]];
            l.textColor = [UIColor blackColor];
        } else {
            l.font = [UIFont openSansBoldFontWithSize:14];
            l.textColor = [UIColor redColor];
            l.text = @"Closed";
        }
    }
}

- (IBAction)followButtonPressed:(id)sender {
    
    self.followButton.enabled = NO;
    [self.activityIndicator  startAnimating];
    [self.followButton setImage:nil forState:UIControlStateNormal];
        
    [[FRChefsService service] followChefwithID:self.chefUid follow:[self.chefProfile.follow boolValue] completion:nil];

}


#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ( self.data.count > 0 ) {
        return self.data.count;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ChefMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChefMenuCell"];
    
    [cell setupCellWithMealModel:self.data[indexPath.section]];

    cell.delegate = self;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 164;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 0 ? 40 : 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == self.data.count-1) {
      return 20;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return  [ChefMenuTableViewHeader view];
    }
    
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Menu" bundle:nil];
    FRMealInfoViewController *vc = [sb instantiateViewControllerWithIdentifier:@"FRMealInfo"];
    vc.meal = [self.data objectAtIndex:indexPath.section];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - FRChefMenuCellDelegate 

- (void)orderNowButtonClicked:(ChefMenuTableViewCell *)cell {
    NSIndexPath *ip = [self.tableView indexPathForCell:cell];
    id<FRMealModel> meal = self.data[ip.section];
    
    [FROrderComposeViewController presentWithMeal:meal fromViewController:self];
}

@end
