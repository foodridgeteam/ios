//
//  FRChefProfileModel.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"
#import <Foundation/Foundation.h>

@class FRDeliveryInfoModel, FRBuisinessInfoModel, FRWeekScheduleModel;
@interface FRChefProfileModel : FRViewModel

@property(nonatomic, strong) NSNumber *uid;
@property(nonatomic, strong) FRDeliveryInfoModel *pickupInfo;
@property(nonatomic, strong) FRBuisinessInfoModel *businessInfo;
@property(nonatomic, strong) NSString *bio;
@property(nonatomic, strong) NSArray *cuisines;
@property(nonatomic, strong) FRWeekScheduleModel *weekSchedule;
@property(nonatomic, strong) NSString *stripeId;

@property(nonatomic, copy) NSString *chefName;
@property(nonatomic, copy) NSNumber *follow;
@property(nonatomic, copy) NSNumber *followersCount;
@property(nonatomic, copy) NSNumber *reviewsCount;
@property(nonatomic, copy) NSString *avatarURL;
@property(nonatomic, copy) NSNumber *chefRating;
@property(nonatomic, copy) NSDate *lastVisit;

@end
