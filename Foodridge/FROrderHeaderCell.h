//
//  FROrderHeaderCell.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FROrderModel.h"

@protocol FROrderHeaderCellDelegate <NSObject>

- (void)callPhoneNumber:(NSString*)number name:(NSString*)name;

@end

@interface FROrderHeaderCell : UITableViewCell
@property (strong, nonatomic) id<FROrderHeaderCellDelegate> delegate;

-(void)setupWithOrder:(FROrderModel *)order;

@end
