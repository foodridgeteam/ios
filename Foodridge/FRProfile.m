//
//  FRProfile.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRProfile.h"

@implementation FRProfile

+ (NSString *)primaryKey{
    return @"uid";
}

@end
