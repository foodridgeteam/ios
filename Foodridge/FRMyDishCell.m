//
//  FRMyDishCell.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMyDishCell.h"
#import "NSArray+Localize.h"
#import "RNCCheckbox.h"
#import "UIImage+Color.h"
#import "UIColor+FoodridgeColors.h"
#import "FRChefControl.h"
#import "FRAppearanceManager.h"
#import "FRCuisineModel.h"

@import KSTokenView;

@interface FRMyDishCell () <KSTokenViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *localizable;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIButton *reviewsButton;
@property (strong, nonatomic) IBOutlet RNCCheckbox *listCheckBox;
@property (strong, nonatomic) IBOutlet UILabel *portionsSoldLabel;
@property (strong, nonatomic) IBOutlet FRChefControl *chefRatingControl;
@property (strong, nonatomic) IBOutlet KSTokenView *cuisinesView;

@end

@implementation FRMyDishCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.localizable localize];
    [self.shareButton setBackgroundImage:[UIImage imageWithColor:[UIColor grayButtonColor]] forState:UIControlStateNormal];
    self.cuisinesView.delegate = self;
}

- (void)setupCellWithMealModel:(FRMealExtendedModel *)meal {
    
    self.cellModel = meal;
    self.titleLabel.text = meal.title;
    self.portionsSoldLabel.text = meal.portionsSold.stringValue;
    self.chefRatingControl.value = meal.averageRating.integerValue;
    self.listCheckBox.on = [meal.listed boolValue];
    [self.reviewsButton setTitle:meal.reviewsCount.stringValue forState:UIControlStateNormal];
    
    for(FRCuisineModel *cuisine in meal.cuisines){
        [[self.cuisinesView addTokenWithTitle:cuisine.name tokenObject:cuisine] setTokenBackgroundColor:[UIColor aquaColor]];
    }
    
    [FRAppearanceManager setupKSTokenView:self.cuisinesView];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.chefRatingControl.value = 0;
    
    [self.cuisinesView deleteAllTokens];
}

- (BOOL)tokenView:(KSTokenView *)tokenView shouldAddToken:(KSToken *)token{
    
    if(!token.object){
        return NO;
    }
    token.icon = [UIImage imageNamed:@"cuisineWhite"];
    return YES;
}

- (IBAction)editAction:(id)sender {
    [self.delegate editDishInCell:self];
}

-(IBAction)shareButtonPressed:(id)sender {
    [self.delegate shareActionInCell:self];
}

- (IBAction)listAction:(RNCCheckbox*)sender {    
    self.cellModel.listed = @(sender.isOn);
    [self.delegate updateMealListed:self];
}

@end
