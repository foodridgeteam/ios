//
//  FRNetworkService+Comments.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService.h"

@interface FRNetworkService (Comments)

+ (NSURLSessionTask *)getCommentsForMealWithID:(NSInteger)mealID completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)leaveCommentForOrderWithID:(NSInteger)orderID
                                     commentText:(NSString *)commentText
                                          rating:(NSInteger)rating
                                     completion:(FRResponceBlock)completion;

@end
