//
//  FRMealsService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/29/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRDynamicDataLoadService.h"

static  NSString *FRMealsUpdatedNotificationName = @"FRMealsUpdatedNotification";


@interface FRMealsService : FRDynamicDataLoadService

- (NSURLSessionTask *)mealListWithChefID:(NSInteger)chefID
                             searchText:(NSString *)text
                             completion:(FRResponceBlock)completion;

- (NSURLSessionTask *)getMealWithID:(NSInteger)mealID
                         completion:(FRResponceBlock)completion;

@end
