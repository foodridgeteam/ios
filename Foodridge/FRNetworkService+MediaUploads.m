//
//  FRNetworkService+MediaUploads.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import AFNetworking;

#import "FRNetworkService+MediaUploads.h"
#import "FRAuthorisationService.h"
#import "NSDictionary+NullToNil.h"

@implementation FRNetworkService (MediaUploads)

+ (NSURLSessionTask *)uploadMedia:(NSString *)localPath
                             type:(FRMediaType)type
                              uid:(NSInteger)uid
                       completion:(FRResponceBlock)completion{
    
    NSString *mediaTypeUrl = (type == FRMediaTypeAvatar) ? @"upload_avatar" : @"meal_media" ;
    NSString *str = [baseURL stringByAppendingPathComponent:mediaTypeUrl];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = [self boundaryString];
    [request addValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
    
    NSData *fileData = [NSData dataWithContentsOfFile:localPath];
    NSData *data = [self createBodyWithBoundary:boundary username:nil password:nil data:fileData filename:[localPath lastPathComponent] mealID:uid];
    
    NSString *token = [FRAuthorisationService accessToken];

    if(token){
        [request setValue:token forHTTPHeaderField:@"token"];
        [request setValue:[NSString stringWithFormat:@"%ld",(long) [FRAuthorisationService currentUID]] forHTTPHeaderField:@"uid"];
    }

    NSURLSessionDataTask *dataTask = [[self sessionManager] uploadTaskWithRequest:request fromData:data progress:^(NSProgress * _Nonnull uploadProgress) {
        NSString *string = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"%@",string);
    } completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
    
        NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
        if([resp statusCode] != 200){
            error = [NSError errorWithDomain:[NSHTTPURLResponse localizedStringForStatusCode:resp.statusCode] code:resp.statusCode userInfo:nil];
        }
        
        if (!error) {
            NSLog(@"%@:%@\n %@",request.URL, response, responseObject);
            
            NSDictionary *data = [responseObject objectOrNilForKey:@"data"];
            if(!data){
                [responseObject objectOrNilForKey:@"data"];
            }
            
            if([data isKindOfClass:[NSDictionary class]]){
                completion(data, data, nil);
            }else if([data isKindOfClass:[NSString class]]){
                completion(data, data, nil);
                
            }else{
                NSDictionary *errorData = [responseObject objectOrNilForKey:@"error" classObject:[NSDictionary class]];
                NSString *msg = [errorData objectOrNilForKey:@"message" classObject:[NSString class]];
                NSInteger code = [[errorData objectOrNilForKey:@"code" classObject:[NSNumber class]] integerValue];
                if(code == 0){
                    code = 500;
                }
                if(!msg){
                    msg = @"Unknown error";
                }
                error = [NSError errorWithDomain:msg code:code userInfo:errorData];
                completion(nil, data, error);
            }
            
            
        } else {
            
            completion(nil, nil, error);
            NSLog(@"Error: %@", error);
        }
    }];
    [dataTask resume];
    return dataTask;

}

+ (NSData *) createBodyWithBoundary:(NSString *)boundary username:(NSString*)username password:(NSString*)password data:(NSData*)data filename:(NSString *)filename mealID:(NSInteger)mealID
{
    NSMutableData *body = [[NSMutableData alloc] initWithCapacity:0];
    
    if (data) {
        //only send these methods when transferring data as well as username and password
        if (mealID != 0) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"id"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%ld\r\n", (long)mealID] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n;", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", [self mimeTypeForPath:filename]] dataUsingEncoding:NSUTF8StringEncoding]];

        [body appendData:data];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    if (!data && mealID != 0) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"id"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%ld\r\n", (long)mealID] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];


    return body;
}

+ (NSString *)boundaryString
{
    NSString *uuidStr = [[NSUUID UUID] UUIDString];
    
     return [NSString stringWithFormat:@"Boundary-%@", uuidStr];
}

+ (NSString *)mimeTypeForPath:(NSString *)path
{
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}

#pragma mark Delete media

+ (NSURLSessionTask *)deleteMediaWithName:(NSString *)mediaName forMealWithID:(NSInteger)mealID completion:(FRResponceBlock)completion {
    
    NSString *str = [baseURL stringByAppendingPathComponent:@"meal_media"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",str,mediaName]]];
    [request setHTTPMethod:@"DELETE"];
    
    NSString *boundary = [self boundaryString];
    [request addValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
    
   
    NSData *data = [self createBodyWithBoundary:boundary username:nil password:nil data:nil filename:nil mealID:mealID];
    
    NSString *token = [FRAuthorisationService accessToken];
    
    if(token){
        [request setValue:token forHTTPHeaderField:@"token"];
        [request setValue:[NSString stringWithFormat:@"%ld",(long) [FRAuthorisationService currentUID]] forHTTPHeaderField:@"uid"];
    }
    
    NSURLSessionDataTask *dataTask = [[self sessionManager] uploadTaskWithRequest:request fromData:data progress:^(NSProgress * _Nonnull uploadProgress) {
    } completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (!error) {
            completion(nil, data, error);
        }
    }];
    [dataTask resume];
    
    return dataTask;
}

@end
