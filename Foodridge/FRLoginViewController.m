//
//  FRLoginViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRLoginViewController.h"
#import "UIImage+Color.h"
#import "UIColor+FoodridgeColors.h"

#import "FRFacebookLoginService.h"
#import "FRGoogleLoginService.h"
#import "FRTermsService.h"
#import "FRAuthorisationService.h"
#import "FRMainScreenService.h"

@interface FRLoginViewController ()

@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong, nonatomic) IBOutlet UIButton *gPlusButton;
@property (strong, nonatomic) IBOutlet UIButton *emailButton;
@property (strong, nonatomic) IBOutlet UIButton *signupButton;

@property (strong, nonatomic) NSURLSessionTask *currentTask;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *facebookActivity;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *googleActivity;

@end

@implementation FRLoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

}

- (void)setupNavigationBar{
    UINavigationBar *bar = self.navigationController.navigationBar;
    [bar setBackgroundImage:[UIImage new]
              forBarMetrics:UIBarMetricsDefault];
    bar.shadowImage = [UIImage new];
    bar.translucent = YES;
    bar.tintColor = [UIColor whiteColor];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setupNavigationBar];

    [self.emailButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    [self.signupButton setBackgroundImage:[UIImage imageWithColor:[UIColor grayButtonColor]] forState:UIControlStateNormal];
}

- (IBAction)facebookAction:(id)sender {
    [self cancelCurrentTask];
    [self.facebookActivity startAnimating];
    [[FRFacebookLoginService new] loginFromViewController:self completion:^(NSString *userID, NSString *token, NSError *error) {
        
        if(userID && token){
            [self facebookLoginSuccess:userID token:token];
        }else if (error) {
           
            [self.facebookActivity stopAnimating];
            
        }
    }];
    
}

- (IBAction)gPlusAction:(id)sender {
    [self cancelCurrentTask];
    [self.googleActivity startAnimating];
    
    [[FRGoogleLoginService service] loginFromViewController:self completion:^(NSString *token, NSError *error) {
        
        if(token){
            [self googlekLoginSuccessWithToken:token];
        }else{
            [self.googleActivity stopAnimating];
        }
    }];
    
    
}

- (IBAction)emailAction:(id)sender {
    [self cancelCurrentTask];
    
}
- (IBAction)signupAction:(id)sender {
    [self cancelCurrentTask];
}

- (void)cancelCurrentTask{
    if(!self.currentTask){
        return;
    }
    [self.currentTask cancel];
    self.currentTask = nil;
    [self.facebookActivity stopAnimating];
    [self.googleActivity stopAnimating];
}


- (void)facebookLoginSuccess:(NSString *)uid 
                       token:(NSString *)facebookToken{

    
    
    self.currentTask = [[FRAuthorisationService service] authWithFacebookToken:facebookToken userID:uid completion:^(id object, id rawObject, NSError *error) {
        
        self.currentTask = nil;
        [self.facebookActivity stopAnimating];
        if(!error){
            
            [[FRMainScreenService new] showLoadingScreen];
            
        }else{
            
        }
    }];
}

- (void)googlekLoginSuccessWithToken:(NSString *)googleToken{
    
    
    [self.googleActivity startAnimating];
    self.currentTask = [[FRAuthorisationService service] authWithGooglePlusToken:googleToken completion:^(id object, id rawObject, NSError *error) {
        
        self.currentTask = nil;
        [self.googleActivity stopAnimating];
        if(!error){
            
            [[FRMainScreenService new] showLoadingScreen];
            
        }else{
            
        }
    }];
}

@end
