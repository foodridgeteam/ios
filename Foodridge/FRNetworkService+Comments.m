//
//  FRNetworkService+Comments.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService+Comments.h"

@implementation FRNetworkService (Comments)

+ (NSURLSessionTask *)getCommentsForMealWithID:(NSInteger)mealID completion:(FRResponceBlock)completion {
    
    return [self requestWithMethod:@"GET" requestAddress:[NSString stringWithFormat:@"comments/%ld",(long)mealID] parameters:nil completion:completion];
}

+ (NSURLSessionTask *)leaveCommentForOrderWithID:(NSInteger)orderID commentText:(NSString *)commentText rating:(NSInteger)rating completion:(FRResponceBlock)completion {
    NSMutableDictionary *params = [@{@"orderId" : @(orderID),
                                     @"text" : commentText,
                                     @"rate" : @(rating)} mutableCopy];
    
    return [self requestWithMethod:@"POST" requestAddress:@"comment" parameters:params completion:completion];
}

@end
