//
//  FRNetworkService+Auth.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService.h"

@interface FRNetworkService (Auth)

+(NSURLSessionTask *)authWithEmail:(NSString *)email
                          password:(NSString *)password
                          deviceId:(NSString *)deviceId
                        completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)registerWithEmail:(NSString *)email
                               username:(NSString *)username
                               password:(NSString *)password
                               deviceId:(NSString *)deviceId
                             completion:(FRResponceBlock)completion;


+ (NSURLSessionTask *)authWithFacebookToken:(NSString *)token
                                        uid:(NSString *)uid
                                   deviceId:(NSString *)deviceId
                                 completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)authWithGooglePlusToken:(NSString *)token
                                     deviceId:(NSString *)deviceId
                                   completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)forgotPasswordWithEmail:(NSString *)email
                                   completion:(FRResponceBlock)completion;


+ (NSURLSessionTask *)termsAggreedCompletion:(FRResponceBlock)completion;


+ (void)logout;


@end
