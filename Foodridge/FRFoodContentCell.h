//
//  FRFoodContentCell.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRMealExtendedModel.h"

@interface FRFoodContentCell : UITableViewCell

@property (assign, nonatomic) BOOL hideSpicyControl;

- (void)setFoodContentCellWithFlag:(NSString *)flag mealModel:(FRMealExtendedModel *)meal;

@end
