//
//  FRSignInViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//
#import <libextobjc/extobjc.h>

#import "FRSignInViewController.h"
#import "UIImage+Color.h"
#import "UIColor+FoodridgeColors.h"
#import "NSString+Email.h"
#import "FRAuthorisationService.h"
#import "FRMainScreenService.h"
#import "FRConstants.h"


@interface FRSignInViewController ()

@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) IBOutlet UIButton *signupButton;
@property (strong, nonatomic) IBOutlet UIButton *forgotPaswordButton;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (strong, nonatomic) IBOutlet UILabel *alertLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic)  UIButton *cancelButton;
@property (strong, nonatomic) NSURLSessionTask *currentTask;


@end

@implementation FRSignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.forgotPaswordButton setBackgroundImage:[UIImage imageWithColor:[UIColor grayButtonColor]] forState:UIControlStateNormal];
    [self.signupButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];


}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self addKeyboardObservers];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self removeKeyboardObservers];
    [self cancelLoading];
}

- (void)addKeyboardObservers {
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillChangeFrame:)
     name:UIKeyboardWillChangeFrameNotification object:nil];
}
- (void)removeKeyboardObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)keyboardWillChangeFrame:(NSNotification*)notification {
    [self adjustViewForKeyboardNotification:notification];
}

- (void)adjustViewForKeyboardNotification:(NSNotification *)notification {
    NSDictionary *notificationInfo = [notification userInfo];
    
    CGRect finalKeyboardFrame = [[notificationInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    finalKeyboardFrame = [self.view convertRect:finalKeyboardFrame fromView:self.view.window];
    
    [self adjustViewsForKeyboardFrame:finalKeyboardFrame];
    
}

- (void)adjustViewsForKeyboardFrame:(CGRect)keyboardFrame {
    // Calculate new position of the view
    NSLog(@"keyboardFrameChanged");
    self.bottomConstraint.constant = CGRectGetHeight(self.view.frame) - CGRectGetMinY(keyboardFrame);
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    self.alertLabel.hidden = YES;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == self.emailTextField){
        [self.passwordTextField becomeFirstResponder];
    }else {
        [self.passwordTextField resignFirstResponder];
        [self signInAction:nil];
    }
    return YES;
}

-(void)activateCancelButton{
    if(!self.cancelButton){
        self.cancelButton = [[UIButton alloc] initWithFrame:self.view.bounds];
        [self.cancelButton addTarget:self action:@selector(cancelLoading) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.cancelButton];
        
    }
    self.signupButton.hidden = YES;
    [self.activityIndicator startAnimating];
    self.cancelButton.hidden = NO;
    
}
-(void)hideCancelButton{
    [self.activityIndicator stopAnimating];
    self.signupButton.hidden = NO;
    self.cancelButton.hidden = YES;
}

- (void)cancelLoading{
    if(self.currentTask){
        [self.currentTask cancel];
        [self hideCancelButton];
    }
    self.currentTask = nil;
}

- (BOOL)validateInput:(BOOL)forgotPassword{
    if([self.emailTextField.text length] == 0){
        [self.emailTextField becomeFirstResponder];
        self.alertLabel.text = NSLocalizedString(@"STR_PLEASE_ENTER_EMAIL", nil);
        [self.alertLabel setHidden:NO];
        return NO;
    }
    if(!forgotPassword && [self.passwordTextField.text length] == 0){
        [self.passwordTextField becomeFirstResponder];
        self.alertLabel.text = NSLocalizedString(@"STR_PLEASE_ENTER_PASSWORD", nil);
        [self.alertLabel setHidden:NO];
        return NO;
    }
    if(![self.emailTextField.text isValidEmail]){
        [self.emailTextField becomeFirstResponder];
        self.alertLabel.text = NSLocalizedString(@"STR_INVALID_EMAIL", nil);
        [self.alertLabel setHidden:NO];
        return NO;
    }
    
    
    return YES;
}


- (IBAction)signInAction:(id)sender {
    
    if([self validateInput:NO]){
        [self activateCancelButton];
        
        self.currentTask = [[FRAuthorisationService service] authWithEmail:self.emailTextField.text password:self.passwordTextField.text  completion:^(id object, id rawObject, NSError *error) {
            [self hideCancelButton];
            self.currentTask = nil;
            NSLog(@"%@",object);
            if(!error){
                [[FRMainScreenService new] showLoadingScreen];
            }else{
                switch (error.code) {
                    case FRErrorCodeInvalidLogin:
                        [self.alertLabel setHidden:NO];
                        self.alertLabel.text = NSLocalizedString(@"STR_ERROR_INVALID_LOGIN", nil);
                        
                        break;
                        
                    default:
                        [self.alertLabel setHidden:NO];
                        self.alertLabel.text = error.domain;
                        
                        break;
                }
            }
        }];
    }
}

- (IBAction)forgotPasswordAction:(id)sender {
    @weakify(self);
    if([self validateInput:YES]){
        [[FRAuthorisationService service] forgotPassword:self.emailTextField.text withCompletion:^(id object, id rawObject, NSError *error) {
            NSString *alertTitle;
            @strongify(self);
            if(error){
                alertTitle = NSLocalizedString(@"STR_ALERT_PASSWORD_SENDING_FAILED",nil);
            }else{
                alertTitle = NSLocalizedString(@"STR_ALERT_PASSWORD_SENT",nil);
            }
            UIAlertController *cntr = [UIAlertController alertControllerWithTitle:alertTitle message:nil preferredStyle:UIAlertControllerStyleAlert];
            [cntr addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}]];
            [self presentViewController:cntr animated:YES completion:nil];

        }];

    }
    
}

@end
