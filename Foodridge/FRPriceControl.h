//
//  FRPriceControl.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRPriceControl : UIControl

@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) UIColor *textColor;
@end
