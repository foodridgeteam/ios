//
//  FRStaticDataService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRStaticDataRealmService.h"
#import "FRCuisine.h"
#import "FRIngridient.h"

@implementation FRStaticDataRealmService

+ (RLMRealm *)currentRealm{
    
    static NSString *path = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *str = [[RLMRealm defaultRealm]  path];
        NSString *last = [str lastPathComponent];
        str = [str substringToIndex:(str.length - last.length)];
        path = [str stringByAppendingString:@"staticDataRealm.realm"];
    });
    
    return [RLMRealm realmWithPath:path];
}

+ (void)saveCuisinesWithData:(NSArray *)array{
    
    RLMRealm *realm = [self.class currentRealm];
    
    // Add to Realm with transaction
    [realm beginWriteTransaction];
    [realm deleteObjects:[FRCuisine allObjectsInRealm:realm]];

    for(id value in array){
        [FRCuisine createInRealm:realm withValue:value];
    }
    
    [realm commitWriteTransaction];

}

+ (void)saveIngridientWithData:(NSArray *)array{
    
    RLMRealm *realm = [self.class currentRealm];
    
    // Add to Realm with transaction
    [realm beginWriteTransaction];
    [realm deleteObjects:[FRIngridient allObjectsInRealm:realm]];
    
    for(id value in array){
        [FRIngridient createInRealm:realm withValue:value];
    }
    
    [realm commitWriteTransaction];

}

+ (RLMResults *)getCuisinesWithIDs:(NSArray *)ids{
    
    return [FRCuisine objectsInRealm:[self currentRealm] withPredicate:[NSPredicate predicateWithFormat:@"uid IN %@",ids]];
}


+ (RLMResults *)getCuisinesWithSearchText:(NSString *)text{
  
    return [FRCuisine objectsInRealm:[self currentRealm] where:@"name CONTAINS[c] %@",text];
}

+ (RLMResults *)getIngridientsWithIDs:(NSArray *)ids{

    return [FRIngridient objectsInRealm:[self currentRealm] withPredicate:[NSPredicate predicateWithFormat:@"uid IN %@",ids]];
}

+ (RLMResults *)getIngridientsWithSearchText:(NSString *)text{
   
    return [FRIngridient objectsInRealm:[self currentRealm] where:@"name CONTAINS[c] %@",text];

    
}


@end
