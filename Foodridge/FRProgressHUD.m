//
//  FRProgressHUD.m
//  Foodridge
//
//  Created by Victor Miroshnichenko on 5/8/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRProgressHUD.h"
#import "MBProgressHUD.h"

@implementation FRProgressHUD

+ (void)showMessageHudWithText:(NSString *)text inView:(UIView *)view delay:(CGFloat)delay {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = text;
    [hud hide:YES afterDelay:delay];
}

@end
