//
//  NetworkService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "FRNetworkService.h"
#import "NSDictionary+NullToNil.h"
#import "FRAuthorisationService.h"
#import "FRTermsService.h"
#import "FRAuthorisationService.h"
#import "FRMainScreenService.h"

@implementation FRNetworkService


+(AFHTTPSessionManager *)sessionManager{
    
    static AFHTTPSessionManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        [configuration setHTTPMaximumConnectionsPerHost:1];
        manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    });
    
    return manager;

}

+ (NSURLSessionTask *)requestWithMethod:(NSString *)method
                         requestAddress:(NSString *)address
                             parameters:(NSDictionary *)params
                             completion:(FRResponceBlock)completion {
    
    NSString *str = [baseURL stringByAppendingPathComponent:address];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:method URLString:str parameters:params error:nil];
    
    NSString *token = [FRAuthorisationService accessToken];
    if(token){
        [req setValue:token forHTTPHeaderField:@"token"];
        [req setValue:[NSString stringWithFormat:@"%ld",(long) [FRAuthorisationService currentUID]] forHTTPHeaderField:@"uid"];
    }

    NSURLSessionDataTask *dataTask = [[self sessionManager] dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
        if([resp statusCode] != 200){
            error = [NSError errorWithDomain:[NSHTTPURLResponse localizedStringForStatusCode:resp.statusCode] code:resp.statusCode userInfo:nil];
        }
        if (!error) {
            NSLog(@"%@:\n%@\n %@ %@",req.URL, [[NSString alloc] initWithData:req.HTTPBody encoding:4], response, responseObject);
            
            NSDictionary *data;
            if([responseObject isKindOfClass:[NSDictionary class]]){
                data = [responseObject objectOrNilForKey:@"data"];
            }else{
                error = [NSError errorWithDomain:@"Unknown error" code:500 userInfo:nil];
                completion(nil, data, error);

            }

            if([data isKindOfClass:[NSDictionary class]] || [data isKindOfClass:[NSArray class]]){
                completion(data, data, nil);
            }else if([data isKindOfClass:[NSString class]] &&
                     [(NSString *)data isEqualToString:@"ok"]){
                completion(nil, nil, nil);
                
            }else{
                NSDictionary *errorData = [responseObject objectOrNilForKey:@"error" classObject:[NSDictionary class]];
                NSString *msg = [errorData objectOrNilForKey:@"message" classObject:[NSString class]];
                NSInteger code = [[errorData objectOrNilForKey:@"code" classObject:[NSNumber class]] integerValue];
                if(code == 0){
                    code = 500;
                }
                if(!msg){
                    msg = @"Unknown error";
                }
                error = [NSError errorWithDomain:msg code:code userInfo:errorData];
                completion(nil, data, error);
            }
            
            
        } else {
            if(error.code == 424 && [[FRAuthorisationService service] authorized] && [[FRAuthorisationService service] showTerms]){
                [[FRAuthorisationService service] setShowTerms:NO];
             
                UIViewController *vc =     [[[UIApplication sharedApplication] keyWindow] rootViewController];
                
                                  [[FRTermsService new] showTermsFromViewController:vc shouldAgree:YES onDismiss:^(BOOL aggreed) {
                                      
                                      if(aggreed){
                                          [[FRAuthorisationService service] aggreeWithTermsWithCompletion:^(id object, id rawObject, NSError *error) {
                                              [[FRMainScreenService new] showLoadingScreen];
                                          }];
                                      }else{
                                          [[FRAuthorisationService service] logout];
                                          [[FRMainScreenService new] showLoginScreen];
                                      }
                                  }];
            }
            
            completion(nil, nil, error);
            
#ifdef DEBUG
            NSLog(@"Error:%@ \n %@:%@\n",error,req.URL, [[NSString alloc] initWithData:req.HTTPBody encoding:4]);

            
            @try {
                NSLog(@"%@",[[NSString alloc] initWithData:[[error userInfo] valueForKey:@"com.alamofire.serialization.response.error.data"] encoding:4]);
            } @catch (NSException *exception) {
                
            }
#endif
            
        }
    }];
    
    [dataTask resume];
    return dataTask;
}

@end
