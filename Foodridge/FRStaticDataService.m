//
//  FRTagsSearchManager.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRStaticDataService.h"
#import "FRNetworkService+Defaults.h"
#import "FRStaticDataRealmService.h"
#import "FRCuisineModel.h"
#import "FRCuisine.h"
#import "FRIngridientModel.h"
#import "FRIngridient.h"

@import libextobjc;
@import Mantle;

@interface FRStaticDataService ()

@property(nonatomic, strong) NSURLSessionTask *cuisinesTask;
@property(nonatomic, strong) NSURLSessionTask *ingridientsTask;

@property(nonatomic, assign) BOOL cuisinesUpdated;
@property(nonatomic, assign) BOOL ingridientsUpdated;

@property(nonatomic, strong) NSArray *cuisines;
@property(nonatomic, strong) NSArray *ingridients;

@end

@implementation FRStaticDataService

+ (FRStaticDataService *)service{
    
    static FRStaticDataService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [FRStaticDataService new];
    });
    return service;
}

+ (NSString *)cuisinesJsonPath{
    static NSString *path;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        path = [path stringByAppendingPathComponent:@"Defaults/Coisines.json"];
        
    });
    return path;
}

+ (NSString *)ingridientsJsonPath{
    static NSString *path;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        path = [path stringByAppendingPathComponent:@"Defaults/Ingridients.json"];
        
    });
    return path;
}

- (void)reloadCuisines{
    
    /*if(self.cuisinesTask){
        return;
    }*/
    
    @weakify(self);
    self.cuisinesTask = [FRNetworkService getCuisinesWithCompletion:^(id object, id rawObject, NSError *error) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @strongify(self);
            if(!error && [rawObject isKindOfClass:[NSArray class]]){
                
                self.cuisinesTask = nil;
                NSArray *arr = [MTLJSONAdapter modelsOfClass:[FRCuisineModel class] fromJSONArray:rawObject error:nil];
                self.cuisines = arr;

                [FRStaticDataRealmService saveCuisinesWithData:arr];
                
            }
        });        
    }];
}

- (void)reloadIngridients{
    
    /*if(self.ingridientsTask){
        return;
    }*/
    
    @weakify(self);
    self.ingridientsTask = [FRNetworkService getIngridientsWithCompletion:^(id object, id rawObject, NSError *error) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @strongify(self);
            self.ingridientsTask = nil;
            
            NSArray *arr = [MTLJSONAdapter modelsOfClass:[FRIngridientModel class] fromJSONArray:rawObject error:nil];
            self.cuisines = arr;
            
            [FRStaticDataRealmService saveIngridientWithData:arr];

        });
    }];
}

-(NSArray *)viewModelsFromCuisines:(RLMResults *)cuisines{
    
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:cuisines.count];
    
    for(FRCuisine *cuisine in cuisines){
        FRCuisineModel *model = [FRCuisineModel new];
        model.uid = @(cuisine.uid);
        model.name = cuisine.name;
        [arr addObject:model];
    }
    return [arr copy];

}

-(NSArray *)viewModelsFromIngridients:(RLMResults *)ingridients{
    
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:ingridients.count];
    
    for(FRIngridient *ingridient in ingridients){
        FRIngridientModel *model = [FRIngridientModel new];
        model.uid = @(ingridient.uid);
        model.name = ingridient.name;
        [arr addObject:model];
    }
    return [arr copy];
    
}


- (NSArray *)getCuisinesWithIDs:(NSArray *)ids{
    
    RLMResults *res = [FRStaticDataRealmService getCuisinesWithIDs:ids];
    return  [self viewModelsFromCuisines:res];
    
}

- (NSArray *)getCuisinesWithSearchText:(NSString *)text{
    
   RLMResults *res = [FRStaticDataRealmService getCuisinesWithSearchText:text];
    return  [self viewModelsFromCuisines:res];

    
}

- (NSArray *)getIngridientsWithIDs:(NSArray *)ids{
    RLMResults *res = [FRStaticDataRealmService getIngridientsWithIDs:ids];
    return  [self viewModelsFromIngridients:res];

}

- (NSArray *)getIngridientsWithSearchText:(NSString *)text{
    RLMResults *res = [FRStaticDataRealmService getIngridientsWithSearchText:text];
    return  [self viewModelsFromIngridients:res];

}


@end
