//
//  UIColor+foodridgeColors.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "UIColor+FoodridgeColors.h"

@implementation UIColor (FoodridgeColors)

+ (UIColor *)grayButtonColor{
    return [UIColor colorWithRed:127/255.0 green:127/255.0 blue:127/255.0 alpha:1.];
}

+ (UIColor *)aquaColor{
    return [UIColor colorWithRed:66/255.0 green:165/255.0 blue:136/255.0 alpha:1.];
}

+ (UIColor *)barColor{
    return [UIColor colorWithRed: 38/255.0 green: 50/255.0 blue: 56/255.0 alpha:1.];
}

+ (UIColor *)barTextColor{
    return [UIColor colorWithRed: 84/255.0 green: 160/255.0 blue: 137/255.0 alpha:1.];
}

+ (UIColor *)segmentLineColor{
    return [UIColor colorWithRed: 211/255.0 green: 72/255.0 blue: 67/255.0 alpha:1.];
}

+ (UIColor *)alertRedColor{
    return [UIColor colorWithRed: 211/255.0 green: 72/255.0 blue: 67/255.0 alpha:1.];

}

+ (UIColor *)stripeButtonColor{
    return [UIColor colorWithRed: 51/255.0 green: 150/255.0 blue: 221/255.0 alpha:1.];
    
}

+ (UIColor *)grayLablesColor{
    return [UIColor colorWithRed: 102/255.0 green: 108/255.0 blue: 112/255.0 alpha:1.];
}

+ (UIColor *)commentsBlueColor{
    return [UIColor colorWithRed: 33/255.0 green: 150/255.0 blue: 243/255.0 alpha:1.];
}

+ (UIColor *)stripeConnectButtonColor {
    return [UIColor colorWithRed:59/255.0 green:89/255.0 blue:152/255.0 alpha:1.];
}


@end
