//
//  ChefMenuTableViewHeader.m
//  Foodridge
//
//  Created by Dev on 5/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "ChefMenuTableViewHeader.h"

@interface ChefMenuTableViewHeader ()

@property (weak, nonatomic) IBOutlet UILabel *headerTitle;

@end

@implementation ChefMenuTableViewHeader

-(void)awakeFromNib{
    [super awakeFromNib];
    
    
    self.shooldShowHeaderTitle = !self.headerTitle.hidden;
}

+(instancetype)view{
    ChefMenuTableViewHeader *headerView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    
    return headerView;
}

-(void)setShooldShowHeaderTitle:(BOOL)shooldShowHeaderTitle{
    _shooldShowHeaderTitle = shooldShowHeaderTitle;
    
    self.headerTitle.hidden = !shooldShowHeaderTitle;
}

@end
