//
//  FRMenuViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <libextobjc/extobjc.h>
#import "FRMenuViewController.h"
#import "FRMealCell.h"
#import "FRMealInfoViewController.h"
#import "FRMealsService.h"
#import "FROrderComposeViewController.h"
#import "FRChefDetailViewController.h"
#import "FRChefsService.h"
#import "FREmptyDataView.h"

@interface FRMenuViewController ()<UISearchBarDelegate, FRMealCellDelegate>

@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSArray *meals;
@property (nonatomic, copy) NSString *searchString;
@property (nonatomic, strong) FREmptyDataView *emptyDataView;

@property (strong, nonatomic) FRMealsService *service;

@end

@implementation FRMenuViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    
    [self setupModel];
    
    [self.service mealListWithChefID:0 searchText:nil completion:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupModel) name:FRMealsUpdatedNotificationName object:nil];
    
    self.emptyDataView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FREmptyDataView class]) owner:self options:nil] objectAtIndex:0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = NSLocalizedString(@"STR_MENU", nil);
}

-(FRMealsService *)service{
    if (!_service){
        _service = [FRMealsService service];
    }
    return _service;
}

- (void)setupModel {
    self.meals = [self.service items];
    
    if (!self.meals.count) {
        self.emptyDataView.filterType = FREmptyDataTypeMeals;
        CGRect tableViewFrameWithoutSearchBar = CGRectMake(self.tableView.frame.origin.x,
                                                           self.tableView.frame.origin.y + self.searchBar.frame.size.height,
                                                           self.tableView.frame.size.width,
                                                           self.tableView.frame.size.height);
        self.emptyDataView.frame = tableViewFrameWithoutSearchBar;
        if (![self.emptyDataView isDescendantOfView:self.tableView]) {
            [self.tableView addSubview:self.emptyDataView];
            [self.searchBar resignFirstResponder];
        }
    }
    else {
        [self.emptyDataView removeFromSuperview];
    }
    
    [self.tableView reloadData];
    [self.refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:0.05];
}

- (void)pullToRefresh {
    [self.service clearData];
    [self.service mealListWithChefID:0 searchText:nil completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.meals.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell setNeedsLayout];
    if (indexPath.row == self.meals.count - 1 && self.service.loadMore) {
        [self.service mealListWithChefID:0 searchText:self.searchString completion:nil];
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FRMealCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (self.meals.count > indexPath.row) {
    [cell setupCellFromMealModel:[self.meals objectAtIndex:indexPath.row]];
    cell.delegate = self;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Menu" bundle:nil];
    FRMealInfoViewController *vc = [sb instantiateViewControllerWithIdentifier:@"FRMealInfo"];
    vc.meal = [self.meals objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark SearchBar delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    self.searchBar.showsCancelButton = YES;
    return YES;
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    self.searchString = [searchBar.text stringByReplacingCharactersInRange:range withString:text];
    [self.service clearData];
    [self.service mealListWithChefID:0 searchText:self.searchString completion:nil];
    
    return YES;
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchBar.text = nil;
    self.searchString = nil;
    self.searchBar.showsCancelButton = NO;
    [self.searchBar resignFirstResponder];
    [self.service clearData];
    [self.service mealListWithChefID:0 searchText:nil completion:nil];
}

#pragma mark - cell delegate

- (void)orderNowButtonClicked:(FRMealCell *)cell{
    NSIndexPath *ip = [self.tableView indexPathForCell:cell];
    id<FRMealModel> meal = self.meals[ip.row];
    
    [FROrderComposeViewController presentWithMeal:meal fromViewController:self];
}

-(void) showChef:(NSNumber*) chefID {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"ChefDetail" bundle:nil];
    FRChefDetailViewController *vc = [sb instantiateInitialViewController];
    vc.chefUid = chefID;
    [self.navigationController pushViewController:vc animated:YES];
   
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:FRMealsUpdatedNotificationName object:nil];
}

@end
