//
//  UIFont+FoodridgeFonts.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (FoodridgeFonts)

+ (UIFont *)openSansFontWithSize:(CGFloat)size;

+ (UIFont *)openSansBoldFontWithSize:(CGFloat)size;

+ (UIFont *)openSansItalicFontWithSize:(CGFloat)size;

@end
