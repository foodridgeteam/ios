//
//  FRMediaUploadsService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import Foundation;
@import UIKit;

#import "FRNetworkService.h"

@interface FRMediaUploadsService : NSObject

- (void)uploadAvatar:(UIImage *)img withCompletion:(FRResponceBlock)completion;
- (void)uploadMealImage:(UIImage *)img forMealWithID:(NSInteger)mealID withCompletion:(FRResponceBlock)completion;
- (void)deleteImageWithName:(NSString *)imageName forMealWithID:(NSInteger)mealID withCompletion:(FRResponceBlock)completion;

@end
