//
//  FRMyMenuViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <libextobjc/extobjc.h>
#import "FRMyMenuViewController.h"
#import "FRAuthorisationService.h"
#import "FRMyDishCell.h"
#import "FRMealExtendedModel.h"
#import "FRProfileModel.h"
#import "FRMealsService.h"
#import "FRDishEditViewController.h"
#import "FRMenuService.h"
#import "FRProgressHUD.h"


@interface FRMyMenuViewController () <FRMyDishCellDelegate>

@property (nonatomic, strong) NSArray *myMeals;
@property (nonatomic, strong) FRProfileModel *profile;

@property (strong, nonatomic) FRMealsService *service;

@end

@implementation FRMyMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight          = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 262.f;
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    
    self.profile = [[FRAuthorisationService service] currentUser];
    
    //[self.service mealListWithChefID:self.profile.uid.integerValue searchText:nil completion:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupModel) name:FRMealsUpdatedNotificationName object:nil];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.service clearData];
    [self.service mealListWithChefID:self.profile.uid.integerValue searchText:nil completion:nil];
}

- (void)setupModel {
    self.myMeals = [self.service items];
    
    [self.tableView reloadData];
    [self.refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:0.05];
}

- (FRMealsService *)service{
    if (!_service){
        _service = [FRMealsService service];
    }
    return _service;
}

- (void)pullToRefresh {
    [self.service clearData];
    [self.service mealListWithChefID:self.profile.uid.integerValue searchText:nil completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.myMeals.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FRMyDishCell *dishCell = [tableView dequeueReusableCellWithIdentifier:@"dishCell"];
    
    if (self.myMeals.count > indexPath.row) {
        FRMealExtendedModel *meal = [self.myMeals objectAtIndex:indexPath.row];
        [dishCell setupCellWithMealModel:meal];
    }
    
    dishCell.delegate = self;
    
    if (indexPath.row == self.myMeals.count - 1 && self.service.loadMore) {
        [self.service mealListWithChefID:self.profile.uid.integerValue searchText:nil completion:nil];
    }
    
    return dishCell;
}

- (void)editDishInCell:(FRMyDishCell *)cell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    FRMealExtendedModel *meal = [self.myMeals objectAtIndex:indexPath.row];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dishes" bundle:nil];
    FRDishEditViewController *dishEditVC = [sb instantiateViewControllerWithIdentifier:@"dishEdit"];
    dishEditVC.model = meal;
    [self.navigationController pushViewController:dishEditVC animated:YES];
}

- (void) updateMealListed:(FRMyDishCell*)cell {
    
    @weakify(self);
    [[FRMenuService service] updateMyMealWithModel:cell.cellModel completion:^(id object, id rawObject, NSError *error) {
        @strongify(self);
        cell.cellModel = object;
        if (error) {
            [FRProgressHUD showMessageHudWithText:@"Stripe account not connected" inView:self.view delay:2.5];
        }
    }];
}



- (void)shareActionInCell:(FRMyDishCell *)cell {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    FRMealExtendedModel *meal = [self.myMeals objectAtIndex:indexPath.row];
    
    NSString* subject = [NSString stringWithFormat:@"%@ Share his %@ with You", meal.chefTitle, meal.title];
    
    NSString *infoText = [NSString stringWithFormat:@"%@\nSpicy rank: %@\nDescription: %@ \n", meal.title, meal.spiceRate, meal.mealDescription];

    if (meal.cuisines.count > 0) {
        infoText = [infoText stringByAppendingString:@"Cuisine: "];
        infoText = [infoText stringByAppendingString:[[meal.cuisines valueForKey:@"name"] componentsJoinedByString:@", "]];
        infoText = [infoText stringByAppendingString:@"\n"];
    }
    
    if (meal.ingridients.count > 0) {
        infoText = [infoText stringByAppendingString:@"Ingridients: "];
        infoText = [infoText stringByAppendingString:[[meal.ingridients valueForKey:@"name"] componentsJoinedByString:@", "]];
        infoText = [infoText stringByAppendingString:@"\n"];
    }
    
    NSArray *objectsToShare = @[infoText];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    [activityVC setValue:subject forKey:@"subject"];
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:FRMealsUpdatedNotificationName object:nil];
}



@end
