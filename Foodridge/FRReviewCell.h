//
//  FRReviewCell.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRCommentsModel.h"

@interface FRReviewCell : UITableViewCell

- (void)setupCellWithCommentModel:(FRCommentsModel *)comment;

@end
