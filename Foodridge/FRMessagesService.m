//
//  FRMessagesService.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMessagesService.h"
#import "FROrderMessageModel.h"

@implementation FRMessagesService

+ (FRMessagesService *)service{
    static FRMessagesService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [FRMessagesService new];
    });
    return service;
}

- (NSURLSessionTask *)getMessagesForOrderID:(NSInteger)orderID lastID:(NSInteger)lastID completion:(FRResponceBlock)completion {
    return [FRNetworkService getMessagesForOrderID:orderID lastID:lastID completion:^(id object, id rawObject, NSError *error) {
        
        NSArray *messages;
        if (!error) {
            if([rawObject isKindOfClass:[NSArray class]]){
                messages = [FROrderMessageModel modelsFromArray:rawObject];
                completion(messages,rawObject,error);
            }
        }
        else {
            completion(nil, nil, error);
        }
    }];
}

- (NSURLSessionTask *)leaveMessageForOrderWithID:(NSInteger)orderID messageText:(NSString *)messageText completion:(FRResponceBlock)completion {
    return [FRNetworkService leaveMessageForOrderWithID:orderID messageText:messageText completion:^(id object, id rawObject, NSError *error) {
        if (!error) {
            FROrderMessageModel *model = [FROrderMessageModel modelWitDictionary:rawObject];
            completion(model, rawObject, error);
        }
        else {
            completion(nil, nil, error);
        }
    }];
}

@end
