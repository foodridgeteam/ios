//
//  UUID.m
//  EndyMed
//
//  Created by Vitalii Yevtushenko on 2/27/13.
//  Copyright (c) 2012-2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import "UUID.h"

@implementation UUID

+ (NSString *)uuid {
    CFUUIDRef uuidRef = CFUUIDCreate(nil);
    CFStringRef uuidStr = CFUUIDCreateString(nil, uuidRef);
    CFRelease(uuidRef);
    NSString *s = [NSString stringWithString:(__bridge NSString *) uuidStr];
    CFRelease(uuidStr);
    return s;
}

@end

