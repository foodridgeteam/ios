//
//  FRNetworkService+Defaults.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService+Defaults.h"

@implementation FRNetworkService (Defaults)

+ (NSURLSessionTask *)getCuisinesWithCompletion:(FRResponceBlock)completion{
    return [self requestWithMethod:@"GET"
                    requestAddress:@"cuisines"
                        parameters:nil
                        completion:^(id object, id rawObject, NSError *error) {
                            completion(nil,rawObject,error);
                            NSLog(@"%@",rawObject);
                        }];
    
}

+ (NSURLSessionTask *)getIngridientsWithCompletion:(FRResponceBlock)completion{
    return [self requestWithMethod:@"GET"
                    requestAddress:@"ingredients"
                        parameters:nil
                        completion:^(id object, id rawObject, NSError *error) {
                            completion(nil,rawObject,error);
                            NSLog(@"%@",rawObject);
                        }];
    
}

+ (NSURLSessionTask *)sendFeedbackWithText:(NSString *)text completion:(FRResponceBlock)completion {
    return [self requestWithMethod:@"POST"
                    requestAddress:@"feedback"
                        parameters:@{@"feedBack" : text}
                        completion:completion];
}

@end
