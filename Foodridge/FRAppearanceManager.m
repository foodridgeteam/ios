//
//  FRAppearanceManager.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//
@import UIKit;
@import KSTokenView;

#import "FRAppearanceManager.h"
#import "UIColor+FoodridgeColors.h"
#import "UIImage+Color.h"
#import "UIFont+FoodridgeFonts.h"

@implementation FRAppearanceManager

+ (void)setupAppearance{
    [self setupNavigationBar];
    [self setupSearchBar];
    [self setupSegmentedControl];

}

+ (void)setupNavigationBar{
    
    UINavigationBar *bar = [UINavigationBar appearance];
    bar.barTintColor = [UIColor barColor];
    bar.tintColor = [UIColor whiteColor];
    bar.titleTextAttributes = @{NSFontAttributeName: [UIFont openSansBoldFontWithSize:17],
                                NSForegroundColorAttributeName : [UIColor whiteColor]};
    
}

+ (void)setupSearchBar{
    UISearchBar *bar =[UISearchBar appearance];
    bar.barTintColor = [UIColor barColor];
    bar.tintColor = [UIColor barTextColor];

}



+ (void)setupSegmentedControl{
    
    UISegmentedControl *control = [UISegmentedControl appearance];
    UIImage *segmentSelected   = [self imageOfSegmentedBgSelectedWithFrame:CGRectMake(0, 0, 1, 50)];
    
    UIImage *segmentDivider    = [self imageOfSegmentedBgDividerWithFrame:CGRectMake(0, 0, 1, 50)];
    
    UIImage *white = [UIImage imageWithColor:[UIColor whiteColor]];
    
    [control setTintColor:[UIColor clearColor]];
    
    [control setBackgroundImage:white forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [control setBackgroundImage:segmentSelected forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [control setBackgroundImage:white forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
    [control setDividerImage:segmentDivider forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [control setDividerImage:segmentDivider forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [control setDividerImage:segmentDivider forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];

    [control setTitleTextAttributes: @{NSFontAttributeName: [UIFont openSansBoldFontWithSize:15],
                                       NSForegroundColorAttributeName : [UIColor blackColor]} forState:UIControlStateSelected];
    
    [control setTitleTextAttributes: @{NSFontAttributeName: [UIFont openSansFontWithSize:15],
                                       NSForegroundColorAttributeName : [UIColor grayButtonColor]} forState:UIControlStateNormal];

}

+ (void)drawSegmentedBgSelectedWithFrame: (CGRect)frame {
    
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: frame];
    [[UIColor whiteColor] setFill];
    [rectanglePath fill];
    
    rectanglePath = [UIBezierPath bezierPathWithRect: CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame) + CGRectGetHeight(frame) - 6, CGRectGetWidth(frame), 6)];
    [[UIColor segmentLineColor] setFill];
    [rectanglePath fill];
}

+ (void)drawSegmentedDividerSelectedWithFrame: (CGRect)frame {
    
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame) + floor(CGRectGetHeight(frame) * 0.25 + 0.5), CGRectGetWidth(frame), floor(CGRectGetHeight(frame) * 0.75 + 0.5) - floor(CGRectGetHeight(frame) * 0.25 + 0.5))];
    
    [UIColor.grayColor setFill];
    [rectanglePath fill];
}

+ (UIImage*)imageOfSegmentedBgSelectedWithFrame: (CGRect)frame
{
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, 0.0f);
    [self drawSegmentedBgSelectedWithFrame: frame];
    
    UIImage* imageOfSegmentedBgSelected = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageOfSegmentedBgSelected;
}

+ (UIImage*)imageOfSegmentedBgDividerWithFrame: (CGRect)frame
{
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, 0.0f);
    [self drawSegmentedDividerSelectedWithFrame: frame];
    
    UIImage* imageOfSegmentedBgSelected = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageOfSegmentedBgSelected;
}

+ (void)setupStandardSegmentControl:(UISegmentedControl *)control{
    
    
    
    [control setTintColor:[UIColor aquaColor]];
    
    [control setBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [control setBackgroundImage:nil forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [control setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
    [control setDividerImage:nil forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [control setDividerImage:nil forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [control setDividerImage:nil forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    [control setTitleTextAttributes: @{NSFontAttributeName: [UIFont openSansFontWithSize:14],
                                       NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
    
    [control setTitleTextAttributes: @{NSFontAttributeName: [UIFont openSansFontWithSize:14],
                                       NSForegroundColorAttributeName : [UIColor aquaColor]} forState:UIControlStateNormal];
}

+ (void)setupKSTokenView:(KSTokenView *)tokenView{
    tokenView.promptText = @"";
    tokenView.font = [UIFont openSansFontWithSize:14];
    tokenView.descriptionText = @"";
    tokenView.style = KSTokenViewStyleSquared;
    tokenView.minimumCharactersToSearch = 1;
    tokenView.searchResultBackgroundColor = [UIColor clearColor];
    tokenView.shouldHideSearchResultsOnSelect = YES;
    tokenView.removesTokensOnEndEditing = NO;
    tokenView.maximumHeight = 320;
}


@end
