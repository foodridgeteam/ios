//
//  FRDishEditViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/15/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import KSTokenView;
#import <libextobjc/extobjc.h>
#import <BlocksKit/UIImagePickerController+BlocksKit.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "FRMenuService.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "FRDishEditViewController.h"
#import "FRMealExtendedModel.h"
#import "FRSpiceControl.h"
#import "RNCCheckbox.h"
#import "FRAppearanceManager.h"
#import "UIColor+FoodridgeColors.h"
#import "UIImage+Color.h"
#import "UITextField+Price.h"
#import "FRStaticDataService.h"
#import "FRCuisineModel.h"
#import "FRChefService.h"
#import "FRIngridientModel.h"
#import "FRMediaUploadsService.h"


@interface FRDishEditViewController()<UITextViewDelegate, UITextFieldDelegate, KSTokenViewDelegate>
//model

@property(nonatomic, copy) id<FRMealModel> unsavedChanges;

@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ingridientsBottomSpace;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cuisinesBottomSpace;

//details
@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet KSTokenView *cuisinesTokenView;

//image
@property (strong, nonatomic) IBOutlet UIButton *addMealImageButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteImageButton;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;

//ingridients
@property (strong, nonatomic) IBOutlet KSTokenView *ingridientsTokenView;

//flags
@property (strong, nonatomic) IBOutlet FRSpiceControl *spiceControl;
@property (strong, nonatomic) IBOutletCollection(RNCCheckbox) NSArray *checkboxes;

@property (weak, nonatomic) IBOutlet UITextField *otherFlagsTextField;

//price and delivery
@property (strong, nonatomic) IBOutlet UITextField *priceTextField;
@property (strong, nonatomic) IBOutlet RNCCheckbox *deliveryCheckbox;
@property (strong, nonatomic) IBOutlet RNCCheckbox *pickupCheckbox;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *deliveryDetailsConstraint;
@property (strong, nonatomic) IBOutlet UIView *deliveryDetailsView;
@property (strong, nonatomic) IBOutlet UITextField *deliveryPriceTextField;

@property (strong, nonatomic) IBOutlet RNCCheckbox *disclamerCheckbox;

@property (strong, nonatomic) IBOutlet UITextField *servingsTextField;


@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *deleteHeight;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

@property (weak, nonatomic) IBOutlet UILabel* ciusinesPlaceholder;
@property (weak, nonatomic) IBOutlet UILabel* ingridientsPlaceholder;

@property (assign, nonatomic) BOOL isEditMode;

@property (nonatomic, assign) NSUInteger spiceOldValue;

@end

@implementation FRDishEditViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self setupUI];
    [self setupModel];
    
    self.title = self.isEditMode ? NSLocalizedString(@"STR_EDIT_DISH", nil) : NSLocalizedString(@"STR_ADD_DISH", nil);
    
    self.spiceOldValue = [self.model spiceRate] ? [self.model spiceRate].unsignedIntegerValue : 1;
}


- (void)setupControllerFromModel {
    
    self.titleTextField.text = self.model.title;
    self.descriptionTextView.text = self.model.mealDescription;
    self.priceTextField.text = [self.model.price stringValue];
    self.spiceControl.value = [self.model.spiceRate integerValue];
    self.deliveryPriceTextField.text = [self.model.deliveryPrice stringValue];
    self.servingsTextField.text = [self.model.servingsLeft stringValue];
    
    if (self.model.imageURLs) {
        [self.imageView setImageWithURL:[NSURL URLWithString:[self.model.imageURLs firstObject]]];
        self.imageHeight.active = NO;
        self.deleteImageButton.hidden = NO;
        self.addMealImageButton.enabled = NO;
    }
    
    for (FRCuisineModel *cuisine in self.model.cuisines) {
        [self.cuisinesTokenView addTokenWithTitle:cuisine.name tokenObject:cuisine];
    }
    
    for (FRIngridientModel *ingredient in self.model.ingridients) {
        [self.ingridientsTokenView addTokenWithTitle:ingredient.name tokenObject:ingredient];
    }
    
    if (self.cuisinesTokenView.tokens.count > 0) {
        self.ciusinesPlaceholder.hidden = YES;
    }
    
    if (self.ingridientsTokenView.tokens.count > 0) {
        self.ingridientsPlaceholder.hidden = YES;
    }
    
    self.otherFlagsTextField.text = self.model.otherFlags;
    
    [self setupDeliveryActive];
}


- (void)setupUI{
    
    
    [self.saveButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    

    [FRAppearanceManager setupKSTokenView:self.cuisinesTokenView];
    [FRAppearanceManager setupKSTokenView:self.ingridientsTokenView];
    
    self.cuisinesTokenView.delegate = self;
    self.ingridientsTokenView.delegate = self;
    
    self.otherFlagsTextField.delegate = self;
    
    self.deleteImageButton.hidden = YES;
    self.deleteButton.hidden = YES;
    self.deleteHeight.constant = 0;
    [self.view layoutIfNeeded];
}



- (void)setModel:(id<FRMealModel>)model{
    _model = model;
    if(![model uid]){
        self.deleteButton.hidden = YES;
        self.deleteHeight.constant = 0;
        [self.view layoutIfNeeded];
    }
    
    [self setupCheckboxes];
    [self setupDeliveryActive];

    self.unsavedChanges = [FRMealExtendedModel new];
    
    if (self.isEditMode) {
        self.unsavedChanges = self.model;
        [self setupControllerFromModel];
    }
}

- (void)setupPriceTextFields{
    
    [self setupPriceTextField:self.priceTextField];
    [self setupPriceTextField:self.deliveryPriceTextField];
    
}
- (void)setupPriceTextField:(UITextField *)textField{
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == self.otherFlagsTextField){
        [self saveOtherFlags];
    }
    
    // Minimal food price should be 1$ or more for successful Stripe transaction.
    if (textField == self.priceTextField) {
        if (textField.text.floatValue < 1) {
            self.unsavedChanges.price = @1;
            self.priceTextField.text = @"1";
        }
    }
    
    if (textField == self.deliveryPriceTextField) {
        if (textField.text.floatValue < 1) {
            self.unsavedChanges.deliveryPrice = @1;
            self.deliveryPriceTextField.text = @"1";
        }
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == self.priceTextField || textField == self.deliveryPriceTextField){
        if([textField.text floatValue] < 0.01){
            textField.text = @"";
        }
    }
}
- (void)textViewDidChange:(UITextView *)textView{
    if(textView == self.descriptionTextView){
        [self.unsavedChanges setMealDescription:textView.text];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == self.priceTextField || textField == self.deliveryPriceTextField){
        BOOL should = [textField procesChange:string inRange:range];
        if(textField == self.priceTextField){
            [self.unsavedChanges setPrice:@([textField.text floatValue])];
        }else{
            [self.unsavedChanges setDeliveryPrice:@([textField.text floatValue])];
        }
        return should;
        
    }else if(textField == self.titleTextField){
        [self.unsavedChanges setTitle:[textField.text stringByReplacingCharactersInRange:range withString:string]];
    }else if(textField == self.servingsTextField){
        NSInteger servings = [self.servingsTextField.text stringByReplacingCharactersInRange:range withString:string].integerValue;
        textField.text = [NSString stringWithFormat:@"%ld",(long)servings];
        [self.unsavedChanges setServingsLeft:@(servings)];
        return NO;
    }
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if(textField == self.priceTextField || textField == self.deliveryPriceTextField){
        [textField finishEditing];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}

- (void)setupModel{
    if(!self.model){
        self.model = [FRMealExtendedModel new];
    }
    else {
        self.isEditMode = YES;
    }
    
    [self setupCheckboxes];
}

-(void)setupCheckboxes{
    
    id<FRMealModel> m = [self.unsavedChanges flags]?self.unsavedChanges:self.model;
    for(RNCCheckbox *chb in self.checkboxes){
        [chb setOn:[m flagValue:chb.tag]];
    }
}

- (IBAction)nameValueChanged:(UITextField *)sender {
    self.unsavedChanges.title = sender.text;
}

#pragma mark - delivery changed

- (IBAction)deliveryChanged:(RNCCheckbox *)sender {
    
    if([self.deliveryPriceTextField isFirstResponder]){
        [self.deliveryPriceTextField resignFirstResponder];
    }
    
    if (self.pickupCheckbox.on) {
        [self.unsavedChanges setDeliveryType:sender.on ? [NSNumber numberWithInteger:FRDeliveryTypePickupAndDelivery] : [NSNumber numberWithInteger:FRDeliveryTypePickup]];
    }
    else {
        [self.unsavedChanges setDeliveryType:sender.on ? [NSNumber numberWithInteger:FRDeliveryTypeDelivery] : @0];
    }
    
    self.deliveryCheckbox.on = sender.on;
    
    [self setupDeliveryActive];
}
- (IBAction)pickupChanged:(RNCCheckbox *)sender {

    if (self.deliveryCheckbox.on) {
        [self.unsavedChanges setDeliveryType:sender.on ? [NSNumber numberWithInteger:FRDeliveryTypePickupAndDelivery] : [NSNumber numberWithInteger:FRDeliveryTypeDelivery]];
    }
    else {
        [self.unsavedChanges setDeliveryType:sender.on ? [NSNumber numberWithInteger:FRDeliveryTypePickup] : @0];
    }
    
    self.pickupCheckbox.on = sender.on;
    
    [self setupDeliveryActive];
}

-(void)setupDeliveryActive{
    
    NSNumber *n = [self.unsavedChanges deliveryType]?:[self.model deliveryType];
    switch (n.integerValue) {
        case FRDeliveryTypePickupAndDelivery:
            self.deliveryCheckbox.on = YES;
            self.pickupCheckbox.on = YES;
            break;
        case FRDeliveryTypeDelivery:
            self.deliveryCheckbox.on = YES;
            self.pickupCheckbox.on = NO;
            break;
        case FRDeliveryTypePickup:
            self.deliveryCheckbox.on = NO;
            self.pickupCheckbox.on = YES;
            break;
            
        default:
            self.deliveryCheckbox.on = NO;
            self.pickupCheckbox.on = NO;
            break;
    }
    
    BOOL active = self.deliveryCheckbox.on;
    
    self.deliveryDetailsConstraint.constant = active?self.deliveryDetailsView.bounds.size.height:-1;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.deliveryDetailsView.alpha = active?1.:0.;
        [self.view layoutIfNeeded];
    }];
}



#pragma mark - tokenfield

-(void)saveCuisines{
    NSArray *tokens = [self.cuisinesTokenView.tokens valueForKey:@"object"];
    if(!tokens){
        tokens = @[];
    }
    self.unsavedChanges.cuisines = tokens;
    NSLog(@"%@",tokens);
}

- (void)saveIngridients{
    NSArray *tokens = [self.ingridientsTokenView.tokens valueForKey:@"object"];
    if(!tokens){
        tokens = @[];
    }
    self.unsavedChanges.ingridients = tokens;
    
    NSLog(@"%@",tokens);
}
- (void)saveOtherFlags{
    
    self.unsavedChanges.otherFlags = self.otherFlagsTextField.text;
    
    NSLog(@"%@",self.otherFlagsTextField.text);
}

- (void)tokenView:(KSTokenView *)tokenView didAddToken:(KSToken *)token{
    
    token.deleteIcon = YES;
    
    if(tokenView == self.cuisinesTokenView){
        [self saveCuisines];
    }else if(tokenView == self.ingridientsTokenView){
        [self saveIngridients];
    }
}

- (void)tokenView:(KSTokenView *)tokenView didDeleteToken:(KSToken *)token{
    if(tokenView == self.cuisinesTokenView){
        [self saveCuisines];
    }else if(tokenView == self.ingridientsTokenView){
        [self saveIngridients];
    }
}

- (void)tokenViewDidBeginEditing:(KSTokenView *)tokenView{
    
    NSLayoutConstraint *c;
    if(tokenView == self.cuisinesTokenView){
        self.ciusinesPlaceholder.hidden = YES;
        c = self.cuisinesBottomSpace;
    }else if(tokenView == self.ingridientsTokenView){
        self.ingridientsPlaceholder.hidden = YES;
        c = self.ingridientsBottomSpace;
    }else{
        return;
    }
    c.constant = 200.;
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
}

-(void)tokenViewDidEndEditing:(KSTokenView *)tokenView{
    NSLayoutConstraint *c;
    self.cuisinesTokenView.text = @"";
    self.ingridientsTokenView.text = @"";
    if(tokenView == self.cuisinesTokenView){
        c = self.cuisinesBottomSpace;
        if (tokenView.tokens.count == 0) {
        self.ciusinesPlaceholder.hidden = NO;
        
        }
    }else if(tokenView == self.ingridientsTokenView){
        c = self.ingridientsBottomSpace;
        if (tokenView.tokens.count == 0) {
            self.ingridientsPlaceholder.hidden = NO;
        }
    }else{
        return;
    }
    c.constant = 0.;
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)tokenView:(KSTokenView *)token performSearchWithString:(NSString *)string completion:(void (^)(NSArray * _Nonnull))completion{
    if(token == self.cuisinesTokenView){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSArray *arr = [[FRStaticDataService service] getCuisinesWithSearchText:string];
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(arr);
            });
        });
    }else if(token == self.ingridientsTokenView){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSArray *arr = [[FRStaticDataService service] getIngridientsWithSearchText:string];
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(arr);
            });
        });
    }else{
        completion(@[]);
    }
}

- (BOOL)tokenView:(KSTokenView *)tokenView shouldAddToken:(KSToken *)token{
    
    if(tokenView == self.cuisinesTokenView || tokenView == self.ingridientsTokenView){
        if(!token.object){
            return NO;
        }
    }
    
    token.tokenBackgroundColor =  [UIColor aquaColor];
    
    return YES;
}

- (NSString *)tokenView:(KSTokenView *)token displayTitleForObject:(id)object{
    if([object respondsToSelector:@selector(name)]){
        return [object name];
    }
    return nil;
}

#pragma mark - flags change

- (IBAction)spiceChanged:(FRSpiceControl *)sender {
    
    // spice level can be 0
    if(self.spiceOldValue == sender.value
       && sender.value == 1){
        sender.value = 0;
    }
    self.spiceOldValue = sender.value;
    
    [self.unsavedChanges setSpiceRate:@(sender.value)];
}

- (IBAction)boolFlagChanged:(RNCCheckbox *)sender {
    [self.unsavedChanges setFlag:sender.tag value:sender.on];
    NSLog(@"%@",self.unsavedChanges);
}

#pragma mark - actions

- (void)showAlertWithText:(NSString *)text{
    UIAlertController *cntr = [UIAlertController alertControllerWithTitle:NSLocalizedString(@" ", nil) message:text preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    [cntr addAction:action];
    [self presentViewController:cntr animated:YES completion:nil];
}

-(BOOL)validateInput{
    
    if(!self.disclamerCheckbox.on){
        [self showAlertWithText:NSLocalizedString(@"STR_ALERT_SHOULD_TICK_DISCLAIMER", nil)];
        return NO;
    }
    if((!self.model && [[_unsavedChanges title] length] == 0) || ([self.unsavedChanges title] && [[self.unsavedChanges title] length] == 0)){
        [self showAlertWithText:NSLocalizedString(@"STR_ALERT_NAME_IS_EMPTY", nil)];
        return NO;
    }
    if((!self.model && [[_unsavedChanges price] floatValue] <= 0.001) || ([self.unsavedChanges price] && [[self.unsavedChanges price] floatValue] <= 0.001)){
        [self showAlertWithText:NSLocalizedString(@"STR_ALERT_NAME_PRICE_EMPTY", nil)];
        return NO;
    }
    
    NSNumber *delivery = [self.unsavedChanges deliveryType]?:[self.model deliveryType];

    if (!delivery) {
        [self showAlertWithText:NSLocalizedString(@"STR_DELIVERY_OR_PICKUP", nil)];
        return NO;
    }

    return YES;
}

- (IBAction)saveAction:(id)sender {
    [self.view endEditing:YES];
    if([self validateInput]){
        NSLog(@"%@",self.unsavedChanges);
        self.saveButton.userInteractionEnabled = NO;
        
        if (self.unsavedChanges.price.floatValue < 1) {
            self.unsavedChanges.price = @1;
        }
        else if (self.unsavedChanges.deliveryPrice.floatValue < 1) {
            self.unsavedChanges.deliveryPrice = @1;
        }
        
        if (self.isEditMode) {
            [[FRMenuService service] updateMyMealWithModel:self.unsavedChanges completion:^(id object, id rawObject, NSError *error) {
                if (self.imageView.image) {
                    FRMealExtendedModel *model = [FRMealExtendedModel modelWitDictionary:rawObject];
                    [[FRMediaUploadsService new] uploadMealImage:self.imageView.image forMealWithID:model.uid.integerValue withCompletion:^(id object, id rawObject, NSError *error) {
                        NSLog(@"image uploaded");
                    }];
                }
                if(!error)
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
        else {
            [[FRChefService service] saveMyMeal:self.unsavedChanges withCompletion:^(id object, id rawObject, NSError *error) {
                if (self.imageView.image) {
                    FRMealExtendedModel *model = [FRMealExtendedModel modelWitDictionary:rawObject];
                    [[FRMediaUploadsService new] uploadMealImage:self.imageView.image forMealWithID:model.uid.integerValue withCompletion:^(id object, id rawObject, NSError *error) {
                        NSLog(@"image uploaded");
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                }
                if(!error)
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }
    else {
        self.saveButton.userInteractionEnabled = YES;
    }
}

- (IBAction)deleteAction:(id)sender {
    
}


- (IBAction)addPhotoAction:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_PICK_DISH_IMAGE", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_FROM_CAMERA", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self presentImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_FROM_GALERY", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self presentImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - image pick

- (void)presentImagePickerWithSourceType:(UIImagePickerControllerSourceType)type{
    UIImagePickerController *cntr = [[UIImagePickerController alloc] init];
    cntr.sourceType = type;
    cntr.allowsEditing = YES;
    [cntr setBk_didCancelBlock:^(UIImagePickerController *picker) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }];
    @weakify(self);
    [cntr setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
        @strongify(self)
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        
        if (chosenImage) {
            self.deleteImageButton.hidden = NO;
            self.addMealImageButton.enabled = NO;
        }

        self.imageView.image = chosenImage;
        self.imageHeight.active = NO;
        [self.view layoutIfNeeded];
        [picker dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self presentViewController:cntr animated:YES completion:nil];
}

- (IBAction)deleteMealImageAction:(id)sender {
    NSString *imageName = [[self.model.imageURLs firstObject] lastPathComponent];

    @weakify(self);
    [[FRMediaUploadsService new] deleteImageWithName:imageName forMealWithID:self.model.uid.integerValue withCompletion:^(id object, id rawObject, NSError *error) {
        @strongify(self);
        self.imageView.image = nil;
        self.imageHeight.active = YES;
        self.deleteImageButton.hidden = YES;
        self.addMealImageButton.enabled = YES;
    }];
}

@end
