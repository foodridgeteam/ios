//
//  FRModel.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"

@implementation FRViewModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{};
}

- (NSDictionary *)dictionaryRepresentation{
    return nil;
}

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary{
    return nil;
}

+ (NSArray *)modelsFromArray:(NSArray *)array {
    
    NSMutableArray *modelsArray = [NSMutableArray new];
    for (NSDictionary *dictionary in array) {
        [modelsArray addObject:[self modelWitDictionary:dictionary]];
    }
    
    return [modelsArray copy];
}

@end
