//
//  FRNetworkService+Stripe.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 7/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService.h"

@interface FRNetworkService (Stripe)

+ (NSURLSessionTask *)deleteStripeAccountWithCompletion:(FRResponceBlock)completion;

@end
