//
//  UITextField+Price.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/15/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "UITextField+Price.h"

@implementation UITextField (Price)

-(BOOL)procesChange:(NSString *)string inRange:(NSRange)range{
    
    NSString *text             = self.text;
    NSString *decimalSeperator = [[NSLocale currentLocale] objectForKey:NSLocaleDecimalSeparator];
    
    NSCharacterSet *numberChars = [NSCharacterSet characterSetWithCharactersInString: @"0123456789.,"];
    
    
    NSString *rep = [[string componentsSeparatedByCharactersInSet:
                            [numberChars invertedSet]]
                           componentsJoinedByString:@""];
    rep = [string stringByReplacingOccurrencesOfString:@"," withString:decimalSeperator];
    rep = [string stringByReplacingOccurrencesOfString:@"." withString:decimalSeperator];
    
    NSString *str = [text stringByReplacingCharactersInRange:range withString:rep];
    
    NSArray *parts = [str componentsSeparatedByString:decimalSeperator];
    if(parts.count > 2){
        return NO;
    }
    
    if(parts.count > 1 && [parts.lastObject length] > 2){
        str = [str substringToIndex:str.length - [parts.lastObject length] + 2];
    }
//    str = [NSString stringWithFormat:@"%.2lf",[str floatValue]];
    
    self.text = str;
    return NO;
}

- (void)finishEditing{
    NSString *text = self.text;
     text = [NSString stringWithFormat:@"%.2lf",[text floatValue]];

//    }
    self.text = text;

}

//    // the number formatter will only be instantiated once ...
//    
//    static NSNumberFormatter *numberFormatter;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        numberFormatter = [[NSNumberFormatter alloc] init];
//        numberFormatter.numberStyle           = NSNumberFormatterCurrencyStyle;
//        numberFormatter.maximumFractionDigits = 10;
//        numberFormatter.minimumFractionDigits = 0;
//        numberFormatter.decimalSeparator      = decimalSeperator;
//        numberFormatter.usesGroupingSeparator = NO;
//    });
//    
//    
//    
//    // create a character set of valid chars (numbers and optionally a decimal sign) ...
//    
//    NSRange decimalRange = [text rangeOfString:decimalSeperator];
//    BOOL isDecimalNumber = (decimalRange.location != NSNotFound);
//    if (isDecimalNumber)
//    {
//        charSet = [NSCharacterSet characterSetWithCharactersInString:numberChars];
//    }
//    else
//    {
//        numberChars = [numberChars stringByAppendingString:decimalSeperator];
//        charSet = [NSCharacterSet characterSetWithCharactersInString:numberChars];
//    }
//    
//    
//    // remove amy characters from the string that are not a number or decimal sign ...
//    
//    NSCharacterSet *invertedCharSet = [charSet invertedSet];
//    NSString *trimmedString = [string stringByTrimmingCharactersInSet:invertedCharSet];
//    text = [text stringByReplacingCharactersInRange:range withString:trimmedString];
//    
//    
//    // whenever a decimalSeperator is entered, we'll just update the textField.
//    // whenever other chars are entered, we'll calculate the new number and update the textField accordingly.
//    
//    if ([string isEqualToString:decimalSeperator] == YES)
//    {
//        self.text = text;
//    }
//    else
//    {
//        NSNumber *number = [numberFormatter numberFromString:text];
//        if (number == nil)
//        {
//            number = [NSNumber numberWithInt:0];
//        }
//        self.text = isDecimalNumber ? text : [numberFormatter stringFromNumber:number];
//    }
//    
//    return NO;
//}

@end
