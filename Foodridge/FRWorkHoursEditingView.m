//
//  FRWorkHoursEditingView.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRWorkHoursEditingView.h"
#import "NSString+DateFormats.h"

@interface FRWorkHoursEditingView ()

@property (strong, nonatomic) IBOutlet UILabel *weekDayLabel;
@property (strong, nonatomic) IBOutlet UIButton *closedButton;

@property (strong, nonatomic) IBOutlet UIButton *openAtButton;
@property (strong, nonatomic) IBOutlet UIButton *closeAtButton;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *openAtHeight;
@property (strong, nonatomic) IBOutlet UIDatePicker *openAtPicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *closeAtPicker;

@property(copy, nonatomic) NSDate *openDate;
@property(copy, nonatomic) NSDate *closeDate;



@end

@implementation FRWorkHoursEditingView

- (void)awakeFromNib{
    [super awakeFromNib];
    [self.openAtPicker setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [self.closeAtPicker setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
}


- (void)setDayIndex:(NSInteger)dayIndex{
    _dayIndex = dayIndex;
    self.weekDayLabel.text = [NSString dayTitleForDayAtIndex:dayIndex];
}

- (void)setupWithOpenDate:(NSDate *)openDate closeDate:(NSDate *)closeDate{
    
    self.openAtHeight.constant = 216;
    
    self.openAtButton.alpha = 0.;
    self.closeAtButton.alpha = 1.;
    
    self.openAtPicker.alpha = 1.;
    self.closeAtPicker.alpha = 0.;
    
    [self setOpenDate:openDate];
    [self setCloseDate:closeDate];
}


- (void)setOpenDate:(NSDate *)date{
    
    if(date){
        _openDate = date;
    }else{
        _openDate = [NSDate dateWithTimeIntervalSince1970:60*60*10];
    }
    
//    if([_openDate compare:self.closeDate] == NSOrderedDescending){
//        self.closeDate = _openDate;
//    }
    
    NSString *openString = [NSString localHoursMinutesStringWithTimeZoneFromDate:_openDate];
    if(!openString){
        openString = @"-";
        
    }
    [self.openAtButton setTitle:openString forState:UIControlStateNormal];
    if(!self.closeDate){
        [self.closeAtPicker setDate:_openDate];
    }
    [self.openAtPicker setDate:_openDate];
}

- (void)setCloseDate:(NSDate *)date{

    if(date){
        _closeDate = date;
    }else{
        _closeDate = [NSDate dateWithTimeIntervalSince1970:60*60*20];
    }
    
//    if([_closeDate compare:self.openDate] == NSOrderedAscending){
//        self.openDate = _closeDate;
//    }
    
    NSString *closeString = [NSString localHoursMinutesStringWithTimeZoneFromDate:_closeDate];
    if(!closeString){
        closeString = @"-";
        
    }
    if(!self.openDate){
        [self.openAtPicker setDate:_closeDate];
    }
    [self.closeAtButton setTitle:closeString forState:UIControlStateNormal];
    [self.closeAtPicker setDate:_closeDate];
}


- (IBAction)openDateChanged:(UIDatePicker *)sender {
    [self setOpenDate:sender.date];
}

- (IBAction)closeDateChanged:(UIDatePicker *)sender {
    [self setCloseDate:sender.date];
}


- (IBAction)closedAction:(id)sender {
    self.onDone(NO, nil, nil);
}

- (IBAction)openEditAction:(id)sender {
    self.openAtHeight.constant = 216;
    [UIView animateWithDuration:0.25 animations:^{
        self.openAtButton.alpha = 0.;
        self.closeAtButton.alpha = 1.;
        
        self.openAtPicker.alpha = 1.;
        self.closeAtPicker.alpha = 0.;
        [self layoutIfNeeded];
    }];
    
}
- (IBAction)closeAtAction:(id)sender {
    self.openAtHeight.constant = 50;
    [UIView animateWithDuration:0.25 animations:^{
        
        self.openAtButton.alpha = 1.;
        self.closeAtButton.alpha = 0.;
        
        self.openAtPicker.alpha = 0.;
        self.closeAtPicker.alpha = 1.;
        [self layoutIfNeeded];
    }];
}
- (IBAction)cancelAction:(id)sender {
    self.onDone(YES, nil, nil);
}

- (IBAction)saveAction:(id)sender {
    self.onDone(NO, self.openDate, self.closeDate);
    
}

@end
