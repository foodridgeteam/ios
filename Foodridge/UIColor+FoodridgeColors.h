//
//  UIColor+foodridgeColors.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (FoodridgeColors)

+ (UIColor *)grayButtonColor;

+ (UIColor *)aquaColor;

+ (UIColor *)barColor;

+ (UIColor *)barTextColor;

+ (UIColor *)segmentLineColor;

+ (UIColor *)alertRedColor;

+ (UIColor *)stripeButtonColor;

+ (UIColor *)grayLablesColor;

+ (UIColor *)commentsBlueColor;

+ (UIColor *)stripeConnectButtonColor;

@end
