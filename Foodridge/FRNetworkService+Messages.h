//
//  FRNetworkService+Messages.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService.h"

@interface FRNetworkService (Messages)

+ (NSURLSessionTask *)getMessagesForOrderID:(NSInteger)orderID lastID:(NSInteger)lastID completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)leaveMessageForOrderWithID:(NSInteger)orderID messageText:(NSString *)messageText completion:(FRResponceBlock)completion;

@end
