//
//  FRSelectionFormViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRSelectionFormViewController.h"

@implementation FRSelectionFormViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setupModel];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    UIView *headerView = self.tableView.tableHeaderView;
    CGRect newFrame = headerView.frame;
    newFrame.size.height = CGRectGetMaxY(self.view.frame) - CGRectGetMaxY(self.navigationController.navigationBar.frame);
    headerView.frame = newFrame;
    [self.tableView setTableHeaderView:headerView];
    
}

- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupModel{
    //abstract
}

@end
