//
//  FRCommentsService.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FRNetworkService+Comments.h"

@interface FRCommentsService : NSObject

+ (FRCommentsService *)service;

- (NSURLSessionTask *)getCommentsForMealWithID:(NSInteger)mealID completion:(FRResponceBlock)completion;

- (NSURLSessionTask *)leaveCommentForOrderWithID:(NSInteger)orderID
                                     commentText:(NSString *)commentText
                                          rating:(NSInteger)rating
                                      completion:(FRResponceBlock)completion;

@end
