//
//  FROrder.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/22/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrderModel.h"
#import "FRMealExtendedModel.h"
#import "FRProfileModel.h"
#import "FRAuthorisationService.h"
#import "FRBuisinessInfoModel.h"
#import "FRDeliveryInfoModel.h"
#import "FRDateHelper.h"
#import "FROrderMessageModel.h"
#import "FRMessagesService.h"

@implementation FROrderModel

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary{
    
    FROrderModel *orderModel = [FROrderModel new];
    
    orderModel.uid = [dictionary objectOrNilForKey:@"id" classObject:[NSNumber class]];
    orderModel.deliveryType = [[dictionary objectOrNilForKey:@"deliveryType" classObject:[NSNumber class]] integerValue];
    orderModel.meal = [FRMealExtendedModel modelWitDictionary:[dictionary objectOrNilForKeypath:@"meal" classObject:[NSDictionary class]]];
    orderModel.count = [[dictionary objectOrNilForKey:@"quantity" classObject:[NSNumber class]] integerValue];
    orderModel.status = [[dictionary objectOrNilForKey:@"status" classObject:[NSNumber class]] integerValue];
    orderModel.customer = [FRProfileModel modelWitDictionary:[dictionary objectOrNilForKeypath:@"client" classObject:[NSDictionary class]]];
    if(orderModel.customer.uid.integerValue == [FRAuthorisationService currentUID]){
        orderModel.asReceiver = YES;
    }
    
    if(orderModel.deliveryType == FRDeliveryTypePickup){
        orderModel.deliveryAdress = [FRDeliveryInfoModel modelWitDictionary:[dictionary objectOrNilForKeypath:@"meal.chef.deliveryInfo" classObject:[NSDictionary class]]];
    }else{
        orderModel.deliveryAdress = [FRDeliveryInfoModel modelWitDictionary:[dictionary objectOrNilForKey:@"clientDeliveryInfo" classObject:[NSDictionary class]]];
    }
    
    orderModel.businessInfo = [FRBuisinessInfoModel modelWitDictionary:[dictionary objectOrNilForKeypath:@"meal.chef.businessInfo" classObject:[NSDictionary class]]];
    
    orderModel.sum = [[dictionary objectOrNilForKey:@"totalSum"] doubleValue];
    orderModel.date = [FRDateHelper dateFromString:[dictionary objectOrNilForKey:@"created" classObject:[NSString class]]];
    orderModel.comment = [FRCommentsModel modelWitDictionary:[dictionary objectOrNilForKey:@"comment" classObject:[NSDictionary class]]];
    
    return orderModel;
}

@end
