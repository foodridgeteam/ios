//
//  FRIngridientsCell.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRIngridientModel.h"

@interface FRIngridientsCell : UITableViewCell

- (void)setupCellWithIngridientModel:(FRIngridientModel *)ingridient;

@end
