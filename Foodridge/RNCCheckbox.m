//
//  RNCCheckbox.m
//  EndyMed
//
//  Created by Vitaly Evtushenko on 7/29/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import "RNCCheckbox.h"

@implementation RNCCheckbox {
    BOOL _initCompleted;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];

    if (self) {
        [self doInit];
    }

    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];

    [self doInit];
}

- (void)doInit {
    if (_initCompleted) {
        return;
    }

    _initCompleted = YES;

    [self addTarget:self action:@selector(_reverseCheckbox:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)_reverseCheckbox:(id)sender {
    self.on = !self.isOn;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)setOn:(BOOL)on {
    _on = on;

    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    UIImage *image = [UIImage imageNamed:self.isOn ? @"switch_on" : @"switch_off"];
    
    [image drawInRect:rect];
}

@end
