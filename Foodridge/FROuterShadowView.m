          //
//  FROuterShadowView.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROuterShadowView.h"

@implementation FROuterShadowView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.layer.cornerRadius = 3.0;
    self.layer.shadowRadius  = 1.5f;
    self.layer.shadowColor   = [UIColor blackColor].CGColor;
    self.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    self.layer.shadowOpacity = 0.3f;
    self.layer.masksToBounds = NO;
    
}

- (void)layoutSubviews {
    [super layoutSubviews];

    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, 0, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.bounds, shadowInsets)];
    self.layer.shadowPath    = shadowPath.CGPath;
    
    [self.layer setNeedsDisplay];
    [self.layer layoutIfNeeded];
}

@end
