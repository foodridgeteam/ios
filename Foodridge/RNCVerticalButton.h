//
//  RNCVerticalButton.h
//  Foodridge
//
//  Created by Vitaly Evtushenko on 7/29/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  vertically aligned button image and title
 */
@interface RNCVerticalButton : UIButton

@end
