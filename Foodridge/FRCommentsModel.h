//
//  FRCommentsModel.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/22/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"
#import "FRProfileModel.h"

@interface FRCommentsModel : FRViewModel

@property (nonatomic, copy) NSNumber *orderId;
@property (nonatomic, copy) NSNumber *mealId;
@property (nonatomic, copy) NSDate *creationDate;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSNumber *rating;
@property (nonatomic, strong) FRProfileModel *userProfile;

@end
