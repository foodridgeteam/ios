//
//  ChefsListViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRChefsListViewController.h"
#import "FRChefsService.h"
#import "FRChefDetailViewController.h"
#import "FRChefProfileModel.h"
#import "FRChefCell.h"
#import "FRNetworkService+Chefs.h"
#import "FREmptyDataView.h"

@interface FRChefsListViewController ()

@property (nonatomic, strong) NSArray *chefs;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, copy) NSString *searchString;
@property (nonatomic, strong) FREmptyDataView *emptyDataView;

@property(nonatomic, strong) FRChefsService *service;

@end

@implementation FRChefsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    
    [self setupModel:nil];
    
    [self.service chefListWithSearchText:nil completion:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupModel:) name:FRChefsUpdatedNotificationName object:nil];

//    self.tableView.contentInset = UIEdgeInsetsMake(-self.searchBar.frame.size.height, self.tableView.contentInset.left, self.tableView.contentInset.bottom, self.tableView.contentInset.right);
//    self.searchBar.hidden = YES;
//    [self.view sendSubviewToBack:self.searchBar];
    
    self.emptyDataView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FREmptyDataView class]) owner:self options:nil] objectAtIndex:0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = NSLocalizedString(@"STR_CHEFS", nil);
}

-(FRChefsService *)service{
    if (!_service){
        _service = [FRChefsService service];
    }
    return _service;
}

- (void)setupModel:(NSDictionary *)userInfo {
    self.chefs = [self.service items];
    
    if (!self.chefs.count) {
        self.emptyDataView.filterType = FREmptyDataTypeChefs;
        CGRect tableViewFrameWithoutSearchBar = CGRectMake(self.tableView.frame.origin.x,
                                                           self.tableView.frame.origin.y + self.searchBar.frame.size.height,
                                                           self.tableView.frame.size.width,
                                                           self.tableView.frame.size.height);
        self.emptyDataView.frame = tableViewFrameWithoutSearchBar;
        if (![self.emptyDataView isDescendantOfView:self.tableView]) {
            [self.tableView addSubview:self.emptyDataView];
            [self.searchBar resignFirstResponder];
        }
    }
    else {
        [self.emptyDataView removeFromSuperview];
    }
    
    
    [self.tableView reloadData];
    [self.refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:0.05];
}

- (void)pullToRefresh {
    [self.service clearData];
    [self.service chefListWithSearchText:nil completion:nil];
}

- (void)reloadModel {
    
    [self.service clearData];
    [self.service chefListWithSearchText:nil completion:nil];

    self.chefs = [[FRDynamicDataLoadService service] items];
    
    [self.tableView reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.chefs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FRChefCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (self.chefs.count > indexPath.row) {
    FRChefProfileModel *profileModel = [self.chefs objectAtIndex:indexPath.row];
    [cell setupCellWithChefModel:profileModel];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == self.chefs.count - 1 && self.service.loadMore) {
        [self.service chefListWithSearchText:self.searchString completion:nil];
    }
}

- (IBAction)searchButtonAction:(id)sender {
    self.searchBar.hidden = NO;
    [self.searchBar becomeFirstResponder];
    [self showSearchBar:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    self.searchBar.showsCancelButton = YES;
    return YES;
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    self.searchString = [searchBar.text stringByReplacingCharactersInRange:range withString:text];
    [self.service clearData];
    [self.service chefListWithSearchText:self.searchString completion:nil];
    
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchBar.text = nil;
    self.searchString = nil;
//    self.searchBar.hidden = YES;
    [self.searchBar resignFirstResponder];
//    [self showSearchBar:NO];
    [self.service clearData];
    [self.service chefListWithSearchText:nil completion:nil];
}

- (void)showSearchBar:(BOOL)show {
    
    CGFloat statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    CGFloat fullHeight = statusBarHeight + self.searchBar.frame.size.height;

    if (show) {
        self.tableView.contentInset = UIEdgeInsetsMake(fullHeight, self.tableView.contentInset.left, self.tableView.contentInset.bottom, self.tableView.contentInset.right);
        [self.tableView setContentOffset:CGPointMake(self.tableView.contentOffset.x, -fullHeight) animated:YES];
    }
    else {
        self.tableView.contentInset = UIEdgeInsetsMake(statusBarHeight, self.tableView.contentInset.left, self.tableView.contentInset.bottom, self.tableView.contentInset.right);;
        [self.tableView setContentOffset:CGPointMake(self.tableView.contentOffset.x, -statusBarHeight) animated:YES];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"ChefDetail" bundle:nil];
    FRChefDetailViewController *vc = [sb instantiateInitialViewController];
    if (self.chefs.count > indexPath.row) {
        FRChefProfileModel *profile = [self.chefs objectAtIndex:indexPath.row];
        vc.chefProfile = profile;
        vc.chefUid = profile.uid;
    }

    [self.navigationController pushViewController:vc animated:YES];
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:FRChefsUpdatedNotificationName object:nil];
}

@end
