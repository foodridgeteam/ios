//
//  UIFont+FoodridgeFonts.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "UIFont+FoodridgeFonts.h"

@implementation UIFont (FoodridgeFonts)

+ (UIFont *)openSansFontWithSize:(CGFloat)size{
    return [UIFont fontWithName:@"OpenSans" size:size];
}

+ (UIFont *)openSansBoldFontWithSize:(CGFloat)size{
    return [UIFont fontWithName:@"OpenSans-Bold" size:size];
}

+ (UIFont *)openSansItalicFontWithSize:(CGFloat)size{
    return [UIFont fontWithName:@"OpenSans-Italic" size:size];
}

@end
