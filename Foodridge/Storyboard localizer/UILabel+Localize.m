//
//  UILabel+Localize.m

//
//  Created by Maxym Deygin on 5/4/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import "UILabel+Localize.h"

@implementation UILabel (Localize)

- (void)localize{
    [self localizeWithOptions:THLocalizationOptionsNone];
}

- (void)localizeWithOptions:(THLocalizationOptions)options {
    
    NSString *text = NSLocalizedString(self.text,@"");
    if(options == THLocalizationOptionsLowercase){
        text = [text lowercaseString];
    }else if(options == THLocalizationOptionsUppercase){
        text = [text uppercaseString];
    }
    
    self.text = text;
}


@end
