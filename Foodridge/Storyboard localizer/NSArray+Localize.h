//
//  NSArray+Localize.h

//
//  Created by Maxym Deygin on 8/25/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *  options for localization
 */
typedef NS_ENUM(NSInteger, THLocalizationOptions) {
    /**
     *  normal
     */
    THLocalizationOptionsNone,
    /**
     *  uppercase
     */
    THLocalizationOptionsUppercase,
    /**
     *  lowercase
     */
    THLocalizationOptionsLowercase
};

/**
 *  localize arrays of objects
 */
@interface NSArray (Localize)

/**
 *  localize objects in array
 */
- (void)localize;

/**
 *  localize objects in array
 *
 *  @param options options for localization
 */
- (void)localizeWithOptions:(THLocalizationOptions) options;

@end
