//
//  UIViewController+Localize.m

//
//  Created by Maxym Deygin on 5/4/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import "UIViewController+Localize.h"

@implementation UIViewController (Localize)

- (void)localize{
    [self localizeWithOptions:THLocalizationOptionsNone];
}


- (void)localizeWithOptions:(THLocalizationOptions)options{
    if(self.navigationController){
        [self.navigationController localizeWithOptions:options];
    }
    NSString *tabbarTitle = NSLocalizedString(self.tabBarItem.title, nil);
    NSString *navTitle = NSLocalizedString(self.navigationItem.title, nil);
    
    if(options == THLocalizationOptionsLowercase){
        tabbarTitle = [tabbarTitle lowercaseString];
        navTitle = [navTitle lowercaseString];
    }else if(options == THLocalizationOptionsUppercase){
        tabbarTitle = [tabbarTitle uppercaseString];
        navTitle = [navTitle uppercaseString];
    }
    self.tabBarItem.title = tabbarTitle;
    self.navigationItem.title = navTitle;
}

@end
