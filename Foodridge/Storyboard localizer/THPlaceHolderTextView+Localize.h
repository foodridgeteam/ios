//
//  THPlaceHolderTextView+Localize.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "THPlaceHolderTextView.h"
#import "NSArray+Localize.h"

@interface THPlaceHolderTextView (Localize)

/**
 *  localize object
 */
- (void)localize;

/**
 *  localize object
 *
 *  @param options options for localization
 */
- (void)localizeWithOptions:(THLocalizationOptions) options;


@end
