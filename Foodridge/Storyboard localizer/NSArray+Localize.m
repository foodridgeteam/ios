//
//  NSArray+Localize.m

//
//  Created by Maxym Deygin on 8/25/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import "NSArray+Localize.h"

@implementation NSArray (Localize)

- (void)localize{
    [self localizeWithOptions:THLocalizationOptionsNone];
}

- (void)localizeWithOptions:(THLocalizationOptions)options{
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([obj respondsToSelector:@selector(localizeWithOptions:)]){
            [obj localizeWithOptions:options];
        }
    }];
}

@end
