//
//  UIButton+Localize.m

//
//  Created by Maxym Deygin on 5/4/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import "UIButton+Localize.h"

@implementation UIButton (Localize)

- (void)localizeWithOptions:(THLocalizationOptions)options{
    NSString *title = NSLocalizedString([self titleForState:UIControlStateNormal],@"");
    if(options == THLocalizationOptionsLowercase){
        title = [title lowercaseString];
    }else if(options == THLocalizationOptionsUppercase){
        title = [title uppercaseString];
    }
    [self setTitle:title forState:UIControlStateNormal];
}

- (void)localize{
    [self localizeWithOptions:THLocalizationOptionsNone];
}

@end
