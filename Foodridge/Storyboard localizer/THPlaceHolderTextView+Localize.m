//
//  THPlaceHolderTextView+Localize.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "THPlaceHolderTextView+Localize.h"
@import Foundation;


@implementation THPlaceHolderTextView (Localize)

- (void)localize{
    [self localizeWithOptions:THLocalizationOptionsNone];
}

- (void)localizeWithOptions:(THLocalizationOptions)options {
    
    NSString *placeHolder = NSLocalizedString(self.placeholder, @"");
    if(options == THLocalizationOptionsLowercase){
        
        placeHolder = [placeHolder lowercaseString];
    }else if(options == THLocalizationOptionsUppercase){
        
        placeHolder = [placeHolder uppercaseString];
    }
    
    
    self.placeholder = placeHolder;
}

@end
