//
//  UITextField+Localize.m

//
//  Created by Maxym Deygin on 5/4/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import "UITextField+Localize.h"

@implementation UITextField (Localize)

- (void)localize{
    [self localizeWithOptions:THLocalizationOptionsNone];
}

- (void)localizeWithOptions:(THLocalizationOptions)options {
    
    NSString *text = NSLocalizedString(self.text,@"");
    NSString *placeHolder = NSLocalizedString(self.placeholder,@"");
    if(options == THLocalizationOptionsLowercase){
        text = [text lowercaseString];
        placeHolder = [placeHolder lowercaseString];
    }else if(options == THLocalizationOptionsUppercase){
        text = [text uppercaseString];
        placeHolder = [placeHolder uppercaseString];
    }
    
    self.text = text;
    self.placeholder = placeHolder;
}


@end
