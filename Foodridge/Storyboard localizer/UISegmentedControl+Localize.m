//
//  UISegmentedControl+Localize.m

//
//  Created by Maxym Deygin on 12/23/15.
//  Copyright © 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import "UISegmentedControl+Localize.h"

@implementation UISegmentedControl (Localize)

- (void)localizeWithOptions:(THLocalizationOptions)options{
    for(NSInteger i = 0; i < self.numberOfSegments; i++){
        NSString *title = [self titleForSegmentAtIndex:i];
        title = NSLocalizedString(title, nil);
        if(options == THLocalizationOptionsLowercase){
            title = [title lowercaseString];
        }else if(options == THLocalizationOptionsUppercase){
            title = [title uppercaseString];
        }
        [self setTitle:title forSegmentAtIndex:i];
    }

}

- (void)localize{
    [self localizeWithOptions:THLocalizationOptionsNone];
}


@end
