//
//  UISegmentedControl+Localize.h

//
//  Created by Maxym Deygin on 12/23/15.
//  Copyright © 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSArray+Localize.h"

/**
 *  localize object
 */
@interface UISegmentedControl (Localize)

/**
 *  localize object
 */
- (void)localize;

/**
 *  localize object
 *
 *  @param options options for localization
 */
- (void)localizeWithOptions:(THLocalizationOptions) options;


@end
