//
//  UIViewController+Localize.h

//
//  Created by Maxym Deygin on 5/4/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSArray+Localize.h"

/**
 *  localize object
 */
@interface UIViewController (Localize)

/**
 *  localize object
 */
- (void)localize;

/**
 *  localize object
 *
 *  @param options options for localization
 */
- (void)localizeWithOptions:(THLocalizationOptions) options;

@end
