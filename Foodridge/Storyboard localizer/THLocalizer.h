//
//  THLocalizer.h

//
//  Created by Maxym Deygin on 8/25/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  THLocalizer is used for localizing labels, buttons, textfields - any controls. simply create instance, and set array of localizable controls. 
 *  Text in control shouuld contain key for localized string. 
 *  You can make localized string uppercase, or lowercase.
 *  Can be created from nib.
 */
@interface THLocalizer : NSObject

@property (weak, nonatomic) IBOutletCollection(id) NSArray *localizableObjects;
@property (weak, nonatomic) IBOutletCollection(id) NSArray *localizableUppercase;
@property (weak, nonatomic) IBOutletCollection(id) NSArray *localizableLowercase;

@end
