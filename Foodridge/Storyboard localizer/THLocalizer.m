//
//  THLocalizer.m

//
//  Created by Maxym Deygin on 8/25/15.
//  Copyright (c) 2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import "THLocalizer.h"
#import "NSArray+Localize.h"

@interface THLocalizer ()

@end

@implementation THLocalizer

- (void)setLocalizableObjects:(NSArray *)localizableObjects{
    [localizableObjects localize];
    _localizableObjects = localizableObjects;
    
}

- (void)setLocalizableLowercase:(NSArray *)localizableLowercase{
    [localizableLowercase localizeWithOptions:THLocalizationOptionsLowercase];
    _localizableLowercase = localizableLowercase;
}

- (void)setLocalizableUppercase:(NSArray *)localizableUppercase{
    [localizableUppercase localizeWithOptions:THLocalizationOptionsUppercase];
    _localizableUppercase = localizableUppercase;
}

@end
