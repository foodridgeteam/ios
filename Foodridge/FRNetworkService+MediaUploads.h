//
//  FRNetworkService+MediaUploads.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService.h"

typedef NS_ENUM(NSInteger, FRMediaType) {
    FRMediaTypeAvatar,
    FRMediaTypeMealPhoto,
};

@interface FRNetworkService (MediaUploads)

+ (NSURLSessionTask *)uploadMedia:(NSString *)localPath
                             type:(FRMediaType)type
                              uid:(NSInteger)uid
                       completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)deleteMediaWithName:(NSString *)mediaName forMealWithID:(NSInteger)mealID completion:(FRResponceBlock)completion;


@end
