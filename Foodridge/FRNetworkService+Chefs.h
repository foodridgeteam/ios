//
//  FRNetworkService+Chefs.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService.h"

@interface FRNetworkService (Chefs)

+ (NSURLSessionTask *)chefsListWithLimit:(NSInteger)limit
                                  offset:(NSInteger)offset
                                followed:(BOOL)followed
                              searchText:(NSString *)searchText
                              completion:(FRResponceBlock)completion;


+ (NSURLSessionTask *)followChef:(NSNumber*)chefId
                             and:(bool)follow
                      completion:(FRResponceBlock)completion;

/*+ (NSURLSessionTask *)mealListForChefWithID:(NSInteger)chefID
                                      limit:(NSInteger)limit
                                     offset:(NSInteger)offset
                                 completion:(FRResponceBlock)completion;*/

+ (NSURLSessionTask *)getChefWithId:(NSNumber*)chefId
                         completion:(FRResponceBlock)completion;


@end
