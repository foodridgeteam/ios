//
//  FRDeliveryInfoViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/8/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRDeliveryInfoViewController.h"
#import "FRDeliveryInfoModel.h"

@interface FRDeliveryInfoViewController ()


@property (strong, nonatomic) IBOutlet UITextField *streetTextField;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UITextField *regionTextField;

@end

@implementation FRDeliveryInfoViewController

- (void)viewDidLoad{
    [super viewDidLoad];
}

-(void)setupModel{
    [super setupModel];
    self.streetTextField.text = self.model.street;
    self.cityTextField.text = self.model.city;
    self.regionTextField.text = self.model.country;
}


- (IBAction)saveAction:(id)sender {
    FRDeliveryInfoModel *model = [FRDeliveryInfoModel new];
    model.street = self.streetTextField.text;
    model.city = self.cityTextField.text;
    model.country = self.regionTextField.text;
    
    self.onSave(model);
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == self.streetTextField){
        [self.cityTextField becomeFirstResponder];
    }else if(textField == self.cityTextField){
        [self.regionTextField becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}



@end
