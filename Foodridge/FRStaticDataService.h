//
//  FRTagsSearchManager.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRStaticDataService : NSObject

+ (FRStaticDataService *)service;

- (NSArray *)cuisines;

- (NSArray *)getCuisinesWithIDs:(NSArray *)ids;
- (NSArray *)getCuisinesWithSearchText:(NSString *)text;

- (NSArray *)getIngridientsWithIDs:(NSArray *)ids;
- (NSArray *)getIngridientsWithSearchText:(NSString *)text;


- (void)reloadCuisines;

- (void)reloadIngridients;

//+ (NSArray *)cuisinesWithSearchString:(NSString *)searchText;

@end
