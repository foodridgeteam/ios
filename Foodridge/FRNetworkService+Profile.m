//
//  FRNetworkService+Profile.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/8/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService+Profile.h"
#import "FRProfileModel.h"
#import "FRChefProfileModel.h"


@implementation FRNetworkService (Profile)

+ (NSURLSessionTask *)myProfileWithCompletion:(FRResponceBlock)completion{
    return [self requestWithMethod:@"GET"
                    requestAddress:@"user_profile"
                        parameters:nil
                        completion:completion];
    
}

+ (NSURLSessionTask *)myChefWithCompletion:(FRResponceBlock)completion{
    return [self requestWithMethod:@"GET"
                    requestAddress:@"chef_profile"
                        parameters:nil
                        completion:completion];

}

+ (NSURLSessionTask *)profileWithUid:(NSInteger)uid
                          completion:(FRResponceBlock)completion{
    return [self requestWithMethod:@"GET"
                    requestAddress:[NSString stringWithFormat:@"get_user/%ld", (long)uid]
                        parameters:nil
                        completion:completion];

}


+ (NSURLSessionTask *)updateProfile:(FRProfileModel *)model
                         completion:(FRResponceBlock)completion{
    
    NSDictionary *params = @{@"userProfile":[model dictionaryRepresentation]};

    return [self requestWithMethod:@"PUT"
                    requestAddress:@"user_profile"
                        parameters:params
                        completion:completion];

}

+ (NSURLSessionTask *)updatePushToken:(NSString *)token completion:(FRResponceBlock)completion{
  
    static NSInteger flag = 2;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
#ifdef DEBUG
        flag = 1;
#endif
    });
    
    NSDictionary *params = @{@"pushId":token,
                             @"push_flag":@(flag)};
    
    return [self requestWithMethod:@"POST"
                    requestAddress:@"set_pushid"
                        parameters:params
                        completion:completion];
    
}

+ (NSURLSessionTask *)updateChefProfile:(FRChefProfileModel *)model completion:(FRResponceBlock)completion{
    NSDictionary *params = @{@"chefProfile":[model dictionaryRepresentation]};
    
    return [self requestWithMethod:@"PUT"
                    requestAddress:@"chef_profile"
                        parameters:params
                        completion:completion];

}

+ (NSURLSessionTask *)saveMyMeal:(FRViewModel<FRMealModel> *)model completion:(FRResponceBlock)completion{
    
    NSDictionary *params = [model dictionaryRepresentation];
    
    return [self requestWithMethod:@"POST"
                    requestAddress:@"meal"
                        parameters:params
                        completion:completion];

}

@end
