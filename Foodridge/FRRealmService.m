//
//  RealmService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRRealmService.h"

@implementation FRRealmService

- (void)clear{
    
    [[NSFileManager defaultManager] removeItemAtPath:[self.class currentRealm].path error:nil];
}

+ (RLMRealm *)currentRealm{
    return [RLMRealm defaultRealm];

}

- (void)writeObject:(RLMObject *)object withCompletion:(dispatch_block_t)completion{
    // Get the default Realm
    RLMRealm *realm = [self.class currentRealm];
    
    // Add to Realm with transaction
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:object];
    [realm commitWriteTransaction];
    
    if (completion) {
        completion();
    }
    
}

- (void)writeObjects:(NSArray *)objects withCompletion:(dispatch_block_t)completion{
    // Get the default Realm
    RLMRealm *realm = [self.class currentRealm];
    
    // Add to Realm with transaction
    [realm beginWriteTransaction];
    
    [realm addOrUpdateObjectsFromArray:objects];
    
    [realm commitWriteTransaction];
    
    if (completion) {
        completion();
    }
}

@end
