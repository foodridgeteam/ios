//
//  FROrdersService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FROrderModel.h"
#import "FRNetworkService+Orders.h"
#import "FROrderModel.h"
#import "FROrderMessageModel.h"
#import "FRMealModel.h"
#import "FRChefProfileModel.h"

static NSString *FROrderUpdatedNotificationName = @"FROrderUpdatedNotification";
static NSString *FROrderNewMessageNotificationName = @"FROrderNewMessageNotification";
static NSString *FROrderNewReviewNotificationName = @"FROrderNewReviewNotification";

typedef NS_ENUM(NSInteger, FROrdersFilter){
    FROrdersFilterReceived,
    FROrdersFilterPlaced,
    FROrdersFilterCompleted,
};

@interface FROrdersService : NSObject

+ (FROrdersService *)service;

//call by push notification service, or websocket service, that handles notifications from server
- (void)processOrderNotification:(NSDictionary *)data;

- (NSURLSessionTask *)refreshOrdersListWithCompletion:(FRResponceBlock)completion;
- (NSURLSessionTask *)refreshCompletedOrdersWithCompletion:(FRResponceBlock)completion;


/*- (NSURLSessionTask *)getOrderWithID:(NSInteger)uid completion:(FRResponceBlock)completion;*/

- (NSURLSessionTask *)createOrder:(NSInteger)mealID
                         quantity:(NSInteger)count
                      stripeToken:(NSString *)token
                         checkSum:(long)sum
                     deliveryType:(FRDeliveryType)type
                       completion:(FRResponceBlock)completion;

- (NSURLSessionTask *)setOrderStatus:(NSInteger)orderStatus
                      forOrderWithID:(NSInteger)orderID
                          completion:(FRResponceBlock)completion;

- (NSArray *)ordersWithFilter:(FROrdersFilter)filter;

@end
