//
//  NSString+DateFormats.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "NSString+DateFormats.h"
#import "UIFont+FoodridgeFonts.h"
#import <UIKit/UIKit.h>

@implementation NSString (DateFormats)

+ (NSString *)localHoursMinutesStringFromDate:(NSDate *)date{
    static NSDateFormatter *frm = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        frm = [[NSDateFormatter alloc] init];
        //frm.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [frm setTimeZone:[NSTimeZone localTimeZone]];
        [frm setDateStyle:NSDateFormatterNoStyle];
        [frm setTimeStyle:NSDateFormatterShortStyle];
    });
    return [frm stringFromDate:date];
}

+ (NSString *)localHoursMinutesStringWithTimeZoneFromDate:(NSDate *)date{
    static NSDateFormatter *frm = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        frm = [[NSDateFormatter alloc] init];
        frm.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [frm setDateStyle:NSDateFormatterNoStyle];
        [frm setTimeStyle:NSDateFormatterShortStyle];
    });
    return [frm stringFromDate:date];
}

+ (NSString *)dayTitleForDayAtIndex:(NSInteger)index{
    switch (index%7) {
        case 0:
            return NSLocalizedString(@"STR_MONDAY", nil);
        case 1:
            return NSLocalizedString(@"STR_TUESDAY", nil);
        case 2:
            return NSLocalizedString(@"STR_WEDNESDAY", nil);
        case 3:
            return NSLocalizedString(@"STR_THURSDAY", nil);
        case 4:
            return NSLocalizedString(@"STR_FRIDAY", nil);
        case 5:
            return NSLocalizedString(@"STR_SATURDAY", nil);
        case 6:
            return NSLocalizedString(@"STR_SUNDAY", nil);
        default:
        return nil;
    }
}

+(NSDateFormatter *)HHmmDateFormatter{
    static NSDateFormatter *frm = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        frm = [[NSDateFormatter alloc] init];
        frm.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [frm setLocale:locale];
        //[frm setTimeZone:[NSTimeZone localTimeZone]];
        [frm setDateFormat:@"HH:mm"];
    });
    return frm;
}

+ (NSString *)HHmmFromDate:(NSDate *)date{
      return [[self HHmmDateFormatter] stringFromDate:date];
}

- (NSDate *)asHHmmDate{
    return [[self.class HHmmDateFormatter] dateFromString:self];
}

+ (NSAttributedString *)orderCreationTimeFromDate:(NSDate *)date{
    static NSDateFormatter *frm = nil;
    static NSDateFormatter *frm1 = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        frm = [[NSDateFormatter alloc] init];
        
        [frm setDateStyle:NSDateFormatterNoStyle];
        [frm setTimeStyle:NSDateFormatterShortStyle];
        
        frm1 = [[NSDateFormatter alloc] init];
        [frm1 setDateFormat:@"dd MMM"];

    });

    NSString *dateStrign = [frm1 stringFromDate:date];
    NSString *timeString = [frm stringFromDate:date];
    if(!dateStrign || !timeString){
        return nil;
    }
     NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@, ",dateStrign] attributes:@{NSFontAttributeName :[UIFont openSansFontWithSize:14]}];
    
     [str appendAttributedString:[[NSAttributedString alloc] initWithString:timeString attributes:@{NSFontAttributeName :[UIFont openSansBoldFontWithSize:14]}]];
    return str;
    
}

@end
