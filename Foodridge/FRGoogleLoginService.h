//
//  FRGoogleLoginService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import UIKit;
@import Foundation;

typedef void (^FRGoogleLoginBlock)(NSString *token, NSError *error);

@interface FRGoogleLoginService : NSObject

+ (FRGoogleLoginService *)service;


- (void)loginFromViewController:(UIViewController *)cntr completion:(FRGoogleLoginBlock)completion;

@end
