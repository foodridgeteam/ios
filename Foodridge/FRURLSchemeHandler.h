//
//  FRURLSchemeHandler.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRURLSchemeHandler : NSObject

- (BOOL)handleURL:(NSURL *)url;

@end
