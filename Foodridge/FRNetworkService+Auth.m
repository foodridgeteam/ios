//
//  FRNetworkService+Auth.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService+Auth.h"


@implementation FRNetworkService (Auth)

+ (NSURLSessionTask *)registerWithEmail:(NSString *)email
                               username:(NSString *)username
                               password:(NSString *)password
                               deviceId:(NSString *)deviceId
                             completion:(FRResponceBlock)completion{
    
    NSDictionary *params = @{@"email":email,
                             @"username":username,
                             @"password":password,
                             @"deviceType":@2,
                             @"deviceId":deviceId};
    
    
    return [self requestWithMethod:@"POST"
                    requestAddress:@"register"
                        parameters:params
                        completion:completion];
}



+ (NSURLSessionTask *)authWithEmail:(NSString *)email
                          password:(NSString *)password
                          deviceId:(NSString *)deviceId
                        completion:(FRResponceBlock)completion {
    
    NSDictionary *params = @{@"email":email,
                             @"password":password,
                             @"deviceType":@2,
                             @"deviceId":deviceId};
    
    return [self requestWithMethod:@"POST"
                    requestAddress:@"login"
                        parameters:params
                        completion:completion];
    
}

+ (NSURLSessionTask *)authWithFacebookToken:(NSString *)token
                                        uid:(NSString *)uid
                                   deviceId:(NSString *)deviceId
                                 completion:(FRResponceBlock)completion{
    
    NSDictionary *params = @{@"socialToken":token,
                             @"socialUserId":uid,
                             @"socialType":@1,
                             @"deviceType":@2,
                             @"deviceId":deviceId};
    
    return [self requestWithMethod:@"POST"
                    requestAddress:@"login"
                        parameters:params
                        completion:completion];

    
}

+ (NSURLSessionTask *)authWithGooglePlusToken:(NSString *)token
                                     deviceId:(NSString *)deviceId
                                   completion:(FRResponceBlock)completion{
    
    NSDictionary *params = @{@"socialToken":token,
                             @"socialType":@2,
                             @"deviceType":@2,
                             @"deviceId":deviceId};
    
    return [self requestWithMethod:@"POST"
                    requestAddress:@"login"
                        parameters:params
                        completion:completion];


    
}

+ (NSURLSessionTask *)forgotPasswordWithEmail:(NSString *)email completion:(FRResponceBlock)completion{
    NSDictionary *params = @{@"email":email};
    
    return [self requestWithMethod:@"POST"
                    requestAddress:@"forgot"
                        parameters:params
                        completion:completion];

}

+ (void)logout{
    [self requestWithMethod:@"GET"
             requestAddress:@"logout"
                 parameters:nil
                 completion:^(id object, id rawObject, NSError *error) {
                     NSLog(@"%@",rawObject);
                 }];
}

+ (NSURLSessionTask *)termsAggreedCompletion:(FRResponceBlock)completion{
    return [self requestWithMethod:@"PUT"
                    requestAddress:@"terms"
                        parameters:@{@"agreeTerms":@YES}
                        completion:completion];

}


@end
