//
//  FRCuisine.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Realm/Realm.h>

@interface FRCuisine : RLMObject

@property NSInteger uid;
@property NSString *name;

@end
