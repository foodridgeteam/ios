//
//  FRMealInfoViewController.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRMealExtendedModel.h"

@interface FRMealInfoViewController : UITableViewController

@property (nonatomic, strong) FRMealExtendedModel *meal;

@end
