//
//  FRMealModel.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/7/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import Foundation;
@import CoreGraphics;

typedef NS_ENUM(NSInteger, FRMealFlag){
    FRMealFlagBreakfast = 1,
    FRMealFlagLunch = 2,
    FRMealFlagDinner = 3,
    FRMealFlagBakery = 4,
    FRMealFlagVegitarian =5,
    FRMealFlagEgg = 6,
    FRMealFlagGlutenFree = 7,
    FRMealFlagNuts = 8,
    FRMealFlagGarlic = 9,
    FRMealFlagGinger = 10,
};

typedef NS_ENUM(NSInteger, FRDeliveryType) {
    FRDeliveryTypePickupAndDelivery = 1,
    FRDeliveryTypePickup,
    FRDeliveryTypeDelivery
};

#import "FRViewModel.h"
#import "FRDeliveryInfoModel.h"

@protocol FRMealModel <NSObject>

@property(nonatomic, copy) NSNumber *uid;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *mealDescription;
@property(nonatomic, copy) NSArray *imageURLs;

@property(nonatomic, copy) NSNumber *price;

@property (nonatomic, copy) NSNumber *listed;
@property (nonatomic, copy) NSNumber *deliveryType;
@property(nonatomic, copy) NSNumber *discountsActive;
@property(nonatomic, copy) NSNumber *servingsLeft;
@property(nonatomic, copy) NSNumber *reviewsCount;

@property(nonatomic, copy) NSNumber *chefID;
@property(nonatomic, copy) NSString *chefTitle;
@property(nonatomic, copy) NSString *chefImageURL;
@property(nonatomic, copy) NSDate *chefLastVisit;
@property(nonatomic, copy) NSNumber *chefRating;

@property(nonatomic, strong) NSArray *cuisines;
@property(nonatomic, strong) NSArray *ingridients;

@property(nonatomic, copy) NSNumber *spiceRate;
@property(nonatomic, copy) NSDictionary *flags;

@property(nonatomic, copy) NSString *otherFlags;
@property(nonatomic, copy) NSNumber *deliveryPrice;
@property(nonatomic, copy) FRDeliveryInfoModel *deliveryInfo;

- (void)setFlag:(FRMealFlag)flag
          value:(BOOL)value;

- (BOOL)flagValue:(FRMealFlag)flag;



@end

@interface FRMealModel : FRViewModel<FRMealModel>



@end
