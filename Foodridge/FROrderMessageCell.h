//
//  FROrderMessageCell.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/29/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FROrderMessageModel.h"
#import "FROrderModel.h"

@interface FROrderMessageCell : UITableViewCell

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topLineConstraint;

- (void)setupWithOrder:(FROrderModel *)order message:(FROrderMessageModel *)message;

@end
