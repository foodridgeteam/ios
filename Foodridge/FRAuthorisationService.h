//
//  FRProfileService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FRNetworkService.h"

@class FRProfileModel;
static  NSString *FRCurrentProfileUpdatedNotificationName = @"FRCurrentProfileUpdatedNotification";

@interface FRAuthorisationService : NSObject



+ (FRAuthorisationService *)service;
+ (NSString *)accessToken;
+ (NSInteger)currentUID;

@property(nonatomic, assign) BOOL showTerms;
@property(nonatomic, assign, readonly) BOOL authorized;
@property(nonatomic, strong, readonly) NSString *authToken;
@property(nonatomic, strong, readonly) FRProfileModel *currentUser;

- (NSURLSessionTask *)authWithEmail:(NSString *)email
                           password:(NSString *)password
                         completion:(FRResponceBlock)completion;

- (NSURLSessionTask *)registerWithEmail:(NSString *)email
                               username:(NSString *)username
                               password:(NSString *)password
                             completion:(FRResponceBlock)completion;

- (NSURLSessionTask *)authWithFacebookToken:(NSString *)token
                                     userID:(NSString *)uid
                                 completion:(FRResponceBlock)completion;

- (NSURLSessionTask *)authWithGooglePlusToken:(NSString *)token
                                   completion:(FRResponceBlock)completion;


- (NSURLSessionTask *)getUserProfileWithCompletion:(FRResponceBlock)completion;

- (NSURLSessionTask *)saveUserProfile:(FRProfileModel *)profile 
                       withCompletion:(FRResponceBlock)completion;

- (NSURLSessionTask *)forgotPassword:(NSString *)email
                       withCompletion:(FRResponceBlock)completion;


- (FRProfileModel *)saveUserProfileWithDictionary:(NSDictionary *)profileDict;

- (void)saveProfileLocally;
- (void)aggreeWithTermsWithCompletion:(FRResponceBlock)completion;

- (void)logout;

@end
