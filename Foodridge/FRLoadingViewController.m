//
//  FRLoadingViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRLoadingViewController.h"
#import "FRLoginViewController.h"
#import "FRAuthorisationService.h"
#import "FRMainScreenService.h"
#import "FRChefService.h"
#import "FRStaticDataService.h"
#import "FRNotificationsService.h"
#import "FROrdersService.h"
@interface FRLoadingViewController ()

@end

@implementation FRLoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    FRAuthorisationService *service = [FRAuthorisationService service];
    
    if(service.authorized){
        [service getUserProfileWithCompletion:^(id object, id rawObject, NSError *error) {
            
            if(error.code == 403){
                [service logout];
                [[FRMainScreenService new] showLoginScreen];
                UIAlertController *alrt = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_SESSION_EXPIRED", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                [alrt addAction:cancel];
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alrt animated:YES completion:nil];
                
                
            }else{
                [[FRNotificationsService new] registerForNotifications];
                [[FRStaticDataService service] reloadCuisines];
                [[FRStaticDataService service] reloadIngridients];
                if ([[FRAuthorisationService service] authorized]) {
                    [[FRAuthorisationService service] setShowTerms:NO];
                    [[FRMainScreenService new] showMainScreen];
                }
            }
            
        }];
        
        [[FRChefService service] getChefProfileWithCompletion:^(id object, id rawObject, NSError *error) {
            
        }];
        
    }else{
        
        [[FRStaticDataService service] reloadCuisines];
        [[FRStaticDataService service] reloadIngridients];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[FRMainScreenService new] showLoginScreen];
        });
    }
}


@end
