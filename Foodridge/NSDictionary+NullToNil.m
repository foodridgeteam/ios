//
//  NSDictionary+NullToNil.m
//  MusketeerNew
//
//  Created by Brovko Roman on 11.11.14.
//  Copyright (c) 2014 Musketeer safety net LTD. All rights reserved.
//

#import "NSDictionary+NullToNil.h"

@implementation NSDictionary (NullToNil)

- (id)objectOrNilForKey:(id)aKey {
    id object = [self objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

- (id)objectOrNilForKey:(id)aKey classObject:(Class)classObject {
    id object =[self objectOrNilForKey:aKey];
    
    if (![object isKindOfClass:classObject]) {
        if(classObject == [NSNumber class] && [object respondsToSelector:@selector(integerValue)]){
            return @([object integerValue]);
        }
        return nil;
    }
    
    return object;
}

- (id)objectOrNilForKeypath:(NSString *)keyPath classObject:(Class)classObject{
    
    id object =[self valueForKeyPath:keyPath];
    if( [object isEqual:[NSNull null]]){
        return nil;
    }
    if (![object isKindOfClass:classObject]) {
        if(classObject == [NSNumber class] && [object respondsToSelector:@selector(integerValue)]){
            return @([object integerValue]);
        }
        return nil;
    }

    
    return object;

}

@end
