//
//  FRPanelViewController.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRPanelViewController : UITableViewController

- (void)showOrdersOnPlacedPage:(BOOL)placed;
- (void)showMenu;

@end
