//
//  FRChefCell.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <libextobjc/extobjc.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "FRChefCell.h"
#import "FRChefControl.h"
#import "UIImage+Color.h"
#import "UIButton+Localize.h"
#import "FRBuisinessInfoModel.h"
#import "UIColor+FoodridgeColors.h"
#import "FRCuisineModel.h"
#import "FRDateHelper.h"
#import "FRNetworkService+Chefs.h"
#import "FRChefsService.h"
#import "FRAuthorisationService.h"

@interface FRChefCell ()

@property (strong, nonatomic) IBOutlet UILabel *chefNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *lastVisitedLabel;
@property (strong, nonatomic) IBOutlet FRChefControl *chefRatingControl;
@property (strong, nonatomic) IBOutlet UILabel *cuisinesLabel;
@property (strong, nonatomic) IBOutlet UILabel *followersLabel;
@property (strong, nonatomic) IBOutlet UIButton *followButton;
@property (strong, nonatomic) IBOutlet UIImageView *chefImageView;
@property (strong, nonatomic) IBOutlet UIImageView *cuisinesImageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSNumber *chefId;
@property (strong, nonatomic) NSNumber *follow;
@property (strong, nonatomic) IBOutlet UIImageView *followersStarImageView;

@end

@implementation FRChefCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.followButton localizeWithOptions:THLocalizationOptionsUppercase];
    self.followButton.layer.cornerRadius = 3;
    self.followButton.layer.masksToBounds = YES;
    [self.followButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chefFollowingUpdate:) name:@"FRChefFollowingUpdate" object:nil];
    
    self.chefRatingControl.useWhiteChefHat = YES;
}

- (void) chefFollowingUpdate:(NSNotification*) chefId {
    
    if ([chefId.object integerValue] == [self.chefId integerValue]) {
        [[FRChefsService service] getChefWithId:chefId.object completion:^(id object, id rawObject, NSError *error) {
            if (!error) {
                [self setupCellWithChefModel:object];
                [self.activityIndicator stopAnimating];
            }
        }];
    }
}

- (void)setupCellWithChefModel:(FRChefProfileModel *)profile {


    self.chefId = profile.uid;
    self.follow = profile.follow;
    
    if ([self.follow boolValue]) {
        [self.followButton setTitle:NSLocalizedString(@"STR_UNFOLLOW", nil) forState:UIControlStateNormal];
        [self.followButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
        
    } else {
        
        [self.followButton setTitle:NSLocalizedString(@"STR_FOLLOW", nil) forState:UIControlStateNormal];
        [self.followButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    }
    
    

    self.chefNameLabel.text = profile.chefName;
    self.lastVisitedLabel.text = [NSString stringWithFormat:NSLocalizedString(@"STR_LAST_VISITED", nil),[FRDateHelper dateStringFromDate:profile.lastVisit]];
    self.cuisinesLabel.text = [FRCuisineModel getCuisisnesStringFromArray:profile.cuisines];

    NSString *followers = profile.followersCount.integerValue > 1 ? @"Followers" : @"Follower";
    self.followersLabel.text = profile.followersCount.integerValue ? [NSString stringWithFormat:@"%@ %@", profile.followersCount.stringValue, followers] : @"";
    self.followersStarImageView.hidden = profile.followersCount.integerValue ? NO : YES;
    
    [self.chefImageView setImageWithURL:[NSURL URLWithString:profile.avatarURL] placeholderImage:[UIImage imageNamed:@"logo_placeholder"]];
    self.chefRatingControl.value = profile.chefRating.integerValue;
    
    self.followButton.hidden = profile.uid.integerValue == [FRAuthorisationService currentUID] ? YES : NO;
    self.cuisinesImageView.hidden = !profile.cuisines.count ? YES : NO;
}

- (IBAction)followButtonAction:(id)sender {
    
    [self.activityIndicator startAnimating];
    [[FRChefsService service] followChefwithID:self.chefId follow:[self.follow boolValue] completion:nil];
}

-(void)prepareForReuse {
    [super prepareForReuse];
    [self.chefImageView cancelImageDownloadTask];
    [self.activityIndicator stopAnimating];
}

-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
