//
//  FRSlideMenuService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMainScreenService.h"
#import "SWHamburgerRevealViewController.h"
#import "FRRealmService.h"
#import "FRMealsService.h"
#import "FRMealInfoViewController.h"
#import "FRPanelViewController.h"
#import "FRAuthorisationService.h"

@implementation FRMainScreenService


- (void)showLoadingScreen{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Loading" bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    [[[UIApplication sharedApplication] keyWindow] setRootViewController:vc];
}


- (void)showMainScreen{
    
    UIViewController *root = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    if ([root isKindOfClass:[SWHamburgerRevealViewController class]]) {
        return;
    }
    
    UIStoryboard *panelstoryboard = [UIStoryboard storyboardWithName:@"Panel" bundle:nil];
    UIViewController *panel = [panelstoryboard instantiateInitialViewController];
    
    UIStoryboard *menuStoryboard = [UIStoryboard storyboardWithName:@"Menu" bundle:nil];
    UIViewController *menu = [menuStoryboard instantiateInitialViewController];

    SWHamburgerRevealViewController *reveal = [[SWHamburgerRevealViewController alloc] init];
    
    [reveal setRearViewRevealWidth:280];
    [reveal setFrontViewController:menu];
    [reveal setRearViewController:panel];
    [reveal setFrontViewShadowRadius:0.];

    [[[UIApplication sharedApplication] keyWindow] setRootViewController:reveal];
    [reveal.panGestureRecognizer setEnabled:YES];
    [reveal.tapGestureRecognizer setEnabled:YES];

    [[FRAuthorisationService service] setShowTerms:YES];
}

- (void)showLoginScreen{
    
    [[FRRealmService new] clear];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    [[[UIApplication sharedApplication] keyWindow] setRootViewController:vc];
}


- (void)showOrdersListAndFocusOnPlased:(BOOL)plased{
    UIViewController *root = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    if(![root isKindOfClass:[SWHamburgerRevealViewController class]]){
        return;
    }
    
    SWHamburgerRevealViewController *reveal = (SWHamburgerRevealViewController *)root;
    
    UINavigationController *rear = (UINavigationController *)reveal.rearViewController;
    FRPanelViewController *panel;
    if([rear isKindOfClass:[FRPanelViewController class]]){
        panel = (FRPanelViewController *)rear;
    }else if([rear isKindOfClass:[UINavigationController class]]){
        panel = [[rear viewControllers] firstObject];
    }else{
        return;
    }
    
    if([panel isKindOfClass:[FRPanelViewController class]]){
        [panel showOrdersOnPlacedPage:plased];
    }
}

- (void)showMealInfoScreenForMealWithID:(NSInteger)mealID {
    UIViewController *root = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    if(![root isKindOfClass:[SWHamburgerRevealViewController class]]){
        return;
    }
    SWHamburgerRevealViewController *reveal = (SWHamburgerRevealViewController *)root;
    
    UINavigationController *rear = (UINavigationController *)reveal.rearViewController;
    FRPanelViewController *panel;
    if([rear isKindOfClass:[FRPanelViewController class]]){
        panel = (FRPanelViewController *)rear;
    }else if([rear isKindOfClass:[UINavigationController class]]){
        panel = [[rear viewControllers] firstObject];
    }else{
        return;
    }
    if([panel isKindOfClass:[FRPanelViewController class]]){
        [[FRMealsService service] getMealWithID:mealID completion:^(id object, id rawObject, NSError *error) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Menu" bundle:nil];
            UINavigationController *mealsNavigationController = [sb instantiateInitialViewController];
            
            FRMealInfoViewController *vc = [sb instantiateViewControllerWithIdentifier:@"FRMealInfo"];
            vc.meal = (FRMealExtendedModel *)object;
            [mealsNavigationController pushViewController:vc animated:YES];
            
            [reveal setFrontViewController:mealsNavigationController animated:NO];
        }];
    }
}


- (void)showDishesScreen {
    UIViewController *root = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    if(![root isKindOfClass:[SWHamburgerRevealViewController class]]){
        return;
    }
    
    SWHamburgerRevealViewController *reveal = (SWHamburgerRevealViewController *)root;
    
    UINavigationController *rear = (UINavigationController *)reveal.rearViewController;
    FRPanelViewController *panel;
    if([rear isKindOfClass:[FRPanelViewController class]]){
        panel = (FRPanelViewController *)rear;
    }else if([rear isKindOfClass:[UINavigationController class]]){
        panel = [[rear viewControllers] firstObject];
    }else{
        return;
    }

    if([panel isKindOfClass:[FRPanelViewController class]]){
        [panel showMenu];
    }
}


@end
