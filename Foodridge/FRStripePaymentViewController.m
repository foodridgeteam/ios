//
//  FRStripePaymentViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/26/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//
#import <libextobjc/extobjc.h>
#import <Stripe/Stripe.h>
#import "FRStripePaymentViewController.h"
#import "UIColor+FoodridgeColors.h"
#import "UIImage+Color.h"
#import "FROrdersService.h"
#import "FRMainScreenService.h"


@interface FRStripePaymentViewController ()<STPPaymentCardTextFieldDelegate>
@property (strong, nonatomic) IBOutlet STPPaymentCardTextField *cardField;
@property (strong, nonatomic) IBOutlet UIButton *payButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation FRStripePaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.payButton setBackgroundImage:[UIImage imageWithColor:[UIColor stripeButtonColor]] forState:UIControlStateNormal];
    [self.payButton setTitle:[NSString stringWithFormat:@"Pay AUD $%.2lf",self.totalPrice/100.] forState:UIControlStateNormal];
    self.payButton.enabled = NO;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.cardField becomeFirstResponder];
}

-(void)paymentCardTextFieldDidChange:(STPPaymentCardTextField *)textField{
    self.payButton.enabled = textField.isValid;
}

- (IBAction)payAction:(id)sender {
    self.view.userInteractionEnabled = NO;
    [self.activityIndicator startAnimating];

    @weakify(self);
    [[STPAPIClient sharedClient] createTokenWithCard:self.cardField.cardParams
     completion:^(STPToken *token, NSError *error) {
         
         @strongify(self);
         
         if (error) {
             self.view.userInteractionEnabled = YES;
             [self.activityIndicator stopAnimating];
             UIAlertController *cntr = [UIAlertController alertControllerWithTitle:nil message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
             
             [cntr addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}]];
             
             [self presentViewController:cntr animated:YES completion:nil];
             
             
         } else {
             [self createOrderWithToken:token];
         }
     }];
}

-(void)createOrderWithToken:(STPToken *)token{
    
    [[FROrdersService service] createOrder:self.mealID.integerValue
                                  quantity:self.portions
                               stripeToken:token.tokenId
                                  checkSum:self.totalPrice
                              deliveryType:self.type
                                completion:^(id object, id rawObject, NSError *error) {
                                    self.view.userInteractionEnabled = YES;
                                    [self.activityIndicator stopAnimating];

                                    if(error){
                                        UIAlertController *cntr = [UIAlertController alertControllerWithTitle:nil message:error.domain preferredStyle:UIAlertControllerStyleAlert];
                                        [cntr addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}]];
                                        [self presentViewController:cntr animated:YES completion:nil];

                                    }else{
                                        [[FRMainScreenService new] showOrdersListAndFocusOnPlased:YES];
                                        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                    }
                                    
                                }];
}

@end
