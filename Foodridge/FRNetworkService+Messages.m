//
//  FRNetworkService+Messages.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService+Messages.h"

@implementation FRNetworkService (Messages)

+ (NSURLSessionTask *)getMessagesForOrderID:(NSInteger)orderID lastID:(NSInteger)lastID completion:(FRResponceBlock)completion {
    NSMutableDictionary *params = [@{@"orderId" : @(orderID),
                                     @"lastId" : @(lastID)} mutableCopy];
    return [self requestWithMethod:@"GET" requestAddress:@"messages" parameters:params completion:completion];
}

+ (NSURLSessionTask *)leaveMessageForOrderWithID:(NSInteger)orderID messageText:(NSString *)messageText completion:(FRResponceBlock)completion {
    NSMutableDictionary *params = [@{@"orderId" : @(orderID),
                                     @"text" : messageText} mutableCopy];
    
    return [self requestWithMethod:@"POST" requestAddress:@"messages" parameters:params completion:completion];
}

@end
