//
//  FRCommentsService.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRCommentsService.h"
#import "FRCommentsModel.h"

@implementation FRCommentsService

+ (FRCommentsService *)service{
    static FRCommentsService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [FRCommentsService new];
    });
    return service;
}

- (NSURLSessionTask *)getCommentsForMealWithID:(NSInteger)mealID completion:(FRResponceBlock)completion {
    return [FRNetworkService getCommentsForMealWithID:mealID completion:^(id object, id rawObject, NSError *error) {
        
        NSArray *comments;
        
        if (!error) {
            if([rawObject isKindOfClass:[NSArray class]]){
                comments = [FRCommentsModel modelsFromArray:rawObject];
                completion(comments,rawObject,error);
            }
        }
        else {
            completion(nil, nil, error);
        }
    }];
}


- (NSURLSessionTask *)leaveCommentForOrderWithID:(NSInteger)orderID commentText:(NSString *)commentText rating:(NSInteger)rating completion:(FRResponceBlock)completion {
    return [FRNetworkService leaveCommentForOrderWithID:orderID commentText:commentText rating:rating completion:^(id object, id rawObject, NSError *error) {
       
        if (!error) {
            FRCommentsModel *comment = [FRCommentsModel modelWitDictionary:rawObject];
            completion(comment, rawObject, error);
        }
        else {
            completion(nil, nil, error);
        }
    }];
}


@end
