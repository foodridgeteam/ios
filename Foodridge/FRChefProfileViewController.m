//
//  FRChefProfileViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <libextobjc/extobjc.h>
#import "FRChefProfileViewController.h"
#import "UIFont+FoodridgeFonts.h"
#import "UIColor+FoodridgeColors.h"
#import "UIImage+Color.h"
#import "FRDeliveryInfoViewController.h"
#import "FRBusinessDetailsViewController.h"
#import "FRDeliveryInfoModel.h"
#import "FRBuisinessInfoModel.h"
#import "FRChefProfileModel.h"
#import "FRStaticDataService.h"
#import "FRWorkHoursEditingView.h"
#import "FRWeekScheduleModel.h"
#import "FRAppearanceManager.h"
#import "FRStripeService.h"
#import "FRWeekScheduleModel.h"
#import "FRChefService.h"
#import "FRCuisineModel.h"
#import "FRProgressHUD.h"

@import SafariServices;
@import KSTokenView;

@interface FRChefProfileViewController ()<KSTokenViewDelegate, UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *pickupInfoLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *saveActivityIndicator;

@property (strong, nonatomic) IBOutlet KSTokenView *cuisineTokenView;
@property (strong, nonatomic) IBOutlet UITextView *bioTextView;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;

@property (strong, nonatomic) IBOutlet UILabel *businessDetailsTitlelabel;
@property (strong, nonatomic) IBOutlet UILabel *businessIDLabel;
@property (strong, nonatomic) IBOutlet UIButton *phoneButton;
@property (strong, nonatomic) IBOutlet UIButton *webSiteButton;
@property (strong, nonatomic) IBOutlet UIView *outerShadowView;

@property (strong, nonatomic) FRChefProfileModel *model;
@property (strong, nonatomic) FRChefProfileModel *unsavedChanges;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tokenBottomSpace;

@property (strong, nonatomic) UITextField *fakeTextField;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *workingHoursLabel;

//payment
@property (strong, nonatomic) IBOutlet UILabel *stripeDisclaimerLabel;
@property (strong, nonatomic) IBOutlet UIButton *stripeButton;

@property (weak, nonatomic) IBOutlet UILabel *tokenViewPlaceholder;


@property (copy, nonatomic) void (^buisnessInfoSaveBlock)(void);

@end

@implementation FRChefProfileViewController

-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self setupModel];
    [self setupUI];
    [self setupBusinessDetails];
    [self setupWorkingHours];
    
    
    self.fakeTextField = [[UITextField alloc] initWithFrame:CGRectMake(-100, 0, 0, 0)];
    [self.view addSubview:self.fakeTextField];
    FRWorkHoursEditingView *editingView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FRWorkHoursEditingView class]) owner:self options:nil] objectAtIndex:0];
    [editingView setAutoresizingMask:UIViewAutoresizingNone];
    [self.fakeTextField setInputView:editingView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupModel) name:FRProfileUpdatedNotificationName object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"stripeDidRecive" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        
        [[FRChefService service] updateChefProfile:self.model WithCompletion:^(id object, id rawObject, NSError *error) {}];
        
    }];
    
    [self buttonColorChecker];
}

- (void)setupModel{
    if(!self.unsavedChanges){
        self.unsavedChanges = [FRChefProfileModel new];
    }
    self.model = [[FRChefService service] currentChef];

}

- (void)setModel:(FRChefProfileModel *)model{
    
    _model = model;
    
    self.pickupInfoLabel.attributedText = [self.model.pickupInfo stringRepresemtation];
    self.bioTextView.text = self.model.bio;
    self.cuisineTokenView.delegate = self;
    [self.cuisineTokenView deleteAllTokens];
    for(FRCuisineModel *cuisine in self.model.cuisines){
        [[self.cuisineTokenView addTokenWithTitle:cuisine.name tokenObject:cuisine] setTokenBackgroundColor:[UIColor aquaColor]];
    }
    [self setupBusinessDetails];
    [self setupWorkingHours];
    [self setupPaymentInfo];
}

- (IBAction)editWorkHours:(UIButton *)sender {
    
    //FRWorkHoursEditingView *editingView = (FRWorkHoursEditingView *)self.fakeTextField.inputView;
    
    // Create and setup new editingView, because if we assign editing view from fakeTextField inputView, it cause crash on iOS 9.
    FRWorkHoursEditingView *editingView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FRWorkHoursEditingView class]) owner:self options:nil] objectAtIndex:0];
    [editingView setAutoresizingMask:UIViewAutoresizingNone];
    [self.fakeTextField setInputView:editingView];
    
    NSInteger index = sender.tag;
    editingView.dayIndex = index;
    
    FRWeekScheduleModel *model = self.unsavedChanges.weekSchedule?:self.model.weekSchedule;
    
    [editingView setupWithOpenDate:[model openDateForDayAtIndex:index]
                         closeDate:[model closeDateForDayAtIndex:index]];
    
    @weakify(self);
    [editingView setOnDone:^(BOOL canceled, NSDate *openDate, NSDate *closeDate) {
        @strongify(self);
            if(!self){
                [self buttonColorChecker];
                return;
            }
        
        if(!canceled){
            if(!self.unsavedChanges.weekSchedule){
                self.unsavedChanges.weekSchedule = self.model.weekSchedule;
                [self buttonColorChecker];
            }
            if(!self.unsavedChanges.weekSchedule){
                self.unsavedChanges.weekSchedule = [FRWeekScheduleModel new];
                [self buttonColorChecker];
            }

            [self.unsavedChanges.weekSchedule setOpenDate:openDate forDayAtIndex:index];
            [self.unsavedChanges.weekSchedule setCloseDate:closeDate forDayAtIndex:index];
            [self setupWorkingHours];
            [self buttonColorChecker];
        }
        [self.fakeTextField resignFirstResponder];
    }];
    
    [self.fakeTextField becomeFirstResponder];
}

-(BOOL)validateUnsavedChanges{
    if(![self.unsavedChanges.businessInfo isValid]){
        UIAlertController *cntr = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_BUSINESS_INFO_INVALID", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        [cntr addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self performSegueWithIdentifier:@"ShowBusinessDetails" sender:nil];
        }]];
        [self presentViewController:cntr animated:YES completion:nil];

        return NO;
    }
    return YES;
}

- (IBAction)saveAction:(id)sender {
    
    [self.view endEditing:YES];
    if(!self.model && ![self validateUnsavedChanges]){
        return;
    }
    
    if(self.unsavedChanges.dictionaryRepresentation){
        
        @weakify(self)
        [[FRChefService service] updateChefProfile:self.unsavedChanges WithCompletion:^(id object, id rawObject, NSError *error) {
            @strongify(self)
            
            if(!error) {
                self.model = object;
                self.unsavedChanges = [FRChefProfileModel new];
                [self.saveActivityIndicator stopAnimating];
                [self.saveActivityIndicator setHidden:YES];
                [self buttonColorChecker];
                
                [FRProgressHUD showMessageHudWithText:@"Changes saved" inView:self.navigationController.view delay:2.5];
            }
        }];
    }
    [self buttonColorChecker];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"ShowDeliveryInfo"]){
        FRDeliveryInfoViewController *cntr = segue.destinationViewController;
        cntr.navigationItem.title = NSLocalizedString(@"STR_PICKUP_ADDRESS", nil);
        cntr.model = self.unsavedChanges.pickupInfo?:self.model.pickupInfo;
        @weakify(self);
        [cntr setOnSave:^(FRDeliveryInfoModel *model) {
            @strongify(self);
            self.unsavedChanges.pickupInfo = model;
            self.pickupInfoLabel.attributedText = [model stringRepresemtation];
            [self buttonColorChecker];
        }];
    }else if([segue.identifier isEqualToString:@"ShowBusinessDetails"]){
        FRBusinessDetailsViewController *cntr = segue.destinationViewController;
        cntr.navigationItem.title = NSLocalizedString(@"STR_BUSINESS_DETAILS", nil);
        cntr.model = self.unsavedChanges.businessInfo?:self.model.businessInfo;
        @weakify(self);
        [cntr setOnSave:^(FRBuisinessInfoModel *model) {
            @strongify(self);
            
            
            self.unsavedChanges.businessInfo = model;
            
            // for conecting stripe. we cant get stripe id before saving business info
            [[FRChefService service] updateChefProfile:self.unsavedChanges WithCompletion:^(id object, id rawObject, NSError *error) {
            
                if(self.buisnessInfoSaveBlock){
                    self.buisnessInfoSaveBlock();
                    
                    self.buisnessInfoSaveBlock = nil;
                }
                
            }];
            
            
            [self setupBusinessDetails];
            [self buttonColorChecker];
        }];
    }
}
- (IBAction)callAction:(id)sender {
    FRBuisinessInfoModel *model = self.unsavedChanges.businessInfo?:self.model.businessInfo;
    
    if([model.phone length]){
        NSString *tel = [@"tel:" stringByAppendingString:model.phone];
        NSURL *url = [NSURL URLWithString:tel];
        if([[UIApplication sharedApplication] canOpenURL:url]){
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}

- (IBAction)openURLAction:(id)sender {
    FRBuisinessInfoModel *model = self.unsavedChanges.businessInfo?:self.model.businessInfo;
    
    if([model.url length]){
        NSString *urlString = model.url;
        if(![urlString hasPrefix:@"http"]){
            urlString =[NSString stringWithFormat:@"http://%@",urlString];
        }
        NSURL *url = [NSURL URLWithString:urlString];
        if([[UIApplication sharedApplication] canOpenURL:url]){
            if([SFSafariViewController class]){
                SFSafariViewController *cntr =[[SFSafariViewController alloc] initWithURL:url];
                [self presentViewController:cntr animated:YES completion:nil];
            }else{
                [[UIApplication sharedApplication] openURL:url];
            }
        }
    }
}

-(IBAction)shareButtonAction:(id)sender {
 
    NSURL *androidAppUrl = [NSURL URLWithString:@"https://goo.gl/UhhvnR"];
    NSURL *foodridgeSiteUrl = [NSURL URLWithString:@"http://foodridge.com/"];
    
    NSString* subject = [NSString stringWithFormat:@"Hi! Now my dishes available in Foodridge App!"];
    
    NSString *infoText = [NSString stringWithFormat:@"Hi! Now my dishes available in Foodridge App! \nCheckout their site: "];
    
    NSString *infoText2 = [NSString stringWithFormat:@"and Android application: "];
    
    NSArray *objectsToShare = @[infoText,foodridgeSiteUrl,infoText2,androidAppUrl];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    [activityVC setValue:subject forKey:@"subject"];
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

-(void) buttonColorChecker {
    
    if ([self.unsavedChanges dictionaryRepresentation] == nil) {
        
        [self.saveButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
    } else {
        
        [self.saveButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    }
}

-(void)setupBusinessDetails{
    
    FRBuisinessInfoModel *model = self.unsavedChanges.businessInfo?:self.model.businessInfo;
    
    self.businessDetailsTitlelabel.text = model.title;
    self.businessIDLabel.text =  model.registrationID;
    [self.phoneButton setTitle: model.phone forState:UIControlStateNormal];
    [self.webSiteButton setTitle: model.url forState:UIControlStateNormal];
    [self buttonColorChecker];
}

-(void)setupWorkingHours{
    
    FRWeekScheduleModel *model = self.unsavedChanges.weekSchedule?:self.model.weekSchedule;
    
    for(UILabel *l in self.workingHoursLabel){
        
        if ([model openDateForDayAtIndex:l.tag] !=nil ){
            [l setAttributedText:[model intervalStringForDayAtIndex:l.tag]];
            l.textColor = [UIColor blackColor];
        } else {
            l.font = [UIFont openSansBoldFontWithSize:14];
            l.textColor = [UIColor redColor];
            l.text = @"Closed";
        }
        
    }
    
    [self buttonColorChecker];
}

//tokens
- (void)setupUI{
    
    [FRAppearanceManager setupKSTokenView:self.cuisineTokenView];
    
    UIRefreshControl *refresh = [UIRefreshControl new];
    [refresh addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [refresh setTintColor:[UIColor aquaColor]];
    [refresh setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"STR_LOADING", nil)]];
    [self.scrollView addSubview:refresh];
    [self.scrollView sendSubviewToBack:refresh];
    self.refreshControl = refresh;
    
    [self.saveButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    
    if (self.cuisineTokenView.tokens.count > 0) {
        self.tokenViewPlaceholder.hidden = YES;
    }
}

- (void)textViewDidChange:(UITextView *)textView{
    self.unsavedChanges.bio = textView.text;
    [self buttonColorChecker];
}

- (void)tokenView:(KSTokenView *)token performSearchWithString:(NSString *)string completion:(void (^)(NSArray * _Nonnull))completion{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSArray *arr = [[FRStaticDataService service] getCuisinesWithSearchText:string];
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(arr);
        });
    });
}

- (void)tokenViewDidBeginEditing:(KSTokenView *)tokenView{
    
    
    self.tokenViewPlaceholder.hidden = YES;
    self.tokenBottomSpace.constant = 200.;
    [self.view layoutIfNeeded];
}

-(void)tokenViewDidEndEditing:(KSTokenView *)tokenView{
    
    if (tokenView.tokens.count == 0) {
        self.tokenViewPlaceholder.hidden = NO;
    }
    
    self.outerShadowView.frame = CGRectMake(self.outerShadowView.frame.origin.x, self.outerShadowView.frame.origin.y, self.outerShadowView.frame.size.width, 110); //iOS 8 fix layout
    self.tokenBottomSpace.constant = 0.;
    self.cuisineTokenView.text = @"";
    [self buttonColorChecker];
    [self.view layoutIfNeeded];
}

- (NSString *)tokenView:(KSTokenView *)token displayTitleForObject:(FRCuisineModel *)object{
    
    return [object name];
}

- (BOOL)tokenView:(KSTokenView *)tokenView shouldAddToken:(KSToken *)token{
    if(!token.object){
        return NO;
    }
    token.deleteIcon = YES;
    token.tokenBackgroundColor = [UIColor aquaColor];
    return YES;
}

-(void)saveTokens{
    NSArray *tokens = [self.cuisineTokenView.tokens valueForKey:@"object"];
    if(!tokens){
        tokens = @[];
    }
    self.unsavedChanges.cuisines = tokens;
    [self buttonColorChecker];
    
    NSLog(@"%@",tokens);
}

- (void)tokenView:(KSTokenView *)tokenView didAddToken:(KSToken *)token{
    token.deleteIcon = YES;

    [self saveTokens];
}

- (void)tokenView:(KSTokenView *)tokenView didDeleteToken:(KSToken *)token{
    [self saveTokens];
}

- (IBAction)connectStripe:(id)sender {
    
    if (self.model.stripeId) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_STRIPE_DISCONNECT", nil)
                                                                                 message:NSLocalizedString(@"STR_STRIPE_DISCONNECT_MESSAGE", nil)
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                               style:UIAlertActionStyleCancel
                                                             handler:nil];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action)
                                   {
                                       [[FRStripeService new] deleteStripeAccountWithCompletion:^(id object, id rawObject, NSError *error) {
                                           if (!error) {
                                               self.model = [FRChefProfileModel modelWitDictionary:rawObject];
                                           }
                                           else {
                                               UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Stripe disconnect error" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                                               [alertView show];
                                           }
                                       }];
                                   }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        if(!self.model && ![self validateUnsavedChanges]){
            
            [self setBuisnessInfoSaveBlock:^{
                [[FRStripeService new] connectStripe];
            }];
            
            return;
        }
        
        if(![[FRStripeService new] connectStripe]){
            NSLog(@"STRIPE CONNECTION ERROR");
        }
    }
  
    [self buttonColorChecker];
}

- (void)setupPaymentInfo{
    if(self.model.stripeId){
        self.stripeDisclaimerLabel.text = NSLocalizedString(@"STR_STRIPE_DISCLAIMER_CONNECTED", nil);
        [self.stripeButton setTitle:NSLocalizedString(@"STR_STRIPE_CONNECTED", nil) forState:UIControlStateNormal];
        self.stripeButton.layer.borderWidth = 2;
        self.stripeButton.layer.borderColor = [UIColor stripeConnectButtonColor].CGColor;
        [self.stripeButton setTitleColor:[UIColor stripeConnectButtonColor] forState:UIControlStateNormal];
        self.stripeButton.backgroundColor = [UIColor clearColor];
        
    }else{
        self.stripeDisclaimerLabel.text = NSLocalizedString(@"STR_STRIPE_DISCLAIMER", nil);
        [self.stripeButton setTitle:NSLocalizedString(@"STR_STRIPE_CONNECT", nil) forState:UIControlStateNormal];
        self.stripeButton.backgroundColor = [UIColor stripeConnectButtonColor];
        [self.stripeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    self.stripeButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self buttonColorChecker];
}

- (void)refresh{
    @weakify(self)
    [[FRChefService service] getChefProfileWithCompletion:^(id object, id rawObject, NSError *error) {
        @strongify(self)
        [self.refreshControl endRefreshing];
    }];
   [self buttonColorChecker];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:FRProfileUpdatedNotificationName
                                                  object:nil];

}

@end
