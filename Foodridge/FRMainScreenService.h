//
//  FRSlideMenuService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import UIKit;

@interface FRMainScreenService : NSObject


- (void)showMainScreen;
- (void)showLoginScreen;
- (void)showLoadingScreen;
- (void)showDishesScreen;

- (void)showOrdersListAndFocusOnPlased:(BOOL)plased;//if NO - show received orders
- (void)showMealInfoScreenForMealWithID:(NSInteger)mealID;

/*- (void)forceUserAggreeWithTerms; */

@end
