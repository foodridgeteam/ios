//
//  FRChefService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRChefService.h"
#import "FRNetworkService+Profile.h"
#import "FRChefProfileModel.h"

@implementation FRChefService



+ (FRChefService *)service{
    
    static FRChefService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [FRChefService new];
    });
    return service;
}

- (NSURLSessionTask *)getChefProfileWithCompletion:(FRResponceBlock)completion{
    
    return [FRNetworkService myChefWithCompletion:^(id object, NSDictionary *rawObject, NSError *error) {
        
        FRChefProfileModel *model = [FRChefProfileModel modelWitDictionary:rawObject];
        _currentChef = model;
        [[NSNotificationCenter defaultCenter] postNotificationName:FRProfileUpdatedNotificationName object:nil];
        if(completion){
            completion(model, rawObject, error);
        }
        
    }];
}

- (NSURLSessionTask *)updateChefProfile:(FRChefProfileModel *)profile WithCompletion:(FRResponceBlock)completion{
    
    return [FRNetworkService updateChefProfile:profile completion:^(id object, id rawObject, NSError *error) {
        
        FRChefProfileModel *model = [FRChefProfileModel modelWitDictionary:rawObject];
        
        _currentChef = model;
        [[NSNotificationCenter defaultCenter] postNotificationName:FRProfileUpdatedNotificationName object:nil];
        if(completion){
            completion(model, rawObject, error);
        }
        
    }];
}

- (NSURLSessionTask *)saveMyMeal:(FRViewModel<FRMealModel> *)mealModel
                  withCompletion:(FRResponceBlock)completion{
    
    return [FRNetworkService saveMyMeal:mealModel completion:^(id object, id rawObject, NSError *error) {
        
        FRMealExtendedModel *model = [FRMealExtendedModel modelWitDictionary:rawObject];
        
        
        completion(model, rawObject, error);
        
    }];

    
}


+ (void)clearData{
    
}


@end
