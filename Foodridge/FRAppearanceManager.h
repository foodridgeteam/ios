//
//  FRAppearanceManager.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KSTokenView;
@interface FRAppearanceManager : NSObject

+ (void)setupAppearance;

+ (void)setupKSTokenView:(KSTokenView *)tokenView;

+ (void)setupStandardSegmentControl:(UISegmentedControl *)control;


@end
