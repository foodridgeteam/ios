//
//  FRRateControl.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRRateControl : UIControl

@property(nonatomic, assign) IBInspectable NSInteger value;

@end
