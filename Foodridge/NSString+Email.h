//
//  NSString+Email.h
//  NFG
//
//  Created by Vitaly Evtushenko on 19.11.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Email)

- (BOOL)isValidEmail;

@end
