//
//  FRNetworkService+Orders.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/25/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import CoreGraphics;
#import "FRNetworkService.h"


@interface FRNetworkService (Orders)

+ (NSURLSessionTask *)createOrder:(NSInteger)mealID
                         quantity:(NSInteger)count
                      stripeToken:(NSString *)token
                     deliveryType:(NSInteger)type
                         checkSum:(long)sum
                       completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)getMyOrders:(BOOL)active
                       completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)setOrderStatus:(NSInteger)orderStatus
                      forOrderWithID:(NSInteger)orderID
                          completion:(FRResponceBlock)completion;

@end
