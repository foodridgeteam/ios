//
//  FROrderMessage.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/28/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Realm/Realm.h>

@interface FROrderMessage : RLMObject

@property NSInteger uid;
@property NSString *text;
@property NSInteger type;
@property NSInteger senderID;

@end
