//
//  FRMenuService.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMenuService.h"
#import "FRNetworkService+Menu.h"
#import "FRMealExtendedModel.h"

@implementation FRMenuService

+ (FRMenuService *)service{
    
    static FRMenuService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [FRMenuService new];
    });
    return service;
}


- (NSURLSessionTask *)updateMyMealWithModel:(FRViewModel<FRMealModel> *)model completion:(FRResponceBlock)completion {
    
    return [FRNetworkService updateMyMealWithModel:model completion:^(id object, id rawObject, NSError *error) {
        FRMealExtendedModel *model = [FRMealExtendedModel modelWitDictionary:rawObject];
        completion(model, rawObject, error);
    }];
    
}



@end
