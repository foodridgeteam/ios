//
//  FRDeliveryInfoModel.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/8/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//
@import UIKit;

#import "UIFont+FoodridgeFonts.h"
#import "FRDeliveryInfoModel.h"

@implementation FRDeliveryInfoModel

- (NSDictionary *)dictionaryRepresentation{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    if(self.country){
        params[@"country"] = self.country;
    }
    if(self.city){
        params[@"city"] = self.city;
    }
    if(self.street){
        params[@"street"] = self.street;
    }
    if(self.more){
        params[@"more"] = self.more;
    }
    if(params.allKeys.count == 0){
        return nil;
    }
    return params.copy;
    
}

- (NSAttributedString *)stringRepresemtation{
    
    NSMutableArray *arr = [NSMutableArray new];
    if(self.street){
        [arr addObject:self.street];
    }
    if(self.city){
        [arr addObject:self.city];
    }
    if(self.country){
        [arr addObject:self.country];
    }
    
    NSString *str = [arr componentsJoinedByString:@", \n"];
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName: [UIFont openSansFontWithSize:14]}];
    [attr setAttributes:@{NSFontAttributeName: [UIFont openSansBoldFontWithSize:14]} range:NSMakeRange(0, self.street.length)];
    return attr.copy;

}

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary{
    
    FRDeliveryInfoModel *deliveryInfo = [FRDeliveryInfoModel new];
    deliveryInfo.street =   [dictionary objectOrNilForKey:@"street" classObject:[NSString class]];
    deliveryInfo.city =     [dictionary objectOrNilForKey:@"city" classObject:[NSString class]];
    deliveryInfo.country =  [dictionary objectOrNilForKey:@"country" classObject:[NSString class]];
    deliveryInfo.more =     [dictionary objectOrNilForKey:@"more" classObject:[NSString class]];
    
    return deliveryInfo;
}

@end
