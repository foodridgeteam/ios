//
//  FROrdersService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrdersService.h"
#import "FRNetworkService+Orders.h"
#import "FRRealmService.h"
#import "FROrder.h"
#import "FRAuthorisationService.h"
#import "FRMessagesService.h"
#import "FROrderMessageModel.h"

@interface FROrdersService()

@property(nonatomic, strong) NSArray *plasedOrders;
@property(nonatomic, strong) NSArray *receivedOrders;
@property(nonatomic, strong) NSArray *completedOrders;



@end

@implementation FROrdersService

+ (FROrdersService *)service{
    
    
    static FROrdersService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [FROrdersService new];
    });
    return service;
}

- (void)processOrderNotification:(NSDictionary *)data{
    [[NSNotificationCenter defaultCenter] postNotificationName:FROrderNewMessageNotificationName object:data];
}


- (NSURLSessionTask *)createOrder:(NSInteger)mealID
                         quantity:(NSInteger)count
                      stripeToken:(NSString *)token
                         checkSum:(long)sum
                     deliveryType:(FRDeliveryType)type
                       completion:(FRResponceBlock)completion{
    
    return [FRNetworkService createOrder:mealID
                                quantity:count
                             stripeToken:token
                            deliveryType:type
                                checkSum:sum
                              completion:^(id object, id rawObject, NSError *error) {
                                  FROrderModel *model;
                                  if([rawObject isKindOfClass:[NSDictionary class]]){
                                      model = [FROrderModel modelWitDictionary:rawObject];
                                      
                                  }else{
                                      
                                  }
                                  if(model){
                                      [self saveOrders:@[model]];
                                  }
                                  completion(model,rawObject,error);
                              }];
}

- (NSURLSessionTask *)refreshOrdersListWithCompletion:(FRResponceBlock)completion{
    return  [FRNetworkService getMyOrders:YES completion:^(id object, id rawObject, NSError *error) {
        NSArray *orders;
        if([rawObject isKindOfClass:[NSArray class]]){
            orders = [FROrderModel modelsFromArray:rawObject];
        }else{
            
        }
        if(orders.count > 0){
            [self saveOrders:@[orders]];
        }
        
        
        NSMutableArray *ordersWithMessages = [NSMutableArray new];
        for (int i = 0; i < [orders count]; i++) {
            FROrderModel *order = orders[i];
            [[FRMessagesService service] getMessagesForOrderID:order.uid.integerValue lastID:0 completion:^(id object, id rawObject, NSError *error) {
                NSArray *messages = [FROrderMessageModel modelsFromArray:rawObject];
                order.messages = messages;
                [ordersWithMessages addObject:order];
                
                if (ordersWithMessages.count == orders.count) {
                    
                    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"uid" ascending:NO];
                    NSArray *sortedArray = [ordersWithMessages sortedArrayUsingDescriptors:@[sortDescriptor]];
                    
                    self.plasedOrders = [sortedArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"customer.uid == %ld && status !=%ld",[FRAuthorisationService currentUID], FROrderStatusCompleted]];
                    self.receivedOrders = [sortedArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"customer.uid != %ld && status !=%ld",[FRAuthorisationService currentUID], FROrderStatusCompleted]];
                    [[NSNotificationCenter defaultCenter] postNotificationName:FROrderUpdatedNotificationName object:nil];
                    completion(orders,rawObject,error);
                }
            }];
        }
        
        if (!orders.count) {
            self.plasedOrders = nil;
            self.receivedOrders = nil;
            completion(orders,rawObject,error);
        }
    }];
}

- (NSURLSessionTask *)refreshCompletedOrdersWithCompletion:(FRResponceBlock)completion{

        return  [FRNetworkService getMyOrders:NO completion:^(id object, id rawObject, NSError *error) {
            NSArray *orders;
            if([rawObject isKindOfClass:[NSArray class]]){
                
                orders = [FROrderModel modelsFromArray:rawObject];
                self.completedOrders = orders;
                [[NSNotificationCenter defaultCenter] postNotificationName:FROrderUpdatedNotificationName object:nil];
                
            }else{
                
            }
            if(orders.count > 0){
                [self saveOrders:@[orders]];
            }
            completion(orders,rawObject,error);
            
        }];
}

- (NSURLSessionTask *)setOrderStatus:(NSInteger)orderStatus forOrderWithID:(NSInteger)orderID completion:(FRResponceBlock)completion {
    return [FRNetworkService setOrderStatus:orderStatus forOrderWithID:orderID completion:^(id object, id rawObject, NSError *error) {
        if (!error) {
            FROrderModel *model = [FROrderModel modelWitDictionary:rawObject];
            completion(model, rawObject, error);
        }
        else {
            completion(nil, nil, error);
        }
    }];
}

- (NSArray *)ordersWithFilter:(FROrdersFilter)filter{
    
    switch (filter) {
            
        case FROrdersFilterPlaced:
            
            return [self plasedOrders];
            
            break;
            
        case FROrdersFilterReceived:
            
            return [self receivedOrders];
            
            break;
            
        case  FROrdersFilterCompleted:
            
            return  [self completedOrders];
            
            break;
            
        default:
            break;
    }
}

- (void)saveOrders:(NSArray *)orders{
    
//    FRRealmService *service = [FRRealmService new];
//    NSMutableArray *arr = [NSMutableArray new];
//    
//    
//
//    for(FROrderModel *model in orders){
//        FROrder *order = [FROrder new];
//        
//    }
//    
//    [service writeObjects:arr withCompletion:^{
//        
//    }];

    
}

@end
