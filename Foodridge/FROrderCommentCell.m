//
//  FROrderCommentCell.m
//  Foodridge
//
//  Created by Victor Miroshnichenko on 5/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrderCommentCell.h"
#import "FRChefControl.h"


@interface FROrderCommentCell ()

@property (strong, nonatomic) IBOutlet FRChefControl *chefControl;
@property (strong, nonatomic) IBOutlet UITextView *commentTextView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *outerShadowViewBottomConstraint;

@end

@implementation FROrderCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setupCellWithCommentModel:(FRCommentsModel *)model {
    
    self.chefControl.value = model.rating.integerValue;
    
    self.outerShadowViewBottomConstraint.constant = model.message.length == 0 ? -68 : 8;
    
    self.commentTextView.text = model.message;
}


@end
