//
//  FRAboutDishCell.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRAboutDishCell.h"
#import "FRCuisineModel.h"

@interface FRAboutDishCell ()

@property (strong, nonatomic) IBOutlet UILabel *dishName;
@property (strong, nonatomic) IBOutlet UILabel *dishDescription;

@end

@implementation FRAboutDishCell

- (void)setupCellWithMealModel:(FRMealExtendedModel *)meal {
    self.dishName.text = [FRCuisineModel getCuisisnesStringFromArray:meal.cuisines];
    self.dishDescription.text = meal.mealDescription;
}

@end
