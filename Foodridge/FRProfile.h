//
//  FRProfile.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Realm/Realm.h>

@interface FRProfile : RLMObject

@property NSInteger uid;
@property NSString *avatarURL;
@property NSString *email;
@property NSString *name;
@property NSString *phone;

@property NSString *deliveryStreet;
@property NSString *deliveryCity;
@property NSString *deliveryCountry;

@end
