//
//  FRProgressHUD.h
//  Foodridge
//
//  Created by Victor Miroshnichenko on 5/8/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FRProgressHUD : NSObject

+ (void)showMessageHudWithText:(NSString *)text inView:(UIView *)view delay:(CGFloat)delay;

@end
