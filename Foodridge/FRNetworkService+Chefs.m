//
//  FRNetworkService+Chefs.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService+Chefs.h"

@implementation FRNetworkService (Chefs)

+ (NSURLSessionTask *)chefsListWithLimit:(NSInteger)limit
                                  offset:(NSInteger)offset
                                followed:(BOOL)followed
                              searchText:(NSString *)searchText
                              completion:(FRResponceBlock)completion{
    
    NSMutableDictionary *params = [@{@"limit" : @(limit),
                             @"offset" : @(offset)} mutableCopy];
    
    if(searchText.length > 0){
        params[@"search"] = searchText;
    }
    
    return [self requestWithMethod:@"GET" requestAddress:@"chefs" parameters:params completion:completion];
}



+ (NSURLSessionTask *)followChef:(NSNumber*) chefId and:(bool)follow completion:(FRResponceBlock)completion {
    
    NSMutableDictionary *params = [@{@"chefId" : chefId,
                                     @"follow" : @(follow)} mutableCopy];
    
    
    return [self requestWithMethod:@"POST" requestAddress:@"follow_chef" parameters:params completion:completion];
}

+ (NSURLSessionTask *)getChefWithId:(NSNumber*)chefId completion:(FRResponceBlock)completion {
    
    return [self requestWithMethod:@"GET" requestAddress:[NSString stringWithFormat:@"get_chef/%@",chefId] parameters:nil completion:completion];
}

@end
