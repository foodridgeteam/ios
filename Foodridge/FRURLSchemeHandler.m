//
//  FRURLSchemeHandler.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRURLSchemeHandler.h"
#import "FRStripeService.h"

@implementation FRURLSchemeHandler

- (BOOL)handleURL:(NSURL *)url{
    
    NSLog(@"app did open URL: %@",url.absoluteString);
    
    if(![url.scheme isEqualToString:@"foodridge"]){
        return NO;
    }
    
    if([[FRStripeService new] processStripeRedirect:url]){
        return YES;
    }
    
    
    return YES;
}

@end
