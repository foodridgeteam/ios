//
//  TermsViewControler.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRTermsViewControler.h"
#import "FRNetworkService.h"
@interface FRTermsViewControler ()<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation FRTermsViewControler


- (void)viewDidLoad {
    [super viewDidLoad];

    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/html/tandc.html",baseURL]];
    [self.webView loadRequest:[[NSURLRequest alloc] initWithURL:url]];
    self.webView.delegate = self;
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"STR_DISMISS", nil) style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];

    if(self.showAgreeButton){
        
        UIBarButtonItem *agreeItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"STR_AGREE", nil) style:UIBarButtonItemStyleDone target:self action:@selector(agreeAction:)];
        agreeItem.enabled = NO;
        self.navigationItem.leftBarButtonItem = doneItem;
        self.navigationItem.rightBarButtonItem = agreeItem;
    }else{
        self.navigationItem.rightBarButtonItem = doneItem;

    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)agreeAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        if(self.onDismiss){
            self.onDismiss(YES);
        }
    }];
}


- (void)doneAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if(self.onDismiss){
            self.onDismiss(NO);
        }
    }];
}

@end
