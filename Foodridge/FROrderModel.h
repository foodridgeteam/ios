//
//  FROrder.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/22/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"
#import "FRMealExtendedModel.h"
#import "FRProfileModel.h"
#import "FRChefProfileModel.h"
#import "FRCommentsModel.h"

typedef NS_ENUM(NSInteger, FROrderStatus){
    FROrderStatusCreated                = 1,
    FROrderStatusAccepted               = 2,
    FROrderStatusPrepared               = 3,
    FROrderStatusSent                   = 4,
    FROrderStatusReceived               = 5,
    FROrderStatusPayed                  = 6,
    FROrderStatusCanceledByCustomer     = 7,
    FROrderStatusCanceledByChef         = 8,
    FROrderStatusCanceledByTimout       = 9,
    FROrderStatusCompleted              = 10
};

@interface FROrderModel : FRViewModel

@property (nonatomic, assign) BOOL asReceiver;

@property (nonatomic, copy) NSNumber *uid;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, assign) FRDeliveryType deliveryType;
@property (nonatomic, copy) FRMealExtendedModel *meal;
@property (nonatomic, copy) FRProfileModel *customer;
@property (nonatomic, assign) FROrderStatus status;
@property (nonatomic, copy) FRDeliveryInfoModel *deliveryAdress;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, copy) FRCommentsModel *comment;
@property (nonatomic, copy) FRBuisinessInfoModel *businessInfo;

@property (nonatomic, assign) CGFloat sum;

@property (nonatomic, copy) NSArray *messages;

@end
