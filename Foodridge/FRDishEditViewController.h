//
//  FRDishEditViewController.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/15/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRMealExtendedModel.h"

@interface FRDishEditViewController : UIViewController

@property(nonatomic, copy) id<FRMealModel> model;

@end
