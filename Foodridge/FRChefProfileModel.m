//
//  FRChefProfileModel.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRChefProfileModel.h"
#import "FRWeekScheduleModel.h"
#import "FRDeliveryInfoModel.h"
#import "FRBuisinessInfoModel.h"
#import "FRCuisineModel.h"
#import "FRDateHelper.h"

@implementation FRChefProfileModel

- (NSDictionary *)dictionaryRepresentation{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    if(self.bio){
        params[@"about"] = self.bio;
    }

    
    NSDictionary *pickupInfo = [self.pickupInfo dictionaryRepresentation];
    if(pickupInfo){
        params[@"deliveryInfo"] = pickupInfo;
    }
    
    NSDictionary *businessInfo = [self.businessInfo dictionaryRepresentation];
    
    if(businessInfo){
        params[@"businessInfo"] = businessInfo;
    }
    
    NSDictionary *weekSchedule = [self.weekSchedule dictionaryRepresentation];
    
    if(weekSchedule){
        params[@"workingTime"] = weekSchedule;
    }
    
    if(self.cuisines){
        params[@"cuisines"] = [self.cuisines valueForKey:@"uid"];
    }
    
    if([[params allKeys] count] == 0){
        return nil;
    }
    
    return params;
}

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary{
    
    BOOL isChef = NO;

    for(NSString *key in [dictionary allKeys]){
        if([dictionary objectOrNilForKey:key]){
            isChef = YES;
            break;
        }
    }
    if(!isChef){
        return nil;
    }
    
    FRChefProfileModel *profile = [FRChefProfileModel new];
    
    profile.uid = [dictionary objectOrNilForKey:@"id" classObject:[NSNumber class]];
    
    profile.bio = [dictionary objectOrNilForKey:@"about" classObject:[NSString class]];
    
    profile.cuisines = [FRCuisineModel getCuisinesModelsFromCuisinesIDsArray:[dictionary objectOrNilForKey:@"cuisines" classObject:[NSArray class]]];
    
    NSDictionary *deliveryInfoDict = [dictionary objectOrNilForKey:@"deliveryInfo" classObject:[NSDictionary class]];
    
    profile.pickupInfo =  [FRDeliveryInfoModel modelWitDictionary:deliveryInfoDict];
    
    NSDictionary *businessInfoDict = [dictionary objectOrNilForKey:@"businessInfo" classObject:[NSDictionary class]];
    
    profile.businessInfo =  [FRBuisinessInfoModel modelWitDictionary:businessInfoDict];
    
    NSDictionary *workingTimeDict = [dictionary objectOrNilForKey:@"workingTime" classObject:[NSDictionary class]];
    
    profile.weekSchedule =  [FRWeekScheduleModel modelWitDictionary:workingTimeDict];
    
    profile.stripeId = [dictionary objectOrNilForKeypath:@"paymentInfo.stripeId" classObject:[NSString class]];
    
    profile.reviewsCount = [dictionary objectOrNilForKey:@"votesCount" classObject:[NSNumber class]];
    
    profile.chefName = [dictionary objectOrNilForKey:@"name" classObject:[NSString class]];
    
    profile.avatarURL = [dictionary objectOrNilForKey:@"avatar" classObject:[NSString class]];
    
    profile.follow = [dictionary objectOrNilForKey:@"follow" classObject:[NSNumber class]];
    
    profile.followersCount = [dictionary objectOrNilForKey:@"followers" classObject:[NSNumber class]];
    
    profile.chefRating = [dictionary objectOrNilForKey:@"rating" classObject:[NSNumber class]];
    
    profile.lastVisit = [FRDateHelper dateFromString:[dictionary objectOrNilForKey:@"lastVisit" classObject:[NSString class]]];
    
    return profile;
}

@end
