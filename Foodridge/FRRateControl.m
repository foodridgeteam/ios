//
//  FRRateControl.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRRateControl.h"

@implementation FRRateControl

- (void)drawRect:(CGRect)rect {
    
    UIImage *fillImage = [self fillImage];
    UIImage *emptyImage = [self emptyImage];
    
    CGSize sz = fillImage.size;
    CGFloat k =  rect.size.height/sz.height;
    CGFloat interval = floor((rect.size.width - (sz.width * 5 * k)) / 4);
    CGRect r = CGRectMake(0, 0, floor(sz.width*k), floor(sz.height * k));
    
    for(int i = 0; i < 5; i++){
        
        if(self.value > i){
            [fillImage drawInRect:r];
        }else{
            [emptyImage drawInRect:r];
        }
        r.origin.x += r.size.width + interval;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    
    UITouch *touch = [touches anyObject];
    CGFloat x = [touch locationInView:self].x;
    NSInteger value = floor((x/ self.bounds.size.width) * 5);
    [self setValue:value+1];

}


- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesEnded:touches withEvent:event];
    
    UITouch *touch = [touches anyObject];
    CGFloat x = [touch locationInView:self].x;
    NSInteger value = floor((x / self.bounds.size.width) * 5);
    [self setValue:value+1];
    [self sendActionsForControlEvents:UIControlEventValueChanged];

}

- (void)touchesMoved:(NSSet<UITouch *> *)touches
           withEvent:(UIEvent *)event{
    
    [super touchesMoved:touches withEvent:event];

    UITouch *touch = [touches anyObject];
    CGFloat x = [touch locationInView:self].x;
    NSInteger value = floor((x/ self.bounds.size.width) * 5);
    [self setValue:value+1];

}

- (void)setValue:(NSInteger)value{
    value = MIN(value, 5);
    value = MAX(value, 0);
    _value = value;
    [self setNeedsDisplay];
}

-(UIImage *)fillImage{
    return nil;
}

- (UIImage *)emptyImage{
    return  nil;
}

@end
