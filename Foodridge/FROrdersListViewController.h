//
//  OrdersListViewController.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FROrdersService.h"

@interface FROrdersListViewController : UITableViewController

@property(nonatomic, assign) FROrdersFilter selectedFilter;

@end
