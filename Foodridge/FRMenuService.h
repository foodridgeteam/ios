//
//  FRMenuService.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FRNetworkService.h"
#import "FRMealModel.h"

@interface FRMenuService : NSObject

+ (FRMenuService *)service;

- (NSURLSessionTask *)updateMyMealWithModel:(id<FRMealModel>)model completion:(FRResponceBlock)completion;


@end
