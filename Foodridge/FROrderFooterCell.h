//
//  FROrderFooterCell.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FROrderModel.h"

@class FROrderFooterCell;

@protocol FROrderFooterCellDelegate <NSObject>

@optional
- (void)acceptOrderInCell:(FROrderFooterCell *)cell;
- (void)preparredOrderInCell:(FROrderFooterCell *)cell;
- (void)cancelOrderInCell:(FROrderFooterCell *)cell;
- (void)messageToOrderInCell:(FROrderFooterCell *)cell;
- (void)deliverOrderInCell:(FROrderFooterCell *)cell;

@end

@interface FROrderFooterCell : UITableViewCell

-(void)setupWithOrder:(FROrderModel *)order;

@property (weak, nonatomic) id<FROrderFooterCellDelegate> delegate;

@end
