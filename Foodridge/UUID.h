//
//  UUID.h
//  EndyMed
//
//  Created by Vitalii Yevtushenko on 2/27/13.
//  Copyright (c) 2012-2015 Roll'n'Code (rollncode.com). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UUID : NSObject

+ (NSString *)uuid;

@end
