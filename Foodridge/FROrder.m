//
//  FROrder.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/27/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrder.h"

@implementation FROrder

+ (NSString *)primaryKey{
    return @"uid";
}

@end
