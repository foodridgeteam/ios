//
//  FRStripePaymentViewController.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/26/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRMealModel.h"

@interface FRStripePaymentViewController : UIViewController

@property(nonatomic, copy) NSNumber *mealID;
@property(nonatomic, assign) NSInteger portions;
@property(nonatomic, assign) long totalPrice;//cents
@property(nonatomic, assign) FRDeliveryType type;

@end
