//
//  RealmService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Realm/Realm.h>

@interface FRRealmService : NSObject

- (void)clear;

- (void)writeObject:(RLMObject *)object withCompletion:(dispatch_block_t)completion;
- (void)writeObjects:(NSArray *)objects withCompletion:(dispatch_block_t)completion;


+ (RLMRealm *)currentRealm;

@end
