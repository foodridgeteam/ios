//
//  UIImageView+BetterThanAFNetworking.m
//  Foodridge
//
//  Created by Dev on 5/17/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "UIImageView+AFNetworking.h"

#import "UIImageView+BetterThanAFNetworking.h"

@implementation UIImageView (BetterThanAFNetworking)

-(void)fr_setImageWithUrl:(NSURL *)url placeholderImage:(UIImage *)placeholder success:(void (^)(UIImage *))success failure:(void (^)(NSError *))failure{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    
    [self setImageWithURLRequest:request placeholderImage:placeholder success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        
        if(success){
            success(image);
        }
        
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
        if(failure){
            failure(error);
        }
        
    }];

}

@end
