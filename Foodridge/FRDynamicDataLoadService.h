//
//  FRDynamicDataLoadService.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/25/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FRNetworkService.h"

static  NSString *FRLoadTableViewDataNotification = @"FRLoadTableViewDataNotification";
static NSInteger defaultLimit = 3;
static NSInteger defaultOffset = 0;

@interface FRDynamicDataLoadService : NSObject

+ (instancetype)service;

@property(nonatomic, strong) NSMutableArray *items;

@property(nonatomic, assign) NSInteger offset;
@property (nonatomic, assign) BOOL loadMore;


- (void)clearData;

+ (Class)modelClass;
+ (NSString *)notificationName;

- (void)prodcessResponce:(id)rawResponce orError:(NSError *)error withCompletion:(FRResponceBlock)completion;

@end
