//
//  UIImageView+BetterThanAFNetworking.h
//  Foodridge
//
//  Created by Dev on 5/17/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (BetterThanAFNetworking)

- (void)fr_setImageWithUrl:(NSURL *)url placeholderImage:(UIImage *)placeholder success:(void (^)(UIImage *image))success failure:(void (^)(NSError *error))failure;


@end
