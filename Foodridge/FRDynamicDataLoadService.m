//
//  FRDynamicDataLoadService.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/25/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRDynamicDataLoadService.h"
#import "FRNetworkService+Menu.h"
#import "FRNetworkService+Chefs.h"
#import "FRMealExtendedModel.h"
#import "FRChefProfileModel.h"

@implementation FRDynamicDataLoadService

+ (instancetype)service{
    
    NSAssert(YES, @"What the fuck, this class is abstract!!!");
    return nil;
}

#pragma mark Meals


- (void)prodcessResponce:(id)rawObject orError:(NSError *)error withCompletion:(FRResponceBlock)completion{
    
    _loadMore = !rawObject ? NO : YES;
    
    NSArray *models = [[self.class modelClass] modelsFromArray:rawObject];
    
    self.offset += 1;
    
    [_items addObjectsFromArray:[self filteredFromDuplicatesArray:models]];
    [[NSNotificationCenter defaultCenter] postNotificationName:[self.class notificationName] object:nil];
    
    if (completion) {
        completion(models, rawObject, error);
    }
}

+ (Class)modelClass{
    return nil;
}

+ (NSString *)notificationName{
    return FRLoadTableViewDataNotification;
}

- (NSArray *)filteredFromDuplicatesArray:(NSArray *)array {
   
    NSMutableArray *uniqueArray = [NSMutableArray array];
    NSMutableSet *uids = [NSMutableSet setWithArray:[self.items valueForKey:@"uid"]];
    for (id obj in array) {
        NSNumber *uniqueObject = [obj uid];
        if (![uids containsObject:uniqueObject]) {
            [uniqueArray addObject:obj];
        }
    }
    return uniqueArray;
}

- (void)clearData {
    
    [self.items removeAllObjects];
    self.loadMore = YES;
    self.offset = 0;
    
}

@end
