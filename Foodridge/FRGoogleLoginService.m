//
//  FRGoogleLoginService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRGoogleLoginService.h"
#import <Google/SignIn.h>


@interface FRGoogleLoginService () <GIDSignInDelegate, GIDSignInUIDelegate>

@property(nonatomic, strong) UIViewController *vc;
@property(nonatomic, copy) FRGoogleLoginBlock completion;

@end

@implementation FRGoogleLoginService

+ (FRGoogleLoginService *)service{
    
    static FRGoogleLoginService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [FRGoogleLoginService new];
    });
    return service;
}


- (instancetype)init{
    if(self = [super init]){
        
        NSError* configureError;
        [[GGLContext sharedInstance] configureWithError: &configureError];
        NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
        
        [GIDSignIn sharedInstance].delegate = self;
    }
    return self;
    
}

- (void)loginFromViewController:(UIViewController *)cntr completion:(FRGoogleLoginBlock)completion{
    
    self.completion = completion;
    self.vc = cntr;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];


}

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    [self.vc presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self.vc dismissViewControllerAnimated:YES completion:nil];
}


- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    
    if(!error){
        NSString *idToken = user.authentication.accessToken; // Safe to send to the server
        if(idToken){
            self.completion(idToken,nil);
        }else{
            NSError *err = [NSError errorWithDomain:@"Google signin error" code:500 userInfo:nil];
            self.completion(nil, err);

        }
    }else{
           self.completion(nil, error);
    
        }

    // ...
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    self.completion(nil, error);

    // Perform any operations when the user disconnects from app here.
    // ...
}



@end
