//
//  OrdersListViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <libextobjc/extobjc.h>
#import "FROrdersListViewController.h"
#import "FROrdersService.h"
#import "UIColor+FoodridgeColors.h"
#import "FROrderHeaderCell.h"
#import "FROrderFooterCell.h"
#import "FROrderMessageCell.h"
#import "FROrderFooterCell.h"
#import "FROrderRateCell.h"
#import "FROrderCommentCell.h"
#import "FRMessageView.h"
#import "FRMessagesService.h"
#import "FRCommentsService.h"
#import "FROrdersContainerViewController.h"
#import "MBProgressHUD.h"
#import "FRNotificationsService.h"
#import "FREmptyDataView.h"
#import "FRMainScreenService.h"

@interface FROrdersListViewController () <FROrderHeaderCellDelegate, FROrderFooterCellDelegate, FROrderRateCellDelegate>

@property(nonatomic, strong) NSMutableArray *currentOrdersList;
@property(nonatomic, strong) FREmptyDataView *emptyDataView;

@end

@implementation FROrdersListViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.emptyDataView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FREmptyDataView class]) owner:self options:nil] objectAtIndex:0];
    
    self.currentOrdersList = [NSMutableArray new];
    
    UIRefreshControl *refresh = [UIRefreshControl new];
    [refresh addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [refresh setTintColor:[UIColor aquaColor]];
    [refresh setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"STR_LOADING", nil)]];
    [self setRefreshControl:refresh];
    self.tableView.estimatedRowHeight = 10;
    [self reloadData];
    [self refresh];
}

- (void)setSelectedFilter:(FROrdersFilter)selectedFilter{
    _selectedFilter = selectedFilter;
    
    if (self.emptyDataView) {
        [self.emptyDataView removeFromSuperview];
    }
    
    [self refresh];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:FROrderUpdatedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMessage:) name:FROrderNewMessageNotificationName object:nil];
    [self reloadData];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:FROrderUpdatedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:FROrderNewMessageNotificationName object:nil];
}

- (void)reloadData{
    self.currentOrdersList = [[[FROrdersService service] ordersWithFilter:self.selectedFilter] mutableCopy];
    
    if (!self.currentOrdersList.count) {
        self.emptyDataView.filterType = self.selectedFilter;
        self.emptyDataView.frame = self.tableView.frame;
        if (![self.emptyDataView isDescendantOfView:self.tableView]) {
            [self.tableView addSubview:self.emptyDataView];
        }
    }
    else {
        [self.emptyDataView removeFromSuperview];
    }
    
    [self.tableView reloadData];
}

- (void)newMessage:(NSNotification *)notification {
    
    NSNumber *orderUID = [NSNumber numberWithInteger:[[notification.object objectForKey:@"entity"] integerValue]];
    NSNumber *type = [NSNumber numberWithInteger:[[notification.object objectForKey:@"type"] integerValue]];
    
    FROrderModel *newMessagesOrder = [FROrderModel new];
    
    if (type.integerValue == FROrderStatusCompleted || type.integerValue == FROrderStatusReceived || type.integerValue == FROrderStatusCanceledByChef || type.integerValue == FROrderStatusCanceledByCustomer) {

        FROrdersContainerViewController *vc = (FROrdersContainerViewController *)self.parentViewController;
        [vc.segmentedControl setSelectedSegmentIndex:FROrdersFilterCompleted];
        [self setSelectedFilter:FROrdersFilterCompleted];
        return;
    }
    
    for (FROrderModel *order in self.currentOrdersList) {
        if ([order.uid compare:orderUID] == NSOrderedSame) {
            newMessagesOrder = order;
            if (type.integerValue != notificationTypeNewMessage) {
                newMessagesOrder.status = type.integerValue;
            }
        }
    }
    
    [[FRMessagesService service] getMessagesForOrderID:orderUID.integerValue lastID:0 completion:^(id object, id rawObject, NSError *error) {
        
        NSInteger index = [self.currentOrdersList indexOfObject:newMessagesOrder];
        
        if (index == NSNotFound) {
            return;
        }
            NSArray *messages = [FROrderMessageModel modelsFromArray:rawObject];
            newMessagesOrder.messages = messages;
            [self.currentOrdersList replaceObjectAtIndex:index withObject:newMessagesOrder];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
            [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
    }];
}

- (void)refresh{
    if (self.selectedFilter == FROrdersFilterCompleted) {
        [[FROrdersService service] refreshCompletedOrdersWithCompletion:^(id object, id rawObject, NSError *error) {
            [self.refreshControl endRefreshing];
            [self reloadData];
            [self.tableView reloadData];
        }];
    }
    else {
        [[FROrdersService service] refreshOrdersListWithCompletion:^(id object, id rawObject, NSError *error) {
            [self.refreshControl endRefreshing];
            [self reloadData];
            [self.tableView reloadData];
        }];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        
        return [self heightForHeaderCell];
        
    } else {
        
        if (self.selectedFilter == FROrdersFilterCompleted) {
            
            return UITableViewAutomaticDimension;
            
        } else {
            
            return 90.0;
            
        }
    }
}

- (CGFloat) heightForHeaderCell {
    return self.selectedFilter == FROrdersFilterCompleted ? 237.0f : 371.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [[self currentOrdersList] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FROrderModel *order = self.currentOrdersList[indexPath.section];
    
    if (self.selectedFilter != FROrdersFilterCompleted) {
        if(indexPath.row == 0){
            return [self headerCellForOrder:order];
        }else if(indexPath.row == (order.messages.count + 1)){
            return [self footerCellForOrder:order];
        }else{
            return [self messageCellForMessage:order.messages[indexPath.row - 1] first:indexPath.row == 1 order:order];
        }
    }
    else {
        if(indexPath.row == 0){
            return [self headerCellForOrder:order];
            
        } else {
            
            if (order.asReceiver && !order.comment.creationDate && order.status != FROrderStatusCanceledByChef && order.status != FROrderStatusCanceledByCustomer && order.status != FROrderStatusCanceledByTimout) {
                return [self rateOrderCellForOrder:order];
            } else {
                return [self commentCellForOrder:order.comment];
            }
            
        }
    }
    
    return nil;
}

- (UITableViewCell *)headerCellForOrder:(FROrderModel *)order{
    FROrderHeaderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"OrderHeaderCell"];
    [cell setupWithOrder:order];
    cell.delegate = self;
    return  cell;
}

- (UITableViewCell *)footerCellForOrder:(FROrderModel *)order{
    
    FROrderFooterCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"orderFooterCell"];
    [cell setupWithOrder:order];
    cell.delegate = self;
    return  cell;
}

- (UITableViewCell *)messageCellForMessage:(FROrderMessageModel *)orderMessage first:(BOOL)first order:(FROrderModel *)order {
    
    FROrderMessageCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"orderMessageCell"];
    cell.topLineConstraint.active = !first;
    [cell setupWithOrder:order message:orderMessage];
    return  cell;
}

- (UITableViewCell *)rateOrderCellForOrder:(FROrderModel *)order {
    
    FROrderRateCell *rateCell = [self.tableView dequeueReusableCellWithIdentifier:@"orderRateCell"];
    rateCell.delegate = self;
    return rateCell;
}

- (UITableViewCell *)commentCellForOrder:(FRCommentsModel *)comment {
    FROrderCommentCell *orderCommentCell = [self.tableView dequeueReusableCellWithIdentifier:@"orderCommentCell"];
    [orderCommentCell setupCellWithCommentModel:comment];
    return orderCommentCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    FROrderModel *order = self.currentOrdersList[section];
    
    if (self.selectedFilter == FROrdersFilterCompleted) {
        
        if (order.status == FROrderStatusCanceledByChef || order.status == FROrderStatusCanceledByCustomer || order.status == FROrderStatusCanceledByTimout) {
            return 1;
        }
        else {
            if (!order.asReceiver && !order.comment.rating) {
                return 1;
            }
            else {
                return 2;
            }
        }
    }
    else {
        return order.messages.count + 2;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return CGFLOAT_MIN;
    return tableView.sectionHeaderHeight;
}

#pragma mark Rate order delegate

- (void)rateOrderWithText:(NSString *)reviewText chefRating:(NSInteger)chefRating inCell:(FROrderRateCell *)cell{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    FROrderModel *order = [self getOrderFromCell:cell];
    
    @weakify(self);
    [[FRCommentsService service] leaveCommentForOrderWithID:order.uid.integerValue commentText:reviewText rating:chefRating completion:^(id object, id rawObject, NSError *error) {
        @strongify(self);
        FRCommentsModel *comment = [FRCommentsModel modelWitDictionary:rawObject];
        order.comment = comment;
        NSInteger index = [self.currentOrdersList indexOfObject:order];
        [self.currentOrdersList replaceObjectAtIndex:index withObject:order];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
        [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

#pragma mark FROrderFooterCell Delegate

- (void)preparredOrderInCell:(FROrderFooterCell *)cell{
    
    [self setOrderStatus:FROrderStatusPrepared forOrder:[self getOrderFromCell:cell]];
    
}

- (void)acceptOrderInCell:(FROrderFooterCell *)cell {
    
    FROrderModel *order = [self getOrderFromCell:cell];
    
    if (order.status == FROrderStatusCreated) {
        @weakify(self);
        UIAlertController *alert=   [UIAlertController
                                     alertControllerWithTitle:@"Info"
                                     message:@"Do you want accept order?"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *ok = [UIAlertAction
                             actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 @strongify(self);
                                 [self setOrderStatus:FROrderStatusAccepted forOrder:[self getOrderFromCell:cell]];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        UIAlertAction *cancel = [UIAlertAction
                                 actionWithTitle:@"No"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)cancelOrderInCell:(FROrderFooterCell *)cell {
    
    @weakify(self);
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Info"
                                  message:@"Do you want cancel order?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Yes"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action) {
                             @strongify(self);
                             FROrderModel *order = [self getOrderFromCell:cell];
                             [self setOrderStatus:order.asReceiver ? FROrderStatusCanceledByCustomer : FROrderStatusCanceledByChef forOrder:order];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"No"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)messageToOrderInCell:(FROrderFooterCell *)cell {

    FROrderModel *order = [self getOrderFromCell:cell];
    
    FRMessageView *messageView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([FRMessageView class]) owner:self options:nil] objectAtIndex:0];
    [messageView setAutoresizingMask:UIViewAutoresizingNone];
    messageView.frame = self.parentViewController.view.frame;
    [self.parentViewController.view addSubview:messageView];
    
    messageView.chef = !order.asReceiver;
    
    @weakify(messageView);
    [messageView setOnDone:^(BOOL cancelled, NSString *messageText) {
        @strongify(messageView);
        if (cancelled) {
            [messageView removeFromSuperview];
        }
        
        if (messageText.length) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[FRMessagesService service] leaveMessageForOrderWithID:order.uid.integerValue messageText:messageText completion:^(id object, id rawObject, NSError *error) {
                
                [[FRMessagesService service] getMessagesForOrderID:order.uid.integerValue lastID:0 completion:^(id object, id rawObject, NSError *error) {
                    NSInteger index = [self.currentOrdersList indexOfObject:order];
                    NSArray *messages = [FROrderMessageModel modelsFromArray:rawObject];
                    order.messages = messages;
                    [self.currentOrdersList replaceObjectAtIndex:index withObject:order];
                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
                    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                }];
            }];
            [messageView removeFromSuperview];
        }
    }];
}

- (void)deliverOrderInCell:(FROrderFooterCell *)cell {
    FROrderModel *order = [self getOrderFromCell:cell];
    
    if (order.asReceiver) {
        [self setOrderStatus:FROrderStatusReceived forOrder:order];
    }
    else {
        if (order.status == FROrderStatusAccepted) {
            [self setOrderStatus:FROrderStatusPrepared forOrder:order];
        }
        else {
            [self setOrderStatus:FROrderStatusSent forOrder:order];
        }
    }
}

- (void)setOrderStatus:(NSInteger)status forOrder:(FROrderModel *)order{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    @weakify(self);
    [[FROrdersService service] setOrderStatus:status forOrderWithID:order.uid.integerValue completion:^(id object, id rawObject, NSError *error) {
        @strongify(self);
        
        NSInteger index = [self.currentOrdersList indexOfObject:order];
        FROrderModel *updatedOrder = [FROrderModel modelWitDictionary:rawObject];
        
        if (index == NSNotFound) {
            return;
        }
        
        if (updatedOrder.status == FROrderStatusCanceledByChef || updatedOrder.status == FROrderStatusCanceledByCustomer) {
            [self refresh];
            FROrdersContainerViewController *vc = (FROrdersContainerViewController *)self.parentViewController;
            [vc.segmentedControl setSelectedSegmentIndex:FROrdersFilterCompleted];
            [self setSelectedFilter:FROrdersFilterCompleted];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            return;
        }
        
        [[FRMessagesService service] getMessagesForOrderID:updatedOrder.uid.integerValue lastID:0 completion:^(id object, id rawObject, NSError *error) {
            NSArray *messages = [FROrderMessageModel modelsFromArray:rawObject];
            updatedOrder.messages = messages;
            [self.currentOrdersList replaceObjectAtIndex:index withObject:updatedOrder];
            if (index == NSNotFound) {
                return;
            }
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
            [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }];
}

- (FROrderModel *)getOrderFromCell:(UITableViewCell *)cell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    return self.currentOrdersList[indexPath.section];
}

#pragma mark Call to Chef

- (void)callPhoneNumber:(NSString*)number name:(NSString*)name {
    
    if (number) {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"STR_PHONE_CALL", nil)
                                              message:[NSString stringWithFormat:@"Would you like to call to the %@",name]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                       NSString *phoneNumber = [@"tel:" stringByAppendingString:number];
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                                       
                                   }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"STR_PHONE_CALL", nil)
                                              message:NSLocalizedString(@"STR_PHONE_NOT_LISTED", nil)
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Close action");
                                   }];
        
    
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

@end
