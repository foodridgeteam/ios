//
//  FRIngridientModel.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/15/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRIngridientModel.h"
#import "FRStaticDataService.h"

@implementation FRIngridientModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"uid":@"id",
             @"name":@"name"
             };
}

+ (NSArray *)getIngredientsModelsFromIngredientsIDsArray:(NSArray *)ingredientsIDsArray {
    NSArray *ingredientsModels = ingredientsIDsArray ? [[FRStaticDataService service] getIngridientsWithIDs:[ingredientsIDsArray valueForKey:@"intValue"]] : nil;
    return ingredientsModels;
}

@end
