//
//  FRDateHelper.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/26/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRDateHelper : NSObject

+ (NSDate *)dateFromString:(NSString *)string;
+ (NSString *)dateStringFromDate:(NSDate *)date;

@end
