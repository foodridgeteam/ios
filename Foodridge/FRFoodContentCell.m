//
//  FRFoodContentCell.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRFoodContentCell.h"
#import "FRSpiceControl.h"

@interface FRFoodContentCell ()

@property (strong, nonatomic) IBOutlet UILabel *contentName;
@property (strong, nonatomic) IBOutlet UIView *spiceControlView;
@property (strong, nonatomic) IBOutlet UIImageView *disclaimerImage;
@property (strong, nonatomic) IBOutlet FRSpiceControl *spiceControl;

@end

@implementation FRFoodContentCell

- (void)awakeFromNib {

}

- (void)setFoodContentCellWithFlag:(NSString *)flag mealModel:(FRMealExtendedModel *)meal {
    self.spiceControlView.hidden = self.hideSpicyControl;
    self.disclaimerImage.hidden = !self.hideSpicyControl;
    self.contentName.text = self.hideSpicyControl ? flag.capitalizedString : NSLocalizedString(@"STR_SPICY", nil);
    self.disclaimerImage.image = [[meal.flags objectForKey:flag] boolValue] ? [UIImage imageNamed:@"food_on"] : [UIImage imageNamed:@"food_off"];
    self.spiceControl.value = meal.spiceRate.integerValue;
    self.spiceControl.spicyControlForMealInfo = YES;
}


@end
