//
//  FRNetworkService+Meal.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService.h"
#import "FRMealModel.h"

@interface FRNetworkService (Menu)

+ (NSURLSessionTask *)mealListWithLimit:(NSInteger)limit
                                 offset:(NSInteger)offset 
                                 chefID:(NSInteger)chefID
                             searchText:(NSString *)searchText
                             completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)updateMyMealWithModel:(FRViewModel <FRMealModel> *)model
                                 completion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)getMealWithID:(NSInteger)mealID
                         completion:(FRResponceBlock)completion;

@end
