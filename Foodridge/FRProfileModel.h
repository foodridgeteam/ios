//
//  FRProfile.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"
#import "FRDeliveryInfoModel.h"

@interface FRProfileModel :FRViewModel

@property(nonatomic, copy) NSNumber *uid;
@property(nonatomic, copy) NSString *avatarURL;
@property(nonatomic, copy) NSString *email;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *phone;

@property(nonatomic, strong) NSArray *cuisines;

@property(nonatomic, strong) FRDeliveryInfoModel *deliveryInfo;

- (NSDictionary *)dictionaryRepresentation;

@end
