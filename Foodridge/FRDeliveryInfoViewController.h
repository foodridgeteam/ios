//
//  FRDeliveryInfoViewController.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/8/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRSelectionFormViewController.h"

@class FRDeliveryInfoModel;
@interface FRDeliveryInfoViewController : FRSelectionFormViewController

@property(strong, nonatomic) FRDeliveryInfoModel *model;

@property (nonatomic, copy) void (^onSave)(FRDeliveryInfoModel *changes);

@end
