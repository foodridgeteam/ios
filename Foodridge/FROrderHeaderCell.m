//
//  FROrderHeaderCell.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrderHeaderCell.h"
#import "NSString+DateFormats.h"
#import "FRMealModel.h"
#import "FRProfileModel.h"
#import "FRBuisinessInfoModel.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+RoundImage.h"
#import "UIColor+FoodridgeColors.h"

@interface FROrderHeaderCell ()
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *localizableButtons;
@property (strong, nonatomic) IBOutlet UILabel *idLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;

@property (strong, nonatomic) IBOutlet UILabel *mealTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *sumLabel;
@property (strong, nonatomic) IBOutlet UILabel *adressTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *adresslabel;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIView *topLineView;
@property (strong, nonatomic) IBOutlet UILabel *orderUpdateTimeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *clockImageView;

@property (strong, nonatomic) FROrderModel *order;
@end

@implementation FROrderHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height / 2;
    self.avatarImageView.clipsToBounds = YES;
}

- (void)setupWithOrder:(FROrderModel *)order{
    
    
    [self.idLabel setText:[NSString stringWithFormat:NSLocalizedString(@"STR_ORDER_ID_",nil), (long)order.uid.integerValue]];
    [self.dateLabel setAttributedText:[NSString orderCreationTimeFromDate:order.date]];
    self.order = order;
    
    self.usernameLabel.text = order.asReceiver ? order.meal.chefTitle : order.customer.name;
    
    NSString *avatar = order.asReceiver ? order.meal.chefImageURL : order.customer.avatarURL;

   [self.avatarImageView fr_setRoundImageWithUrl:[NSURL URLWithString:avatar]  placeholderImage:[UIImage imageNamed:@"ic_no_user"] success:^(UIImage *image) {} failure:^(NSError *error) {}];
    
    [self.mealTitleLabel setText:[NSString stringWithFormat:@"%ld x %@",(long)order.count, order.meal.title]];
    [self.sumLabel setText:[NSString stringWithFormat:@"$%.2lf",order.sum]];
    if(order.deliveryType == FRDeliveryTypeDelivery){
        self.adressTitleLabel.text = NSLocalizedString(@"STR_DELIVERY_ADDRESS", nil);
    }else{
        self.adressTitleLabel.text = NSLocalizedString(@"STR_PICKUP_ADDRESS", nil);
    }
    [self.adresslabel setAttributedText:[order.deliveryAdress stringRepresemtation]];

    if(order.status == FROrderStatusCanceledByCustomer
       || order.status == FROrderStatusCanceledByChef
       || order.status == FROrderStatusCanceledByTimout){
        self.topLineView.backgroundColor = [UIColor segmentLineColor];
        self.idLabel.textColor = [UIColor segmentLineColor];
    }else{
        self.topLineView.backgroundColor = [UIColor aquaColor];
        self.idLabel.textColor = [UIColor aquaColor];
    }
    
    
    NSDate *lastDate = order.date;
    if(order.messages.count > 0){
        lastDate = [[order.messages lastObject] date];
    }
    
    self.orderUpdateTimeLabel.text = [NSString localHoursMinutesStringFromDate:lastDate];
   
    if (order.status == FROrderStatusCanceledByChef || order.status == FROrderStatusCanceledByCustomer || order.status == FROrderStatusCanceledByTimout || order.status == FROrderStatusCompleted) {
        self.orderUpdateTimeLabel.hidden = YES;
        self.clockImageView.hidden = YES;
    }
    else {
        self.orderUpdateTimeLabel.hidden = NO;
        self.clockImageView.hidden = NO;
    }
    
    NSLog(@"%@",order);
}

- (IBAction)callPhoneNumber:(id)sender {
    if (self.order.asReceiver) {
        [self.delegate callPhoneNumber:self.order.businessInfo.phone name:self.order.meal.chefTitle];
    }
}

@end
