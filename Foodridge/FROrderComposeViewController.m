//
//  FROrderComposeViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/25/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrderComposeViewController.h"
#import "UIColor+FoodridgeColors.h"
#import "UIImage+Color.h"
#import "FRAppearanceManager.h"
#import "FROrderModel.h"
#import <Stripe/Stripe.h>
#import "FRStripePaymentViewController.h"
#import "UIFont+FoodridgeFonts.h"
#import "FRDeliveryInfoModel.h"

@interface FROrderComposeViewController () <UITextFieldDelegate>

#define kOFFSET_FOR_KEYBOARD 90.0

@property(nonatomic, copy) id<FRMealModel> meal;
@property(nonatomic, assign) FRDeliveryType deliveryType;

@property(nonatomic, assign) NSInteger count;

@property (strong, nonatomic) IBOutlet UILabel *mealTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *mealPricelabel;

@property (strong, nonatomic) IBOutlet UISegmentedControl *orderTypeControl;
@property (strong, nonatomic) IBOutlet UILabel *orderTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *pickupAddressLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pickupAddressConstraint;
@property (strong, nonatomic) IBOutlet UIView *pickupAdressView;

@property (strong, nonatomic) IBOutlet UIView *plusMinusView;
@property (strong, nonatomic) IBOutlet UIButton *minusButton;
@property (strong, nonatomic) IBOutlet UIButton *plusButton;
@property (strong, nonatomic) IBOutlet UILabel *counterLabel;
@property (strong, nonatomic) IBOutlet UITextField *counterTextField;

@property (strong, nonatomic) IBOutlet UILabel *mealTotalTitleLabel;

@property (strong, nonatomic) IBOutlet UILabel *totalQuantityLabel;
@property (strong, nonatomic) IBOutlet UILabel *sumLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryChargeLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalSumLabel;
@property (strong, nonatomic) IBOutlet UIButton *orderButton;

@end

@implementation FROrderComposeViewController

+ (void)presentWithMeal:(id<FRMealModel>)mealModel
     fromViewController:(UIViewController *)sourceViewController{
    
    if([[mealModel servingsLeft] integerValue] == 0){
        
        UIAlertController *cntr = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_NO_SERVINGS_LEFT", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        [cntr addAction:action];

        
        [sourceViewController presentViewController:cntr animated:YES completion:nil];
                                   
        return;
    }
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"OrderCompose" bundle:nil];
    UINavigationController *nav = [sb instantiateInitialViewController];
    
    FROrderComposeViewController *cntr = [nav.viewControllers firstObject];
    
    
    [cntr setMeal:mealModel];
    [cntr setDeliveryType:mealModel.deliveryType.integerValue animated:YES];
    
    [sourceViewController presentViewController:nav animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.mealTotalTitleLabel.text = [self.meal title];
    self.mealTitleLabel.text = [self.meal title];
    self.mealPricelabel.text = [NSString stringWithFormat:@"$%.2lf",[self.meal price].floatValue];
    [self setupUI];
    [self setupDeliveryType];
    self.count = 1;
    self.counterTextField.delegate = self;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlack;
    numberToolbar.items =
  @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
   [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
   [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    
    for (UIBarButtonItem* item in numberToolbar.items)
    {
        [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIFont openSansFontWithSize:15] , NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    }
    
    [numberToolbar sizeToFit];
    self.counterTextField.inputAccessoryView = numberToolbar;
}

-(void)cancelNumberPad{
    [self.counterTextField resignFirstResponder];
    self.counterLabel.hidden = NO;
    self.counterTextField.text = @"";
}

-(void)doneWithNumberPad{
    NSString *numberFromTheKeyboard = self.counterTextField.text;
    self.counterLabel.text = numberFromTheKeyboard;
    self.counterLabel.hidden = NO;
    self.count = [numberFromTheKeyboard integerValue];
    self.counterTextField.text = @"";
    [self.counterTextField resignFirstResponder];
}



- (void)setupUI{
    
    UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"STR_CANCEL", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction)];
    self.navigationItem.leftBarButtonItem = cancelItem;
    
    [FRAppearanceManager setupStandardSegmentControl:self.orderTypeControl];
    
    self.plusMinusView.layer.masksToBounds = YES;
    self.plusMinusView.layer.borderWidth = 1.;
    self.plusMinusView.layer.borderColor = [UIColor aquaColor].CGColor;
    self.plusMinusView.layer.cornerRadius = 3.;
    
    [self.orderButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    self.orderButton.layer.masksToBounds = YES;
    self.orderButton.layer.cornerRadius = 3.;

}

- (void)setupDeliveryType{
    
    FRDeliveryType possibleType = [self.meal.deliveryType integerValue];
    if(possibleType == FRDeliveryTypePickupAndDelivery){
        self.orderTypeControl.hidden = NO;
        self.orderTypeLabel.hidden = YES;
        [self setDeliveryType:FRDeliveryTypeDelivery animated:NO];
    }else{
        self.orderTypeControl.hidden = YES;
        self.orderTypeLabel.hidden = NO;
        [self setDeliveryType:possibleType];
    }

    self.pickupAddressLabel.attributedText = [self.meal.deliveryInfo stringRepresemtation];
}

- (void)cancelAction{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void) setDeliveryType:(FRDeliveryType)deliveryType animated:(BOOL)animated{
    self.deliveryType = deliveryType;
    if(self.deliveryType == FRDeliveryTypePickup){
        
        self.pickupAddressConstraint.active = YES;
        self.pickupAdressView.alpha = 0.;
    }else{
        self.pickupAddressConstraint.active = NO;
        self.pickupAdressView.alpha = 1.;
    }
    
    [UIView animateWithDuration:animated?0.2:0.0 animations:^{
        self.pickupAdressView.alpha = (self.deliveryType == FRDeliveryTypePickup)?1.:0.;
        [self.view layoutIfNeeded];
    }];
}

- (void)setDeliveryType:(FRDeliveryType)deliveryType{
    _deliveryType = deliveryType;
    if(deliveryType == FRDeliveryTypeDelivery){
        self.orderTypeControl.selectedSegmentIndex = 0;
        self.orderTypeLabel.text = NSLocalizedString(@"STR_DELIVERY", nil);
    }else if(deliveryType == FRDeliveryTypePickup){
        self.orderTypeControl.selectedSegmentIndex = 1;
        self.orderTypeLabel.text = NSLocalizedString(@"STR_PICKUP", nil);
    }else{
        self.orderTypeLabel.text = @"";
    }
    [self resetSumms];
}

- (void)setCount:(NSInteger)count{
    NSInteger c = MAX(count,1);
    c = MIN(c,[self.meal servingsLeft].integerValue);
    _count = c;
    self.counterLabel.text = [NSString stringWithFormat:@"%ld",(long)c];
    self.totalQuantityLabel.text = c>1?[NSString stringWithFormat:@"(%ld)",(long)c]:@"";
    [self resetSumms];
}

- (void)resetSumms{
    
    long sum = (long)([self.meal price].floatValue *100)  * self.count;
    long delivery = (self.deliveryType == FRDeliveryTypeDelivery)?((long)([self.meal deliveryPrice].floatValue * 100)):0;
    long total = sum + delivery;
    [self.sumLabel setText:[NSString stringWithFormat:@"$%.2lf",sum/100.]];
    [self.deliveryChargeLabel setText:[NSString stringWithFormat:@"$%.2lf",delivery/100.]];
    [self.totalSumLabel setText:[NSString stringWithFormat:@"$%.2lf",total/100.]];
    
}

- (IBAction)minusAction:(id)sender {
    self.count--;
}

- (IBAction)plusAction:(id)sender {
    self.count++;
}

- (IBAction)OrderTypeChanged:(UISegmentedControl *)sender {
    
    if(sender.selectedSegmentIndex == 1){
        [self setDeliveryType:FRDeliveryTypePickup animated:YES];
    }else{
        [self setDeliveryType:FRDeliveryTypeDelivery animated:YES];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqual: @"PaySegue"]){
        FRStripePaymentViewController *cntr = segue.destinationViewController;
        cntr.mealID = [self.meal uid];
        cntr.portions = self.count;
        cntr.type = self.deliveryType;
        
        
        long sum = (long)([self.meal price].floatValue *100)  * self.count;
        long delivery = (self.deliveryType == FRDeliveryTypeDelivery)?((long)([self.meal deliveryPrice].floatValue * 100)):0;
        long total = sum + delivery;

        
        cntr.totalPrice = total;
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    self.counterLabel.hidden = YES;
    [self setViewMovedUp:YES];
    sender.text = self.counterLabel.text;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    self.counterLabel.hidden = NO;
    textField.text = @"";
    [self setViewMovedUp:NO];
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

@end
