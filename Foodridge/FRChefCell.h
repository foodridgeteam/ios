//
//  FRChefCell.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRChefProfileModel.h"


@interface FRChefCell : UITableViewCell


- (void)setupCellWithChefModel:(FRChefProfileModel *)profile;

@end
