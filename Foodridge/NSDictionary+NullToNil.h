//
//  NSDictionary+NullToNil.h
//  MusketeerNew
//
//  Created by Brovko Roman on 11.11.14.
//  Copyright (c) 2014 Musketeer safety net LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NullToNil)

- (id)objectOrNilForKey:(id)aKey;

- (id)objectOrNilForKeypath:(NSString *)keyPath classObject:(Class)classObject;

- (id)objectOrNilForKey:(id)aKey classObject:(Class)classObject;

@end
