//
//  FROrder.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/27/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Realm/Realm.h>

@interface FROrder : RLMObject

@property NSInteger uid;
@property NSInteger status;
@property NSInteger deliveryType;
@property NSInteger mealId;

@property NSInteger chefId;
@property NSInteger clientId;

@property NSString *chefName;
@property NSString *clientName;
@property NSString *chefAvatarURL;
@property NSString *clientAvatarURL;

@property NSString *pickupStreet;
@property NSString *pickupCity;
@property NSString *pickupContry;

@property NSString *deliveryStreet;
@property NSString *deliveryCity;
@property NSString *deliveryContry;

@end
