//
//  FRGeneralProfileViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/7/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRGeneralProfileViewController.h"
#import "UIImage+Color.h"
#import "UIColor+FoodridgeColors.h"
#import "FRProfileModel.h"
#import "FRAuthorisationService.h"
#import <AFNetworking/UIButton+AFNetworking.h>
#import <libextobjc/extobjc.h>
#import "FRDeliveryInfoViewController.h"
#import <BlocksKit/UIImagePickerController+BlocksKit.h>
#import "FRMediaUploadsService.h"
#import "FRProgressHud.h"

@interface FRGeneralProfileViewController ()<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *avatarView;

@property (strong, nonatomic) IBOutlet UIButton *saveButton;

@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

@property (strong, nonatomic) IBOutlet UIView *bottomBorderView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *saveIndicator;


@property (strong, nonatomic) IBOutlet UIButton *avatarButton;
@property (strong, nonatomic) IBOutlet UILabel *deliveryInfoLabel;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;

@property(strong, nonatomic) FRProfileModel *model;
@property(strong, nonatomic) FRProfileModel *unsavedChanges;

@end

@implementation FRGeneralProfileViewController

-(void)setupTextFields{
    UIToolbar *tb =[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [tb setTintColor:[UIColor aquaColor]];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissKeyboard)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    tb.items = @[flex,item];
    self.phoneTextField.inputAccessoryView = tb;
}

- (void)dismissKeyboard{
    [self.view endEditing:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    UIView *headerView = self.tableView.tableHeaderView;
    CGRect newFrame = headerView.frame;
    newFrame.size.height = CGRectGetMaxY(self.bottomBorderView.frame);
    headerView.frame = newFrame;
    [self.tableView setTableHeaderView:headerView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setupTextFields];


    UIRefreshControl *refresh = [UIRefreshControl new];
    [refresh addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [refresh setTintColor:[UIColor aquaColor]];
    [refresh setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"STR_LOADING", nil)]];
    [self setRefreshControl:refresh];
    [self setupModel];
    [self buttonColorChecker];

}



-(void)setupModel{
    self.model = [[FRAuthorisationService service] currentUser];
    self.unsavedChanges = [FRProfileModel new];
}



- (void)setModel:(FRProfileModel *)model{
    
    _model = model;
    NSURL *url = [NSURL URLWithString:self.model.avatarURL];
    
    if(url){
        [self.avatarButton setBackgroundImageForState:UIControlStateNormal withURL:url placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    }
    self.deliveryInfoLabel.attributedText = [self.model.deliveryInfo stringRepresemtation];
    self.nameTextField.text = self.model.name;
    self.emailTextField.text = self.model.email;
    self.phoneTextField.text = self.model.phone;
    
}

- (void)refresh{
    @weakify(self)
    [[FRAuthorisationService service] getUserProfileWithCompletion:^(FRProfileModel *object, id rawObject, NSError *error) {
        @strongify(self)
        if(!error){
            self.model = object;
        }
        [self.refreshControl endRefreshing];
    }];
}

- (void)reloadData{
    
}

-(void) buttonColorChecker {
    
    if ([self.unsavedChanges dictionaryRepresentation] == nil) {
        
        [self.saveButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
    } else {
        
        [self.saveButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    }
}

-(void)setupUI{
//
    self.avatarButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarButton.layer.cornerRadius = self.avatarButton.bounds.size.width/2;
    self.avatarButton.layer.masksToBounds = YES;
    
    self.avatarView.layer.cornerRadius = self.avatarView.bounds.size.width/2;
    self.avatarView.layer.shadowRadius  = 4.f;
    self.avatarView.layer.shadowColor   = [UIColor blackColor].CGColor;
    self.avatarView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    self.avatarView.layer.shadowOpacity = 0.4f;
    self.avatarView.layer.masksToBounds = NO;
    [self.saveButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
    [self.deleteButton setBackgroundImage:[UIImage imageWithColor:[UIColor grayColor]] forState:UIControlStateNormal];
}

- (IBAction)setupDeliveryAction:(id)sender {
    
}

- (IBAction)setupImageAction:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_PICK_PROFILE_IMAGE", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];

    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_FROM_CAMERA", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self presentImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_FROM_GALERY", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self presentImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
                                
}

- (void)presentImagePickerWithSourceType:(UIImagePickerControllerSourceType)type{
    UIImagePickerController *cntr = [[UIImagePickerController alloc] init];
    cntr.sourceType = type;
    cntr.allowsEditing = YES;
    [cntr setBk_didCancelBlock:^(UIImagePickerController *picker) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }];
    @weakify(self);
    [cntr setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
        @strongify(self)
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        [self.avatarButton setBackgroundImage:chosenImage forState:UIControlStateNormal];
        [[FRMediaUploadsService new] uploadAvatar:chosenImage withCompletion:^(id object, id rawObject, NSError *error) {
            if(!error) {
                
                /*UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_AVATAR_UPLOADED", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
                
                [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                [self presentViewController:alert animated:YES completion:nil];*/
                FRProfileModel *profile = [FRProfileModel modelWitDictionary:rawObject];
                [[FRAuthorisationService service] saveUserProfile:profile withCompletion:^(id object, id rawObject, NSError *error) {

                }];
            }
        }];
        [picker dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self presentViewController:cntr animated:YES completion:nil];
}

- (IBAction)saveAction:(id)sender {
    
    [self.view endEditing:YES];
    if(self.unsavedChanges.dictionaryRepresentation){
        [self.saveIndicator startAnimating];
        @weakify(self)
        [[FRAuthorisationService service] saveUserProfile:self.unsavedChanges withCompletion:^(id object, id rawObject, NSError *error) {
            @strongify(self)
            [self.saveIndicator stopAnimating];
            if(!error){
                self.model = object;
                self.unsavedChanges = [FRProfileModel new];
                [self buttonColorChecker];
                [FRProgressHUD showMessageHudWithText:@"Changes saved" inView:self.navigationController.view delay:2.5f];
            }
        }];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"ShowDeliveryInfo"]){
        FRDeliveryInfoViewController *cntr = segue.destinationViewController;
        cntr.model = self.unsavedChanges.deliveryInfo?:self.model.deliveryInfo;
        @weakify(self);
        [cntr setOnSave:^(FRDeliveryInfoModel *model) {
            @strongify(self);
            self.unsavedChanges.deliveryInfo = model;
            self.deliveryInfoLabel.attributedText = [model stringRepresemtation];
            [self buttonColorChecker];
        }];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(textField == self.nameTextField){
        self.unsavedChanges.name = str;
    }else if(textField == self.phoneTextField){
        self.unsavedChanges.phone = str;
    }
    [self buttonColorChecker];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self buttonColorChecker];
    return YES;
}



@end
