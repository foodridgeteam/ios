//
//  NetworkService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>


static const NSString *baseURL = @"https://www.foodridge.com/api";

typedef void (^FRResponceBlock)(id object, id rawObject , NSError *error);

@class AFHTTPSessionManager;
@interface FRNetworkService : NSObject

+ (NSURLSessionTask *)requestWithMethod:(NSString *)method
                             requestAddress:(NSString *)address
                                 parameters:(NSDictionary *)params
                                 completion:(FRResponceBlock)completion;

+ (AFHTTPSessionManager *)sessionManager;

@end
