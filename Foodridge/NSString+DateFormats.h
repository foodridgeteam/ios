//
//  NSString+DateFormats.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DateFormats)

+ (NSString *)localHoursMinutesStringFromDate:(NSDate *)date;
+ (NSString *)localHoursMinutesStringWithTimeZoneFromDate:(NSDate *)date;

+ (NSString *)dayTitleForDayAtIndex:(NSInteger)index;

+ (NSString *)HHmmFromDate:(NSDate *)date;

- (NSDate *)asHHmmDate;

+ (NSAttributedString *)orderCreationTimeFromDate:(NSDate *)date;

@end
