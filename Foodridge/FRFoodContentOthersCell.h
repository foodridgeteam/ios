//
//  FRFoodContentOthersCell.h
//  Foodridge
//
//  Created by Dev on 5/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FRMealExtendedModel.h"

@interface FRFoodContentOthersCell : UITableViewCell

- (void)setFoodContentCellWithFlag:(NSString *)flag mealModel:(FRMealExtendedModel *)meal;

@end
