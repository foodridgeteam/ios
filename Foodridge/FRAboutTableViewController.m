//
//  FRAboutTableViewController.m
//  Foodridge
//
//  Created by NekitBOOK on 22.04.16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRAboutTableViewController.h"

@interface FRAboutTableViewController ()

@property (strong, nonatomic) IBOutlet UILabel *versionSectionLabel;

@end

@implementation FRAboutTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    self.versionSectionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"STR_VERSION", nil), build];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(12, 20, self.view.frame.size.width-12, 20);
    label.textColor = [UIColor darkGrayColor];
    label.font = [UIFont fontWithName:@"OpenSans-Bold" size:17];
    label.backgroundColor = [UIColor clearColor];
    
    if (section == 0)
    {
        label.text = NSLocalizedString(@"STR_ABOUT_FOODRIDGE_HEADER", nil);
        
    }
    
    else if (section == 1)
    {
        label.text = NSLocalizedString(@"STR_CHEFS_HEADER", nil);
    }
    
    else if (section == 2)
    {
        label.text = NSLocalizedString(@"STR_DINNERS_HEADER", nil);
    }
    
    else if (section == 3)
    {
        label.text = NSLocalizedString(@"STR_VERSION_HEADER", nil);
        
    }
    
    UIView *view = [[UIView alloc] initWithFrame: CGRectMake(0, 8, self.view.frame.size.width, 50)];
    [view addSubview:label];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    // remove bottom extra 20px space.
    return 0.001;
}

/* - (NSString *)tableView:(UITableView *)table titleForHeaderInSection:(NSInteger)section {
    
       return @"Header";
} */

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
