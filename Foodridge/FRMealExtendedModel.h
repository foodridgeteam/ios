//
//  FRMealExtendedModel.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/15/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMealModel.h"
#import "FRDeliveryInfoModel.h"

@interface FRMealExtendedModel : FRViewModel<FRMealModel>

@property(nonatomic, copy) NSNumber *portionsSold;
@property(nonatomic, copy) NSNumber *averageRating;

+ (NSArray *)flagNames;

@end
