//
//  FRWeekSchedule.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/11/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRWeekScheduleModel.h"
#import "NSString+DateFormats.h"
#import "UIFont+FoodridgeFonts.h"
#import "UIColor+FoodridgeColors.h"

#import "NSDictionary+NullToNil.h"
@implementation FRWeekScheduleModel

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary{
    
    FRWeekScheduleModel *model = [FRWeekScheduleModel new];
    
    model.monOpenTime = [[dictionary objectOrNilForKeypath:@"mon.start" classObject:[NSString class]] asHHmmDate];
    model.monCloseTime = [[dictionary objectOrNilForKeypath:@"mon.end" classObject:[NSString class]] asHHmmDate];
    
    model.tueOpenTime = [[dictionary objectOrNilForKeypath:@"tue.start" classObject:[NSString class]] asHHmmDate];
    model.tueCloseTime = [[dictionary objectOrNilForKeypath:@"tue.end" classObject:[NSString class]] asHHmmDate];
    
    model.wedOpenTime = [[dictionary objectOrNilForKeypath:@"wed.start" classObject:[NSString class]] asHHmmDate];
    model.wedCloseTime = [[dictionary objectOrNilForKeypath:@"wed.end" classObject:[NSString class]] asHHmmDate];
    
    model.thuOpenTime = [[dictionary objectOrNilForKeypath:@"thu.start" classObject:[NSString class]] asHHmmDate];
    model.thuCloseTime = [[dictionary objectOrNilForKeypath:@"thu.end" classObject:[NSString class]] asHHmmDate];
    
    model.friOpenTime = [[dictionary objectOrNilForKeypath:@"fri.start" classObject:[NSString class]] asHHmmDate];
    model.friCloseTime = [[dictionary objectOrNilForKeypath:@"fri.end" classObject:[NSString class]] asHHmmDate];
    
    model.satOpenTime = [[dictionary objectOrNilForKeypath:@"sat.start" classObject:[NSString class]] asHHmmDate];
    model.satCloseTime = [[dictionary objectOrNilForKeypath:@"sat.end" classObject:[NSString class]] asHHmmDate];
    
    model.sunOpenTime = [[dictionary objectOrNilForKeypath:@"sun.start" classObject:[NSString class]] asHHmmDate];
    model.sunCloseTime = [[dictionary objectOrNilForKeypath:@"sun.end" classObject:[NSString class]] asHHmmDate];
    
    return model;
}

- (NSDictionary *)dictionaryRepresentation{
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    if(self.monOpenTime && self.monCloseTime){
        dict[@"mon"] = @{@"start"    :[NSString HHmmFromDate:self.monOpenTime],
                         @"end"      :[NSString HHmmFromDate:self.monCloseTime]};
    }else{
        dict[@"mon"] = [NSNull null];
    }
    if(self.tueOpenTime && self.tueCloseTime){
        dict[@"tue"] = @{@"start"    :[NSString HHmmFromDate:self.tueOpenTime],
                         @"end"      :[NSString HHmmFromDate:self.tueCloseTime]};
    }else{
        dict[@"tue"] = [NSNull null];
    }
    if(self.wedOpenTime && self.wedCloseTime){
        dict[@"wed"] = @{@"start"    :[NSString HHmmFromDate:self.wedOpenTime],
                         @"end"      :[NSString HHmmFromDate:self.wedCloseTime]};
    }else{
        dict[@"wed"] = [NSNull null];
    }
    if(self.thuOpenTime && self.thuCloseTime){
        dict[@"thu"] = @{@"start"    :[NSString HHmmFromDate:self.thuOpenTime],
                         @"end"      :[NSString HHmmFromDate:self.thuCloseTime]};
    }else{
        dict[@"thu"] = [NSNull null];
    }
    if(self.friOpenTime && self.friCloseTime){
        dict[@"fri"] = @{@"start"    :[NSString HHmmFromDate:self.friOpenTime],
                         @"end"      :[NSString HHmmFromDate:self.friCloseTime]};
    }else{
        dict[@"fri"] = [NSNull null];
    }
    if(self.satOpenTime && self.satCloseTime){
        dict[@"sat"] = @{@"start"    :[NSString HHmmFromDate:self.satOpenTime],
                         @"end"      :[NSString HHmmFromDate:self.satCloseTime]};
    }else{
        dict[@"sat"] = [NSNull null];
    }
    if(self.sunOpenTime && self.sunCloseTime){
        dict[@"sun"] = @{@"start"    :[NSString HHmmFromDate:self.sunOpenTime],
                         @"end"      :[NSString HHmmFromDate:self.sunCloseTime]};
    }else{
        dict[@"sun"] = [NSNull null];
    }

    
    return dict;
}

- (NSDate *)openDateForDayAtIndex:(NSInteger)day{
    
    NSDate *date;
    
    switch (day%7) {
        case 0:
            date = self.monOpenTime;
            break;
        case 1:
            date = self.tueOpenTime;
            break;
        case 2:
            date = self.wedOpenTime;
            break;
        case 3:
            date = self.thuOpenTime;
            break;
        case 4:
            date = self.friOpenTime;
            break;
        case 5:
            date = self.satOpenTime;
            break;
        case 6:
            date = self.sunOpenTime;
            break;
        default:
            break;
    }
    
    return date;
}

- (NSDate *)closeDateForDayAtIndex:(NSInteger)day{
    
    NSDate *date;
    switch (day%7) {
        case 0:
            date = self.monCloseTime;
            break;
        case 1:
            date = self.tueCloseTime;
            break;
        case 2:
            date = self.wedCloseTime;
            break;
        case 3:
            date = self.thuCloseTime;
            break;
        case 4:
            date = self.friCloseTime;
            break;
        case 5:
            date = self.satCloseTime;
            break;
        case 6:
            date = self.sunCloseTime;
            break;
        default:
            break;
    }

    return date;
}

- (void)setOpenDate:(NSDate *)date forDayAtIndex:(NSInteger)day{
    switch (day%7) {
        case 0:
            self.monOpenTime = date;
            break;
        case 1:
            self.tueOpenTime = date;
            break;
        case 2:
            self.wedOpenTime = date;
            break;
        case 3:
            self.thuOpenTime = date;
            break;
        case 4:
            self.friOpenTime = date;
            break;
        case 5:
            self.satOpenTime = date;
            break;
        case 6:
            self.sunOpenTime = date;
            break;
        default:
            break;
    }
}


- (void)setCloseDate:(NSDate *)date forDayAtIndex:(NSInteger)day{
    switch (day%7) {
        case 0:
            self.monCloseTime = date;
            break;
        case 1:
            self.tueCloseTime = date;
            break;
        case 2:
            self.wedCloseTime = date;
            break;
        case 3:
            self.thuCloseTime = date;
            break;
        case 4:
            self.friCloseTime = date;
            break;
        case 5:
            self.satCloseTime = date;
            break;
        case 6:
            self.sunCloseTime = date;
            break;
        default:
            break;
    }
}

- (NSAttributedString *)intervalStringForDayAtIndex:(NSInteger)index{
    NSDate *openDate = [self openDateForDayAtIndex:index];
    NSDate *closeDate = [self closeDateForDayAtIndex:index];
    
    if(openDate && closeDate){
        NSString *str = [NSString stringWithFormat:@"%@ - %@",[NSString localHoursMinutesStringWithTimeZoneFromDate:openDate ], [NSString localHoursMinutesStringWithTimeZoneFromDate:closeDate]];
        return [[NSAttributedString alloc] initWithString:str];
    }else{
        NSString *str = [NSLocalizedString(@"STR_CLOSED", nil) capitalizedString];
        return [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName:[UIFont openSansBoldFontWithSize:14], NSForegroundColorAttributeName:[UIColor alertRedColor]}];
    }
    
}

@end
