//
//  ChefMenuTableViewCell.m
//  Foodridge
//
//  Created by NekitBOOK on 25.04.16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <libextobjc/extobjc.h>
#import "ChefMenuTableViewCell.h"
#import "UIColor+FoodridgeColors.h"
#import "UIImage+Color.h"
#import "FRAuthorisationService.h"

@implementation ChefMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.orderNowButton.layer.cornerRadius = 3;
    self.orderNowButton.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setupCellWithMealModel:(FRMealExtendedModel *)meal {

    self.price.price = meal.price;
    self.price.textColor = [UIColor blackColor];
    self.name.text = meal.title;
    
    
    if (meal.imageURLs.count > 0) {
        NSURL *imageURL = [NSURL URLWithString:meal.imageURLs.firstObject];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.photo.image = [UIImage imageWithData:imageData];
            });
        });

    } else {
        self.photo.image = [UIImage imageNamed:@"dish_empty"];
    }
    
    
    self.rate = [meal.averageRating integerValue];
    self.rateStars.image = [UIImage imageNamed:[NSString stringWithFormat:@"rate%ld",(long)self.rate]];
    
    if (meal.servingsLeft.integerValue > 0){
        [self.orderNowButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
        [self.orderNowButton setTitle:NSLocalizedString(@"STR_ORDER_NOW", nil) forState:UIControlStateNormal];
        
    }else{
        
        [self.orderNowButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
        [self.orderNowButton setTitle:NSLocalizedString(@"STR_SOLD_OUT", nil) forState:UIControlStateNormal];
    }
    
    self.orderNowButton.enabled = meal.chefID.integerValue == [FRAuthorisationService currentUID] ? NO : YES;
}

-(IBAction)orderNowButtonAction:(id)sender {
  [self.delegate orderNowButtonClicked:self];
}



@end
