//
//  FRChefControl.h
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRRateControl.h"

@interface FRChefControl : FRRateControl

@property (nonatomic, assign) BOOL useWhiteChefHat;

@end
