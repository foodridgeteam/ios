//
//  ChefMenuTableViewCell.h
//  Foodridge
//
//  Created by NekitBOOK on 25.04.16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRMealExtendedModel.h"
#import "FRPriceControl.h"

@class ChefMenuTableViewCell;
@protocol FRChefMenuCellDelegate <NSObject>

- (void)orderNowButtonClicked:(ChefMenuTableViewCell *)cell;

@end

@interface ChefMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) id<FRChefMenuCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet FRPriceControl *price;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UIButton *orderNowButton;
@property (weak, nonatomic) IBOutlet UIImageView *rateStars;
@property (assign, nonatomic) NSInteger rate;


- (void)setupCellWithMealModel:(FRMealExtendedModel *)meal;
@end
