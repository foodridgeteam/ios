//
//  FRTermsService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRTermsService.h"
#import "FRTermsViewControler.h"

static BOOL presented = NO;

@implementation FRTermsService

- (void)showTermsFromViewController:(UIViewController *)vc
                        shouldAgree:(BOOL)shouldAgree
                          onDismiss:(void (^)(BOOL))onDismiss {
    
    if(presented){
        return;
    }
    
    presented = YES;
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Terms" bundle:nil];
    UINavigationController *termsNav = [sb instantiateInitialViewController];
    FRTermsViewControler *termsVC = [[termsNav viewControllers] lastObject];
    termsVC.showAgreeButton = shouldAgree;
    [termsVC setOnDismiss:^(BOOL aggreed) {
        presented = NO;
        if(onDismiss){
            onDismiss(aggreed);
        }
    }];
    [vc presentViewController:termsNav animated:YES completion:nil];
}

@end
