//
//  FRMediaUploadsService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMediaUploadsService.h"
#import "FRNetworkService+MediaUploads.m"
#import "FRAuthorisationService.h"
#import "FRProfileModel.h"
#import "UUID.h"


@implementation FRMediaUploadsService

- (void)uploadAvatar:(UIImage *)img withCompletion:(FRResponceBlock)completion{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSString *uuid = [UUID uuid];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *directory = [paths objectAtIndex:0];
        NSString *filePath = [[directory stringByAppendingPathComponent:uuid] stringByAppendingPathExtension:@"png"];
        [UIImagePNGRepresentation(img) writeToFile:filePath atomically:YES];
        
        [FRNetworkService uploadMedia:filePath type:FRMediaTypeAvatar uid:0 completion:^(id object, id rawObject, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if([object isKindOfClass:[NSString class]]){
                    [FRAuthorisationService service].currentUser.avatarURL = object;
                    [[FRAuthorisationService service] saveProfileLocally];
                }
                completion(object,rawObject,error);
            });
        }];
    });
}

- (void)uploadMealImage:(UIImage *)img forMealWithID:(NSInteger)mealID withCompletion:(FRResponceBlock)completion {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *uuid = [UUID uuid];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *directory = [paths objectAtIndex:0];
        NSString *filePath = [[directory stringByAppendingPathComponent:uuid] stringByAppendingPathExtension:@"png"];
        [UIImagePNGRepresentation(img) writeToFile:filePath atomically:YES];
        
        [FRNetworkService uploadMedia:filePath type:FRMediaTypeMealPhoto uid:mealID completion:^(id object, id rawObject, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(object,rawObject,error);
            });
        }];
    });
}

- (void)deleteImageWithName:(NSString *)imageName forMealWithID:(NSInteger)mealID withCompletion:(FRResponceBlock)completion {
    [FRNetworkService deleteMediaWithName:imageName forMealWithID:mealID completion:completion];
}

@end
