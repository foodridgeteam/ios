//
//  FRFoodContentOthersCell.m
//  Foodridge
//
//  Created by Dev on 5/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRFoodContentOthersCell.h"

@interface FRFoodContentOthersCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation FRFoodContentOthersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setFoodContentCellWithFlag:(NSString *)flag mealModel:(FRMealExtendedModel *)meal{
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@:", [flag uppercaseString]];
    
    self.contentLabel.text = meal.otherFlags;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
}

@end
