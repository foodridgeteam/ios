//
//  FRMessageView.m
//  Foodridge
//
//  Created by Victor Miroshnichenko on 5/3/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMessageView.h"

@interface FRMessageView ()

@property (strong, nonatomic) IBOutlet UITextField *messageTextField;
@property (strong, nonatomic) IBOutlet UIButton *pickUpNowButton;
@property (strong, nonatomic) IBOutlet UIButton *deliveryOnWayButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *messageViewBottomConstraint;
@property (strong, nonatomic) IBOutlet UIView *messageView;

@end

@implementation FRMessageView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    self.messageView.layer.cornerRadius = 3;
    self.messageView.clipsToBounds = YES;
    [self.messageTextField becomeFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.messageViewBottomConstraint.constant = keyboardSize.height + 10;
}

- (IBAction)pickupNowAction:(id)sender {
    self.messageTextField.text = NSLocalizedString(@"STR_PICKUP_NOW", nil);
}

- (IBAction)deliveryOnWayAction:(id)sender {
    self.messageTextField.text = NSLocalizedString(@"STR_DELIVERY_ON_WAY", nil);
}

- (void)setChef:(BOOL)chef{
    _chef = chef;
    self.deliveryOnWayButton.superview.hidden = !chef;
    self.pickUpNowButton.superview.hidden = !chef;
}

- (IBAction)cancelAction:(id)sender {
    if (self.onDone) {
        self.onDone(YES, nil);
    }
}

- (IBAction)sendAction:(id)sender {
    if (self.onDone) {
        self.onDone(NO, self.messageTextField.text);
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
