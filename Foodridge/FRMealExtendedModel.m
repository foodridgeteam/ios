//
//  FRMealExtendedModel.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/15/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMealExtendedModel.h"
#import "FRStaticDataService.h"
#import "FRDateHelper.h"
#import "FRCuisineModel.h"
#import "FRIngridientModel.h"


@implementation FRMealExtendedModel

@synthesize uid;
@synthesize title;
@synthesize mealDescription;
@synthesize imageURLs;
@synthesize price;
@synthesize deliveryType;
@synthesize discountsActive;
@synthesize servingsLeft;
@synthesize reviewsCount;
@synthesize chefTitle;
@synthesize chefImageURL;
@synthesize chefLastVisit;
@synthesize chefRating;
@synthesize cuisines;
@synthesize ingridients;
@synthesize spiceRate;
@synthesize otherFlags;
@synthesize deliveryPrice;
@synthesize flags;
@synthesize listed;
@synthesize chefID;
@synthesize deliveryInfo;

+(NSArray *)flagNames{
    
    static NSArray *flags;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        flags =   @[@"breakfast",
                    @"lunch",
                    @"dinner",
                    @"bakery",
                    @"vegetarian",
                    @"egg",
                    @"glutenFree",
                    @"tracesNuts",
                    @"garlic",
                    @"ginger",
                    @"other"];
    });
    return flags;
}

- (NSDictionary *)dictionaryRepresentation{
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    
    if(self.title){
        params[@"name"] = self.title;
    }
    if(self.mealDescription){
        params[@"description"] = self.mealDescription;
    }
    
    if(self.cuisines){
        params[@"cuisines"] = [self.cuisines valueForKey:@"uid"];
    }
    
    if(self.ingridients){
        params[@"ingredients"] = [self.ingridients valueForKey:@"uid"];
    }
    
    if (self.flags) {
        params[@"flags"] = self.flags;
    }
    
    if(self.otherFlags){
        params[@"other"] = self.otherFlags.length ? self.otherFlags : nil;
    }
    
    if (self.deliveryType) {
        NSInteger type;
        switch (self.deliveryType.integerValue) {
            case FRDeliveryTypePickupAndDelivery:
                type = 1;
                break;
            case FRDeliveryTypePickup:
                type = 2;
                break;
            case FRDeliveryTypeDelivery:
                type = 3;
                break;
                
            default:
                break;
        }
        params[@"deliveryType"] = @(type);
    }
    if(self.price){
        params[@"portionPrice"] = self.price;
    }
    if(self.deliveryPrice){
        params[@"deliveryPrice"] = self.deliveryPrice;
    }
    
    if(self.servingsLeft){
        params[@"portionsAvailable"] = self.servingsLeft;
    }
    
    if (self.spiceRate) {
        params[@"spicyRank"] = self.spiceRate;
    }
    
    if (self.listed) {
        params[@"listed"] = self.listed;
    }
    
    if([[params allKeys] count] == 0){
        return nil;
    }
    
    if(self.uid){
        params[@"mealId"] = self.uid;
    }
    if(!params[@"deliveryPrice"]){
        params[@"deliveryPrice"] = @0;
    }
    
    return params;
}

- (void)setFlag:(FRMealFlag)flag value:(BOOL)value{
    if(!self.flags){
        self.flags = [NSDictionary new];
    }
    NSMutableDictionary *d = [self.flags mutableCopy];
    
    [d setObject:@(value) forKey:[self.class flagNames][flag-1]];
    
    self.flags = d;
}

- (BOOL)flagValue:(FRMealFlag)flag{
    return [self.flags[[self.class flagNames][flag-1]] boolValue];
}

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary{
    FRMealExtendedModel *mealModel = [FRMealExtendedModel new];
    
    mealModel.uid = [dictionary objectOrNilForKey:@"id" classObject:[NSNumber class]];
    mealModel.title = [dictionary objectOrNilForKey:@"name" classObject:[NSString class]];
    mealModel.mealDescription = [dictionary objectOrNilForKey:@"description" classObject:[NSString class]];
    mealModel.imageURLs = [dictionary objectOrNilForKey:@"photo" classObject:[NSArray class]];
    mealModel.price = [dictionary objectOrNilForKey:@"portionPrice" classObject:[NSNumber class]];
    mealModel.deliveryType = [dictionary objectOrNilForKey:@"deliveryType" classObject:[NSNumber class]];
    mealModel.servingsLeft = [dictionary objectOrNilForKey:@"portionsAvailable" classObject:[NSNumber class]];
    mealModel.reviewsCount = [dictionary objectOrNilForKey:@"votesCount" classObject:[NSNumber class]];
    mealModel.chefTitle = [dictionary objectOrNilForKeypath:@"chef.name" classObject:[NSString class]];
    mealModel.chefID = [dictionary objectOrNilForKeypath:@"chef.id" classObject:[NSNumber class]];
    mealModel.chefImageURL = [dictionary objectOrNilForKeypath:@"chef.avatar" classObject:[NSString class]];
    mealModel.chefLastVisit = [FRDateHelper dateFromString:[dictionary objectOrNilForKeypath:@"chef.lastVisit" classObject:[NSString class]]];
    mealModel.chefRating = [dictionary objectOrNilForKeypath:@"chef.rating" classObject:[NSNumber class]];
    mealModel.cuisines = [FRCuisineModel getCuisinesModelsFromCuisinesIDsArray:[dictionary objectOrNilForKey:@"cuisines" classObject:[NSArray class]]];
    mealModel.ingridients = [FRIngridientModel getIngredientsModelsFromIngredientsIDsArray:[dictionary objectOrNilForKey:@"ingredients" classObject:[NSArray class]]];
    mealModel.spiceRate = [dictionary objectOrNilForKey:@"spicyRank" classObject:[NSNumber class]];
    mealModel.otherFlags = [dictionary objectOrNilForKey:@"other" classObject:[NSString class]];
    mealModel.flags = [dictionary objectOrNilForKeypath:@"flags" classObject:[NSDictionary class]];
    mealModel.deliveryPrice = [dictionary objectOrNilForKey:@"deliveryPrice" classObject:[NSNumber class]];
    mealModel.portionsSold = [dictionary objectOrNilForKey:@"portionsSold" classObject:[NSNumber class]];
    mealModel.averageRating = [dictionary objectOrNilForKey:@"rating" classObject:[NSNumber class]];
    mealModel.listed = [dictionary objectOrNilForKey:@"listed" classObject:[NSNumber class]];

    NSDictionary *deliveryInfoDict = [dictionary objectOrNilForKeypath:@"chef.deliveryInfo" classObject:[NSDictionary class]];
    mealModel.deliveryInfo = [FRDeliveryInfoModel modelWitDictionary:deliveryInfoDict];
    
    return mealModel;
}


@end
