//
//  FRFacebookLoginService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import UIKit;
@import Foundation;

typedef void (^FRFacebookLoginBlock)(NSString *userID, NSString *token, NSError *error);

@interface FRFacebookLoginService : NSObject

- (void)loginFromViewController:(UIViewController *)cntr completion:(FRFacebookLoginBlock)completion;

@end
