//
//  FRCuisineModel.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"

@interface FRCuisineModel : FRViewModel

@property (nonatomic, copy) NSNumber *uid;
@property (nonatomic, copy) NSString *name;

+ (NSString *)getCuisisnesStringFromArray:(NSArray *)cuisines;
+ (NSArray *)getCuisinesModelsFromCuisinesIDsArray:(NSArray *)cuisinesIDsArray;

@end
