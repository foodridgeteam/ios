//
//  FRSpiceControl.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRSpiceControl.h"

@implementation FRSpiceControl


-(UIImage *)fillImage{
    return self.spicyControlForMealInfo ? [UIImage imageNamed:@"spicy_yes"] : [UIImage imageNamed:@"spicy_full"];
}

- (UIImage *)emptyImage{
    return self.spicyControlForMealInfo ? [UIImage imageNamed:@"spicy_no"] : [UIImage imageNamed:@"spicy_empty"];
}


@end
