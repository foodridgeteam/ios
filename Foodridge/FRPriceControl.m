//
//  FRPriceControl.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRPriceControl.h"
#import "UIFont+FoodridgeFonts.h"

@implementation FRPriceControl

- (void)drawRect:(CGRect)rect {
    
    //CGRect rect;
    
    double cc;
    double dc;
    dc = modf([self.price doubleValue],&cc);
    
    
    NSString *dollarsPriceText = [NSString stringWithFormat:@"$%.0f",cc];
    NSString *centsPriceText = [NSString stringWithFormat:@"%.0f",dc * 100];
    if (centsPriceText.length == 1) {
        centsPriceText = [centsPriceText stringByAppendingString:@"0"];
    }

    
    UIFont *dollarsPriceFont = [UIFont openSansBoldFontWithSize:20];
    UIFont *centsPriceFont = [UIFont openSansFontWithSize:14];
    
    CGSize dollarsStringSize = [dollarsPriceText sizeWithAttributes:@{NSFontAttributeName:dollarsPriceFont}];
    CGSize centsStringSize = [centsPriceText sizeWithAttributes:@{NSFontAttributeName:centsPriceFont}];
    
    if(!_textColor) {
        _textColor = [UIColor whiteColor];
    }
    
    NSDictionary *dollarsPriceAttributes = @{NSFontAttributeName : dollarsPriceFont, NSForegroundColorAttributeName : _textColor};
    NSDictionary *centsPriceAttributes   = @{NSFontAttributeName : centsPriceFont,   NSForegroundColorAttributeName : _textColor};
    
    [dollarsPriceText drawInRect:CGRectMake(0, 0, dollarsStringSize.width, dollarsStringSize.height) withAttributes:dollarsPriceAttributes];
    [centsPriceText drawInRect:CGRectMake(dollarsStringSize.width, 0, centsStringSize.width, centsStringSize.height) withAttributes:centsPriceAttributes];
}

//- (NSString *)getTextForDollarsLabel {
//    return [NSString stringWithFormat:@"$%.f", floor([self.price floatValue])];
//}
//
//- (NSString *)getTextForCentsLabel {
//    NSArray *stringComponents = [[self.price stringValue] componentsSeparatedByString:@"."];
//    NSString *str = [stringComponents lastObject];
//    if (str.length == 1) {
//        str = [str stringByAppendingString:@"0"];
//    }
//
//    return stringComponents.count > 1 ? str : @"";
//}

- (void)setPrice:(NSNumber *)price {
    _price = price;
    [self setNeedsDisplay];
}

@end
