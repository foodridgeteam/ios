//
//  FRProfileService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/4/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRAuthorisationService.h"
#import "FRNetworkService+Auth.h"
#import "FRNetworkService+Profile.h"

#import "NSString+MD5.h"
#import "UUID.h"
#import <SSKeychain/SSKeychain.h>
#import "FRProfileModel.h"
#import "FRProfile.h"
#import "NSDictionary+NullToNil.h"
#import "FRRealmService.h"
#import "FRMainScreenService.h"
#import "FRNotificationsService.h"

#import "FRChefService.h"

static NSString *accessToken = nil;
static NSInteger currentUid = 0;

@implementation FRAuthorisationService

+ (FRAuthorisationService *)service{
    
    static FRAuthorisationService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [FRAuthorisationService new];
    });
    return service;
}
- (id)init{
    if(self = [super init]){
        NSNumber *uid = [[NSUserDefaults standardUserDefaults] objectForKey:@"kFRUserID"];
        if(uid){
            FRProfile *profile = [FRProfile objectForPrimaryKey:uid];
            if(profile){
                FRProfileModel *model = [FRProfileModel new];
                model.uid = @(profile.uid);
                model.avatarURL = profile.avatarURL;
                model.name = profile.name;
                model.email = profile.email;
                model.deliveryInfo = [FRDeliveryInfoModel new];
                model.deliveryInfo.country = profile.deliveryCountry;
                model.deliveryInfo.city = profile.deliveryCity;
                model.deliveryInfo.street = profile.deliveryStreet;
                _currentUser = model;
                currentUid = profile.uid;
            }
        }
    }
    return self;
}

+ (void)setAccessToken:(NSString *)token {
    if ([accessToken isEqual:token]) {
        return;
    }
    
    accessToken = token;
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"kFRUserAccessToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+ (NSInteger)currentUID {
    if (currentUid == 0) {
        currentUid = [[NSUserDefaults standardUserDefaults] integerForKey:@"kFRUserID"];
        if(currentUid == 0){
            currentUid = NSNotFound;
        }
    }
    return currentUid;
}


+ (NSString *)accessToken {
    if (!accessToken) {
        accessToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"kFRUserAccessToken"];
    }
    
    return accessToken;
}


+ (NSString *)deviceUUID{
    
    static NSString *deviceID = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        deviceID = [SSKeychain passwordForService:@"Foodridge" account:@"default"];
        if(!deviceID){
            deviceID = [UUID uuid];
            [SSKeychain setPassword:deviceID forService:@"Foodridge" account:@"default"];
        }
    });
    
    return deviceID;
}

- (BOOL)authorized{
    return [self.class accessToken] != nil;
}

- (NSURLSessionTask *)authWithEmail:(NSString *)email
             password:(NSString *)password
           completion:(FRResponceBlock)completion{
    
    return [FRNetworkService authWithEmail:email password:[password MD5String] deviceId:[self.class deviceUUID] completion:[self processAuthWithCompletion:completion requireToken:YES]];
}


- (NSURLSessionTask *)registerWithEmail:(NSString *)email
                 username:(NSString *)username
                               password:(NSString *)password
                             completion:(FRResponceBlock)completion{
    
    return [FRNetworkService registerWithEmail:email
                                      username:username
                                      password:[password MD5String]
                                      deviceId:[self.class deviceUUID]
                                    completion:[self processAuthWithCompletion:completion requireToken:YES]];
}


- (NSURLSessionTask *)authWithFacebookToken:(NSString *)token userID:(NSString *)uid completion:(FRResponceBlock)completion{
  
    return [FRNetworkService authWithFacebookToken:token
                                               uid:uid
                                          deviceId:[self.class deviceUUID]
                                        completion:[self processAuthWithCompletion:completion requireToken:YES]];
}


- (NSURLSessionTask *)authWithGooglePlusToken:(NSString *)token completion:(FRResponceBlock)completion{
  
    return [FRNetworkService authWithGooglePlusToken:token
                                          deviceId:[self.class deviceUUID]
                                        completion:[self processAuthWithCompletion:completion requireToken:YES]];
}


- (void)logout{
    [FRNetworkService logout];
    
    [self.class setAccessToken:nil];
    
    _currentUser = nil;
    currentUid = NSNotFound;
    
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"kFRUserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (FRResponceBlock)processAuthWithCompletion:(FRResponceBlock)completion
                                requireToken:(BOOL)requireToken{
    
    FRResponceBlock block = ^(id object, id rawObject, NSError *error_) {
        
        NSError *error = error_;
        
        if(!error){
            
            NSString *token = [rawObject objectOrNilForKey:@"token" classObject:[NSString class]];
            NSDictionary *profileDict = rawObject;
            if(token){
                profileDict = [rawObject objectOrNilForKey:@"userProfile" classObject:[NSDictionary class]];
            }
            if((!requireToken || token) && profileDict){
                
                if(token){
                    [self.class setAccessToken:token];
                    [[FRNotificationsService new] registerForNotifications];
                    [[FRChefService service] getChefProfileWithCompletion:^(id object, id rawObject, NSError *error) {
                    }];
                }
                
                FRProfileModel *profile = [self saveUserProfileWithDictionary:profileDict];
                if(profile){
                    completion(profile, rawObject, error);
                    return;
                } else {
                    error = [NSError errorWithDomain:@"Can't get profile" code:500 userInfo:nil];
                    
                }
            } else {
                error = [NSError errorWithDomain:@"Can't get profile" code:500 userInfo:nil];
            }
        }else{
        }
        
        completion(object,rawObject,error);
    };
    return block;
}

- (NSURLSessionTask *)getUserProfileWithCompletion:(FRResponceBlock)completion{
    
    return [FRNetworkService myProfileWithCompletion:[self processAuthWithCompletion:completion requireToken:NO]];
}

- (NSURLSessionTask *)saveUserProfile:(FRProfileModel *)profile
                       withCompletion:(FRResponceBlock)completion{
    
    return [FRNetworkService updateProfile:profile completion:[self processAuthWithCompletion:completion requireToken:NO]];
}

- (FRProfileModel *)saveUserProfileWithDictionary:(NSDictionary *)profileDict{
    
    FRProfileModel *profile = [FRProfileModel modelWitDictionary:profileDict];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:profile.uid forKey:@"kFRUserID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    currentUid = [profile.uid integerValue];
    
    if(profile.uid
       && profile.email
       && profile.name){
        
      
        _currentUser = profile;
        [[NSNotificationCenter defaultCenter] postNotificationName:FRCurrentProfileUpdatedNotificationName object:nil];

        currentUid = [profile.uid integerValue];
        [self saveProfileLocally];
        return profile;
    }
    return nil;
    
}

-(void)saveProfileLocally{
    
    FRProfile *realmProfile = [[FRProfile alloc] init];
    
    realmProfile.name = _currentUser.name;
    realmProfile.email = _currentUser.email;
    realmProfile.phone = _currentUser.phone;
    realmProfile.uid = [_currentUser.uid integerValue];
    realmProfile.avatarURL = _currentUser.avatarURL;
    
    realmProfile.deliveryCity = _currentUser.deliveryInfo.city;
    realmProfile.deliveryStreet = _currentUser.deliveryInfo.street;
    realmProfile.deliveryCountry = _currentUser.deliveryInfo.country;
    
    [[FRRealmService new] writeObject:realmProfile withCompletion:nil];
    

}


- (NSURLSessionTask *)forgotPassword:(NSString *)email withCompletion:(FRResponceBlock)completion{
    return [FRNetworkService forgotPasswordWithEmail:email completion:completion];
}

- (void)aggreeWithTermsWithCompletion:(FRResponceBlock)completion{
    [FRNetworkService termsAggreedCompletion:completion];
}
@end
