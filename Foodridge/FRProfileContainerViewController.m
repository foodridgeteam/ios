//
//  FRProfileContainerViewController.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/7/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRProfileContainerViewController.h"

@interface FRProfileContainerViewController ()

@property (strong, nonatomic) IBOutlet UIButton *generalButton;
@property (strong, nonatomic) IBOutlet UIButton *chefButton;

@property (strong, nonatomic) IBOutlet UIView *generalContainerView;
@property (strong, nonatomic) IBOutlet UIView *chefContainerView;

@end

@implementation FRProfileContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}


- (IBAction)valueChanged:(UISegmentedControl *)sender {
    BOOL isChef = sender.selectedSegmentIndex == 1;
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.15 animations:^{
        self.generalContainerView.alpha = isChef?0.:1.;
        self.chefContainerView.alpha = isChef?1.:0.;
    } completion:^(BOOL finished) {
        [self.view sendSubviewToBack:isChef?self.generalContainerView:self.chefContainerView];
    }];
}

@end
