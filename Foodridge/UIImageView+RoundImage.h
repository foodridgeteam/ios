//
//  UIImageView+RoundImage.h
//  Foodridge
//
//  Created by Dev on 5/17/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (RoundImage)

/**
 *  Setting round image by url using AFNetworking category. In case when downloaded image hight not equal to wight setting UIViewContentModeScaleAspectFill content mode. By defualt setting UIViewContentModeScaleAspectFit content mode.
 *
 *  @param url     Image URL
 *  @param success Called when image did loaded and setted
 *  @param failure Celled when something went wrong
 */
- (void)fr_setRoundImageWithUrl:(NSURL *)url placeholderImage:(UIImage *)placeholder success:(void (^)(UIImage *image))success failure:(void (^)(NSError *error))failure;

@end
