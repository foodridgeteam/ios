//
//  FRStripeService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRStripeService.h"
#import "FRAuthorisationService.h"
#import "FRConstants.h"
#import "FRAuthorisationService.h"
#import "FRChefService.h"
#import "FRChefProfileModel.h"
#import "FRNetworkService+Stripe.h"


@implementation FRStripeService

- (BOOL)connectStripe{
    NSString *token = [FRAuthorisationService accessToken];
    if(token.length == 0){
        return  NO;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=%@&scope=read_write&state=%@",FRStripePlatoformID,token];
    NSURL *url = [NSURL URLWithString:urlString];

    return [[UIApplication sharedApplication] openURL:url];
    
}

- (BOOL)processStripeRedirect:(NSURL *)url{
    
    NSURLComponents *comps = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:NO];

    
    if([comps.host isEqualToString:@"stripeconnect"]){
        
        if([comps.path hasPrefix:@"/success"]){
            
            NSArray *items = comps.queryItems;
            NSInteger userID = NSNotFound;
            for(NSURLQueryItem *item in items){
                if([item.name isEqualToString:@"userId"]){
                    userID = [item.value integerValue];
                    break;
                }
            }
            if([FRAuthorisationService currentUID] && [FRAuthorisationService currentUID] == userID){
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"stripeDidRecive" object:nil];
                
                UIAlertController *alrt = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_STRIPE_CONNECTED_ALERT_TITLE", nil) message:NSLocalizedString(@"STR_STRIPE_CONNECTED_ALERT_MESSAGE", nil) preferredStyle:UIAlertControllerStyleAlert];
                [alrt addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alrt animated:YES completion:nil];
            }
            
        }else{
            
            UIAlertController *alrt = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STR_STRIPE_NOT_CONNECTED_ALERT_TITLE", nil) message:NSLocalizedString(@"STR_STRIPE_NOT_CONNECTED_ALERT_MESSAGE", nil) preferredStyle:UIAlertControllerStyleAlert];
            [alrt addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"STR_OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
            [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alrt animated:YES completion:nil];

        }
        
        return YES;
    }
    return NO;
}

- (NSURLSessionTask *)deleteStripeAccountWithCompletion:(FRResponceBlock)completion {
    return [FRNetworkService deleteStripeAccountWithCompletion:completion];
}

@end
