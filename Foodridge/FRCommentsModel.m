//
//  FRCommentsModel.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/22/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRCommentsModel.h"
#import "FRDateHelper.h"

@implementation FRCommentsModel

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary {
    FRCommentsModel *comment = [FRCommentsModel new];
    
    comment.orderId = [dictionary objectOrNilForKey:@"orderId" classObject:[NSNumber class]];
    comment.mealId = [dictionary objectOrNilForKey:@"mealId" classObject:[NSNumber class]];
    comment.creationDate = [FRDateHelper dateFromString:[dictionary objectOrNilForKeypath:@"created" classObject:[NSString class]]];
    comment.message = [dictionary objectOrNilForKey:@"message" classObject:[NSString class]];
    comment.rating = [dictionary objectOrNilForKey:@"rating" classObject:[NSNumber class]];
    
    NSDictionary *userProfileDictionary = [dictionary objectOrNilForKey:@"user" classObject:[NSDictionary class]];
    comment.userProfile = [FRProfileModel modelWitDictionary:userProfileDictionary];
    
    return comment;
}

@end
