//
//  FRStripeService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/21/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FRNetworkService.h"

@interface FRStripeService : NSObject

- (BOOL)connectStripe;

- (BOOL)processStripeRedirect:(NSURL *)url;

- (NSURLSessionTask *)deleteStripeAccountWithCompletion:(FRResponceBlock)completion;

@end
