//
//  FREmptyDataView.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 5/12/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FREmptyDataView.h"
#import "FRMainScreenService.h"

@interface FREmptyDataView ()

@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UIButton *actionButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *actionButtonBottomConstraint;

@end

@implementation FREmptyDataView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.actionButton.layer.cornerRadius = 3;
}

- (void)setFilterType:(NSInteger)filterType {
    _filterType = filterType;
    
    self.actionButton.hidden = YES;
    
    switch (filterType) {
        case FREmptyDataViewTypeReceived:
            self.messageLabel.text = NSLocalizedString(@"STR_NO_RECEIVED_ORDERS", nil);
            break;
            
        case FREmptyDataViewTypePlaced:
            self.messageLabel.text = NSLocalizedString(@"STR_NO_PLACED_ORDERS", nil);
            self.actionButton.hidden = NO;
            [self.actionButton setTitle:NSLocalizedString(@"STR_CHECK_MENU", nil) forState:UIControlStateNormal];
            break;
            
        case FREmptyDataViewTypeCompleted:
            self.messageLabel.text = NSLocalizedString(@"STR_NO_COMPLETED_ORDERS", nil);
            break;
            
        case FREmptyDataTypeChefs:
            self.messageLabel.text = NSLocalizedString(@"STR_NO_CHEFS", nil);
            self.actionButton.hidden = NO;
            [self.actionButton setTitle:NSLocalizedString(@"STR_INVITE", nil) forState:UIControlStateNormal];
            break;
            
        case FREmptyDataTypeMeals:
            self.messageLabel.text = NSLocalizedString(@"STR_NO_MEALS", nil);
            break;
            
        default:
            break;
    }
}


- (IBAction)actionButtonAction:(id)sender {
    if (self.filterType == FREmptyDataViewTypePlaced) {
        [[FRMainScreenService new] showDishesScreen];
    }
    else if (self.filterType == FREmptyDataTypeChefs) {
        UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        
        NSString *subject = [NSString stringWithFormat:@"Invite to Foodridge"];
        NSString *shareText = [NSString stringWithFormat:@"Hey, I would like to invite you to join Foodridge. It's a marketplace for chefs and diners to interact and transact."];
        
        NSArray *objectsToShare = @[shareText];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        [activityVC setValue:subject forKey:@"subject"];
        
        [topVC presentViewController:activityVC animated:YES completion:nil];
    }
}

@end
