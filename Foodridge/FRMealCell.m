//
//  FRMealCell.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/6/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>
#import "UIImageView+RoundImage.h"
#import "FRMealCell.h"
#import "UIButton+Localize.h"
#import "UIImage+Color.h"
#import "UIColor+FoodridgeColors.h"
#import "FRPriceControl.h"
#import "FRChefControl.h"
#import "FRDateHelper.h"
#import "FRAuthorisationService.h"

@interface FRMealCell ()
//outlets
@property (strong, nonatomic) IBOutlet UIView *insetBackgroundView;
@property (strong, nonatomic) IBOutlet UIImageView *mealImageView;
@property (strong, nonatomic) IBOutlet UILabel *mealTitleLabel;

@property (strong, nonatomic) IBOutlet UIImageView *pickupImageView;
@property (strong, nonatomic) IBOutlet UIImageView *deliveryImageView;
@property (strong, nonatomic) IBOutlet UIImageView *discountImageView;
@property (strong, nonatomic) IBOutlet UIImageView *servingsImageView;
@property (strong, nonatomic) IBOutlet UIImageView *starImageView;

@property (strong, nonatomic) IBOutlet UIView *servingsCountView;
@property (strong, nonatomic) IBOutlet UILabel *servingsCountLabel;
@property (strong, nonatomic) IBOutlet UIView *starsCountView;
@property (strong, nonatomic) IBOutlet UILabel *starsCountLabel;


@property (strong, nonatomic) IBOutlet UIButton *orderNowButton;
@property (strong, nonatomic) IBOutlet UIImageView *chefImageView;
@property (strong, nonatomic) IBOutlet UILabel *chefNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *chefLastVisitedLabel;
@property (strong, nonatomic) IBOutlet FRPriceControl *priceControl;
@property (strong, nonatomic) IBOutlet FRChefControl *chefRateControl;

@end

static NSUInteger const kFRServingsCountMaxDisplayValue = 99;
static NSString *const kFRServingsCountMaxDisplayValueString = @"99+";

@implementation FRMealCell

- (void)awakeFromNib{

    self.chefImageView.layer.masksToBounds = YES;
    self.chefImageView.layer.cornerRadius = self.chefImageView.frame.size.width/2;
    self.chefImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.orderNowButton localizeWithOptions:THLocalizationOptionsUppercase];
    self.orderNowButton.layer.cornerRadius = 3;
    self.orderNowButton.layer.masksToBounds = YES;
    [self.starImageView setHighlighted:YES];
    
}

- (void)setSelected:(BOOL)selected{
    // prevent to call super
}

- (void)prepareForReuse{
    [super prepareForReuse];
    
    [self.mealImageView cancelImageDownloadTask];
    [self.chefImageView cancelImageDownloadTask];
    
    self.chefImageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)setupCellFromMealModel:(FRMealExtendedModel *)meal {
    
    self.cellMealModel = meal;
    self.mealTitleLabel.text = meal.title;
    [self.mealImageView setImageWithURL:[NSURL URLWithString:meal.imageURLs.firstObject] placeholderImage:[UIImage imageNamed:@"logo_placeholder"]];
    
    self.chefNameLabel.text = meal.chefTitle;
    
    [self.chefImageView fr_setRoundImageWithUrl:[NSURL URLWithString:meal.chefImageURL] placeholderImage:[UIImage imageNamed:@"ic_no_user"] success:^(UIImage *image) {} failure:^(NSError *error) {}];
   
    
    self.chefLastVisitedLabel.text = [NSString stringWithFormat:NSLocalizedString(@"STR_LAST_VISITED", nil),[FRDateHelper dateStringFromDate:meal.chefLastVisit]];
    if (meal.reviewsCount.integerValue > 0){
        self.starsCountView.hidden = NO;
        self.starsCountLabel.text = [meal.reviewsCount stringValue];
    }else{
        self.starsCountView.hidden = YES;
    }
    if (meal.servingsLeft.integerValue > 0){
        self.servingsCountView.hidden = NO;
        self.servingsCountLabel.text = meal.servingsLeft.unsignedIntegerValue <= kFRServingsCountMaxDisplayValue ? [meal.servingsLeft stringValue] : kFRServingsCountMaxDisplayValueString;
        [self.orderNowButton setBackgroundImage:[UIImage imageWithColor:[UIColor aquaColor]] forState:UIControlStateNormal];
        [self.orderNowButton setTitle:NSLocalizedString(@"STR_ORDER_NOW", nil) forState:UIControlStateNormal];

    }else{
        
        [self.orderNowButton setImage:[UIImage imageNamed:@"order_icon"] forState:UIControlStateNormal];
        [self.orderNowButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateNormal];
        [self.orderNowButton setTitle:NSLocalizedString(@"STR_SOLD_OUT", nil) forState:UIControlStateNormal];
        self.servingsCountView.hidden = YES;
    }
    
    self.priceControl.price = meal.price;
    
    self.chefRateControl.value = meal.chefRating.integerValue;
    
    self.pickupImageView.highlighted = (meal.deliveryType.integerValue == FRDeliveryTypePickup || meal.deliveryType.integerValue == FRDeliveryTypePickupAndDelivery) ? YES : NO;
    self.deliveryImageView.highlighted = (meal.deliveryType.integerValue == FRDeliveryTypePickupAndDelivery || meal.deliveryType.integerValue == FRDeliveryTypeDelivery) ? YES : NO;
    self.servingsImageView.highlighted = meal.servingsLeft.integerValue > 0;
    self.starImageView.highlighted = meal.reviewsCount.integerValue > 0;
    
    self.orderNowButton.hidden = meal.chefID.integerValue == [FRAuthorisationService currentUID];

}

- (IBAction)orderAction:(id)sender {

    [self.delegate orderNowButtonClicked:self];
}

-(IBAction)showChef:(id)sender{
    
    [self.delegate showChef:self.cellMealModel.chefID];
}




@end
