//
//  FRIngridient.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/15/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <Realm/Realm.h>

@interface FRIngridient : RLMObject

@property NSInteger uid;
@property NSString *name;


@end
