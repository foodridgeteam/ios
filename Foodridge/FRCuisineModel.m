//
//  FRCuisineModel.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRCuisineModel.h"
#import "FRStaticDataService.h"
#import "FRCuisine.h"

@implementation FRCuisineModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"uid":@"id",
             @"name":@"name"
             };
}

+ (NSString *)getCuisisnesStringFromArray:(NSArray *)cuisines {

    NSString *cuisinesString = @"";
    if (cuisines) {
        cuisinesString = [[cuisines valueForKey:@"name"] componentsJoinedByString:@", "];
    }
    
    return cuisinesString;
}

+ (NSArray *)getCuisinesModelsFromCuisinesIDsArray:(NSArray *)cuisinesIDsArray {
    NSArray *cuisinesModels = cuisinesIDsArray ? [[FRStaticDataService service] getCuisinesWithIDs:[cuisinesIDsArray valueForKey:@"intValue"]] : nil;
    return cuisinesModels;
}

@end
