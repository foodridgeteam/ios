//
//  FRMyDishCell.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRMealExtendedModel.h"

@class FRMyDishCell, RNCCheckbox;

@protocol FRMyDishCellDelegate <NSObject>

@optional

- (void)editDishInCell:(FRMyDishCell *)cell;
- (void)shareActionInCell:(FRMyDishCell *)cell;
- (void) updateMealListed:(FRMyDishCell*)cell;

@end

@interface FRMyDishCell : UITableViewCell

@property (nonatomic, strong) FRMealExtendedModel *cellModel;
@property (nonatomic, weak) id<FRMyDishCellDelegate> delegate;

- (void)setupCellWithMealModel:(FRMealExtendedModel *)meal;

@end
