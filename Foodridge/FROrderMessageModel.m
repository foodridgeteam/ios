//
//  FROrderMessage.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/22/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrderMessageModel.h"
#import "FROrderModel.h"
#import "FRDateHelper.h"

@implementation FROrderMessageModel

+ (instancetype)modelWitDictionary:(NSDictionary *)dictionary {
    
    FROrderMessageModel *model = [FROrderMessageModel new];
    model.uid = [dictionary objectOrNilForKey:@"id" classObject:[NSNumber class]];
    model.senderUID = [dictionary objectOrNilForKey:@"userId" classObject:[NSNumber class]];
    model.date = [FRDateHelper dateFromString:[dictionary objectOrNilForKeypath:@"created" classObject:[NSString class]]];
    model.messageText = [dictionary objectOrNilForKey:@"message" classObject:[NSString class]];
    model.type = [dictionary objectOrNilForKey:@"system" classObject:[NSNumber class]];
    
    return model;
}

+ (NSArray *)messageImages {
    
    static NSArray *images;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        images =  @[@"chat_chef",
                    @"accept",
                    @"chef_accepted",
                    @"menu_servings_left_on",
                    @"delivered",
                    @"received",
                    @"icon_card_yes",
                    @"cancel",
                    @"cancel",
                    @"cancel",
                    @"accept"];
    });
    return images;
}

+ (NSArray *)messageImagesForClient {
    
    static NSArray *images;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        images =  @[@"user_message",
                    @"accept",
                    @"chef_accepted",
                    @"menu_servings_left_on",
                    @"delivered",
                    @"received",
                    @"icon_card_yes",
                    @"cancel",
                    @"cancel",
                    @"cancel",
                    @"accept"];
    });
    return images;
}

+ (NSString *)getMessageImageNameForOrderStatus:(NSInteger)orderStatus asReceiver:(BOOL)receiver{
    //for chat messages, response orderStatus always 0
    return  (receiver && orderStatus == 0) ? @"user_message" : [[self messageImages] objectAtIndex:orderStatus];
}

+ (NSString *)getMessageImageNameForOrderStatusClient:(NSInteger)orderStatus asReceiver:(BOOL)receiver{
    //for chat messages, response orderStatus always 0
    return  (receiver && orderStatus == 0) ? @"user_message" : [[self messageImagesForClient] objectAtIndex:orderStatus];
}



@end
