//
//  FRRateChefCell.m
//  Foodridge
//
//  Created by Victor Miroshnichenko on 5/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrderRateCell.h"
#import "FRChefControl.h"


@interface FROrderRateCell ()

@property (strong, nonatomic) IBOutlet FRChefControl *chefControl;
@property (strong, nonatomic) IBOutlet UITextView *rateTextView;
@property (strong, nonatomic) IBOutlet UIButton *rateButton;


@end

@implementation FROrderRateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.chefControl.useWhiteChefHat = YES;
    self.chefControl.value = 1;
}

- (IBAction)rateButtonAction:(id)sender {
    [self.delegate rateOrderWithText:self.rateTextView.text chefRating:self.chefControl.value inCell:self];
}

@end
