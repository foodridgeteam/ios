//
//  UITextField+Price.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/15/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Price)

-(BOOL)procesChange:(NSString *)string inRange:(NSRange)range;
- (void)finishEditing;

@end
