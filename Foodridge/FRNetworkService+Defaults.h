//
//  FRNetworkService+Defaults.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/14/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService.h"

@interface FRNetworkService (Defaults)

+ (NSURLSessionTask *)getCuisinesWithCompletion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)getIngridientsWithCompletion:(FRResponceBlock)completion;

+ (NSURLSessionTask *)sendFeedbackWithText:(NSString *)text completion:(FRResponceBlock)completion;

@end
