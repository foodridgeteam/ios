//
//  FROrderFooterCell.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/19/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FROrderFooterCell.h"
#import "NSArray+Localize.h"

@interface FROrderFooterCell()
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *localizableButtons;

@property (strong, nonatomic) IBOutlet UIButton *acceptButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *messageButton;
@property (strong, nonatomic) IBOutlet UIButton *deliverButton;

@end

@implementation FROrderFooterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.localizableButtons localize];
}

- (void)setupWithOrder:(FROrderModel *)order {
    
    if (order.asReceiver) {
        //as Customer
        self.acceptButton.enabled = NO; // is accept button enabled sometime?
        self.deliverButton.enabled = order.status == FROrderStatusSent ? YES : NO;
        [self.deliverButton setTitle:NSLocalizedString(@"STR_RECEIVED", nil) forState:UIControlStateNormal];
        [self.deliverButton setImage:[UIImage imageNamed:@"received"] forState:UIControlStateNormal];
        
        self.cancelButton.enabled = (order.status == FROrderStatusCreated);
        
    }
    else {
        //as Chef
        self.deliverButton.imageView.transform = CGAffineTransformMakeRotation(M_PI);
        self.deliverButton.enabled = NO;

        self.deliverButton.tag = 0;
        switch (order.status) {
            case FROrderStatusAccepted:
                [self.acceptButton setTitle:NSLocalizedString(@"STR_PREPARED", nil) forState:UIControlStateNormal];
                self.acceptButton.tag = 1;
                self.acceptButton.enabled = YES;
                break;
                
            case FROrderStatusPrepared:
                [self.acceptButton setTitle:NSLocalizedString(@"STR_PREPARED", nil) forState:UIControlStateNormal];
                [self.deliverButton setTitle:NSLocalizedString(@"STR_SENT", nil) forState:UIControlStateNormal];
                self.deliverButton.enabled = YES;

                self.acceptButton.tag = 1;
                self.acceptButton.enabled = NO;

                [self.deliverButton setTitle:NSLocalizedString(@"STR_SENT", nil) forState:UIControlStateNormal];
                self.deliverButton.enabled = YES;
                break;
                
            case FROrderStatusSent:
                self.acceptButton.tag = 1;
                self.acceptButton.enabled = NO;

                [self.deliverButton setTitle:NSLocalizedString(@"STR_SENT", nil) forState:UIControlStateNormal];
                
                self.deliverButton.enabled = NO;
                
                break;
                
            default:
                self.deliverButton.enabled = NO;
                [self.acceptButton setTitle:NSLocalizedString(@"STR_ACCEPT", nil) forState:UIControlStateNormal];
                break;
        }
        
        self.cancelButton.enabled = order.status >= FROrderStatusSent ? NO : YES;
        self.acceptButton.enabled = order.status < FROrderStatusPrepared ? YES : NO;
    }
    
    
    
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.cancelButton.enabled = YES;

    self.deliverButton.imageView.transform = CGAffineTransformIdentity;
}

- (IBAction)orderAcceptAction:(id)sender {
    if (self.acceptButton.tag == 1) {
        [self.delegate preparredOrderInCell:self];

    }
    [self.delegate acceptOrderInCell:self];
}

- (IBAction)orderCancelAction:(id)sender {
    [self.delegate cancelOrderInCell:self];
}

- (IBAction)orderMessageAction:(id)sender {
    [self.delegate messageToOrderInCell:self];
}

- (IBAction)orderDeliveredAction:(id)sender {
    [self.delegate deliverOrderInCell:self];
}

@end
