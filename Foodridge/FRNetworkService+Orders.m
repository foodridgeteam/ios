//
//  FRNetworkService+Orders.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/25/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRNetworkService+Orders.h"

@implementation FRNetworkService (Orders)

+ (NSURLSessionTask *)createOrder:(NSInteger)mealID
                             quantity:(NSInteger)count
                          stripeToken:(NSString *)token
                         deliveryType:(NSInteger)type
                         checkSum:(long)sum
                           completion:(FRResponceBlock)completion{
    
    NSDictionary *params = @{@"dishId":@(mealID),
                             @"quantity":@(count),
                             @"deliveryType":@(type),
                             @"totalSum":@(sum/100.),
                             @"stripeToken":token};
    
    return [self requestWithMethod:@"POST"
                    requestAddress:@"order"
                        parameters:params
                        completion:completion];
}

+ (NSURLSessionTask *)getMyOrders:(BOOL)active completion:(FRResponceBlock)completion{
    
    NSString *activeStringValue = active ? @"true" : @"false";
    
    NSDictionary *params = @{@"active":activeStringValue};
    return [self requestWithMethod:@"GET"
                    requestAddress:@"orders"
                        parameters:params
                        completion:completion];

}

+ (NSURLSessionTask *)setOrderStatus:(NSInteger)orderStatus forOrderWithID:(NSInteger)orderID completion:(FRResponceBlock)completion {
    NSDictionary *params = @{@"orderStatus" : @(orderStatus),
                             @"orderId" : @(orderID)};
    return [self requestWithMethod:@"PUT" requestAddress:@"order" parameters:params completion:completion];
}

@end
