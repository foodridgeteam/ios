//
//  FRTermsService.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/5/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

@import UIKit;

@interface FRTermsService : NSObject

- (void)showTermsFromViewController:(UIViewController *)vc shouldAgree:(BOOL)shouldAgree onDismiss:(void (^)(BOOL))onDismiss;

@end
