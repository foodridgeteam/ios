//
//  FRIngridientsCell.m
//  Foodridge
//
//  Created by Victor Miroshnychenko on 4/18/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRIngridientsCell.h"

@interface FRIngridientsCell ()

@property (strong, nonatomic) IBOutlet UILabel *ingridientName;

@end

@implementation FRIngridientsCell

- (void)setupCellWithIngridientModel:(FRIngridientModel *)ingridient {
    self.ingridientName.text = [NSString stringWithFormat:@"- %@", ingridient.name];
}

@end
