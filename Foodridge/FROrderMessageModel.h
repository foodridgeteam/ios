//
//  FROrderMessage.h
//  Foodridge
//
//  Created by Maxym Deygin on 4/22/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRViewModel.h"

@interface FROrderMessageModel : FRViewModel

@property (nonatomic, copy) NSNumber *uid;
@property (nonatomic, copy) NSNumber *senderUID;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, copy) NSString *messageText;
@property (nonatomic, copy) NSNumber *type;

+ (NSString *)getMessageImageNameForOrderStatus:(NSInteger)orderStatus asReceiver:(BOOL)receiver;

+ (NSString *)getMessageImageNameForOrderStatusClient:(NSInteger)orderStatus asReceiver:(BOOL)receiver;

@end
