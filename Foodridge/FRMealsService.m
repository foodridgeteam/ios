//
//  FRMealsService.m
//  Foodridge
//
//  Created by Maxym Deygin on 4/29/16.
//  Copyright © 2016 Rollncode. All rights reserved.
//

#import "FRMealsService.h"
#import "FRMealExtendedModel.h"
#import "FRNetworkService+Menu.h"

@implementation FRMealsService

+ (instancetype)service{
    
    FRMealsService *service = [FRMealsService new];
    service.items = [NSMutableArray new];
    service.loadMore = YES;
    
    return service;
}

+ (Class)modelClass{
    return [FRMealExtendedModel class];
}
+ (NSString *)notificationName{
    return FRMealsUpdatedNotificationName;
}

-(NSURLSessionTask *)mealListWithChefID:(NSInteger)chefID
                             searchText:(NSString *)text
                             completion:(FRResponceBlock)completion{
    
    return [FRNetworkService mealListWithLimit:defaultLimit offset:self.offset chefID:chefID searchText:text completion:^(id object, id rawObject, NSError *error) {
        
        [self prodcessResponce:rawObject orError:error withCompletion:completion];
    }];
}

- (NSURLSessionTask *)getMealWithID:(NSInteger)mealID completion:(FRResponceBlock)completion {
    return [FRNetworkService getMealWithID:mealID completion:^(id object, id rawObject, NSError *error) {
        if (!error) {
            FRMealExtendedModel *model = [FRMealExtendedModel modelWitDictionary:rawObject];
            completion(model, rawObject, error);
        }
        else {
            completion(nil, nil, error);
        }
    }];
}

@end
